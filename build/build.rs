use std::env;

use cc::Build;
use lazy_static::lazy_static;

mod sokol_headers;
mod util;

const GRAPHICS_LIB_NAME: &str = "graphics_lib_sys";

lazy_static! {
    static ref PROJECT_ROOT_DIR: String = env::var("CARGO_MANIFEST_DIR").unwrap();
    static ref PROJECT_OUT_DIR: String = env::var("OUT_DIR").unwrap();
    static ref BUILD_PROFILE: String = env::var("PROFILE").unwrap();
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Build native graphics library
    let mut graphics_build = Build::new();
    sokol_headers::download(&PROJECT_OUT_DIR, &mut graphics_build)?;

    println!("cargo:rustc-link-lib=static={}", GRAPHICS_LIB_NAME);

    #[cfg(target_os = "windows")]
    {
        graphics_build
            .define("SOKOL_D3D11", None)
            .define("_WIN32_WINNT", "0x0601")
            .define("WIN32_LEAN_AND_MEAN", None)
            .define("NOGDICAPMASKS", None)
            .define("NOSYSCOMMANDS", None)
            .define("OEMRESOURCE", None)
            .define("NOPROFILER", None)
            .define("NOKANJI", None)
            .define("NORPC", None)
            .define("NOHELP", None)
            .define("NOMCX", None)
            .define("NORASTEROPS", None)
            .define("NOSYSMETRICS", None)
            .define("NOMINMAX", None)
            .define("STRICT", None);

        println!("cargo:rustc-link-lib=dylib=dxgi");
        println!("cargo:rustc-link-lib=dylib=dxguid");
        println!("cargo:rustc-link-lib=dylib=d3d11");
    }

    #[cfg(target_os = "linux")]
    {
        graphics_build.define("SOKOL_GLES3", None);
        if BUILD_PROFILE.as_str() == "release" {
            graphics_build.flag_if_supported("-flto");
        }
        println!("cargo:rustc-link-arg=-Wl,--as-needed");
        println!("cargo:rustc-link-arg=-l{}", GRAPHICS_LIB_NAME);
        println!("cargo:rustc-link-arg=-Wl,-Bdynamic");
        println!("cargo:rustc-link-arg=-lGL");
    }

    graphics_build
        .file(format!("{}/lib/renderer/lib.c", PROJECT_ROOT_DIR.as_str()))
        .compile(GRAPHICS_LIB_NAME);

    #[cfg(target_os = "linux")]
    // Load libraries from the same directory as the executable
    #[cfg(target_os = "macos")]
    println!("cargo:rustc-link-arg=-Wl,-rpath,@loader_path");

    Ok(())
}
