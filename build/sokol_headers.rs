use std::error::Error;

use lazy_static::lazy_static;

const SOKOL_GFX_VERSION: &str = "1e4a6ec72ae7bcbc93373e6d19d7ed73798b9c76";
lazy_static! {
    static ref SOKOL_GFX_HEADER_URL: String = {
        String::from("https://raw.githubusercontent.com/floooh/sokol/")
            + SOKOL_GFX_VERSION
            + "/sokol_gfx.h"
    };
}
const SOKOL_GFX_HEADER_SHA256: Option<&str> =
    Some("abf79dbed06f4d508c783a388764af6ff13cbb0b0289926d5c5922a6da19a0a3");

const SOKOL_GP_VERSION: &str = "615364ba5084a4dd18ce18ad8573277b5b8d55c5";
lazy_static! {
    static ref SOKOL_GP_HEADER_URL: String = {
        String::from("https://raw.githubusercontent.com/edubart/sokol_gp/")
            + SOKOL_GP_VERSION
            + "/sokol_gp.h"
    };
}
const SOKOL_GP_HEADER_SHA256: Option<&str> =
    Some("13aed6f8b51971ffbab3b85e5426d4879c1de2d379ba6821cd02b0080e26257a");

pub fn download(out_dir: &str, build_context: &mut cc::Build) -> Result<(), Box<dyn Error>> {
    crate::util::download_file(&SOKOL_GFX_HEADER_URL, out_dir, SOKOL_GFX_HEADER_SHA256)?;
    crate::util::download_file(&SOKOL_GP_HEADER_URL, out_dir, SOKOL_GP_HEADER_SHA256)?;

    build_context.include(out_dir);

    Ok(())
}
