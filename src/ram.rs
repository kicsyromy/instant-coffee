use crate::bus::Device;

pub trait MemoryRange {
    const START_ADDR: u16 = 0x0000;
    const END_ADDR: u16 = 0x3FFF;
}

pub struct Ram {
    data: [u8; <Ram as MemoryRange>::END_ADDR as usize + 1],
}

impl Ram {
    pub fn new() -> Self {
        Self {
            data: [0; Self::END_ADDR as usize + 1],
        }
    }
}

impl Default for Ram {
    fn default() -> Self {
        Self::new()
    }
}

impl Device for Ram {
    fn read(&self, addr: u16) -> u8 {
        if addr > Self::END_ADDR {
            panic!("Invalid address: {:#04X}", addr);
        }

        self.data[addr as usize]
    }

    fn write(&mut self, addr: u16, data: u8) {
        if addr > Self::END_ADDR {
            panic!("Invalid address: {:#04X}", addr);
        }

        self.data[addr as usize] = data;
    }
}

impl MemoryRange for Ram {}
