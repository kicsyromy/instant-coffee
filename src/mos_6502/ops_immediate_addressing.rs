use super::*;

#[allow(non_snake_case)]
impl<const DECIMAL_SUPPORT: bool, Bus: DataBus> MOS6502<DECIMAL_SUPPORT, Bus> {
    pub fn op_0x09_ORA_bitwise_or_with_a(&mut self, value: u8) {
        self.ora_bitwise_or_with_a(value, 2, 2);
    }

    pub fn op_0x29_AND_bitwise_and_with_a(&mut self, value: u8) {
        self.and_bitwise_and_with_a(value, 2, 2);
    }

    pub fn op_0x49_EOR_bitwise_xor_with_a(&mut self, value: u8) {
        self.eor_bitwise_xor_with_a(value, 2, 2);
    }

    pub fn op_0x69_ADC_add_to_a_with_carry(&mut self, value: u8) {
        self.adc_add_to_a_with_carry(value, 2, 2);
    }

    pub fn op_0xA0_LDY_load_y_with_value(&mut self, value: u8) {
        self.ldy_load_y_with_value(value, 2, 2);
    }

    pub fn op_0xA2_LDX_load_x_with_value(&mut self, value: u8) {
        self.ldx_load_x_with_value(value, 2, 2);
    }

    pub fn op_0xA9_LDA_load_a_with_value(&mut self, value: u8) {
        self.lda_load_a_with_value(value, 2, 2);
    }

    pub fn op_0xC0_CPY_compare_with_y(&mut self, value: u8) {
        self.cpy_compare_with_y(value, 2, 2);
    }

    pub fn op_0xC9_CMP_compare_with_a(&mut self, value: u8) {
        self.cmp_compare_with_a(value, 2, 2);
    }

    pub fn op_0xE0_CPX_compare_with_x(&mut self, value: u8) {
        self.cpx_compare_with_x(value, 2, 2);
    }

    pub fn op_0xE9_SBC_subtract_from_a_with_carry(&mut self, value: u8) {
        self.sbc_subtract_from_a_with_carry(value, 2, 2);
    }
}

#[allow(non_snake_case)]
#[cfg(test)]
mod test {
    use super::super::*;

    #[test]
    fn test_ORA_negative() {
        tests::immediate::test_0x09_ORA_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x09_ORA_bitwise_or_with_a,
        );
    }

    #[test]
    fn test_ORA_zero() {
        tests::immediate::test_0x09_ORA_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x09_ORA_bitwise_or_with_a,
        );
    }

    #[test]
    fn test_AND_negative() {
        tests::immediate::test_0x29_AND_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x29_AND_bitwise_and_with_a,
        );
    }

    #[test]
    fn test_AND_zero() {
        tests::immediate::test_0x29_AND_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x29_AND_bitwise_and_with_a,
        );
    }

    #[test]
    fn test_EOR_negative() {
        tests::immediate::test_0x49_EOR_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x49_EOR_bitwise_xor_with_a,
        );
    }

    #[test]
    fn test_EOR_zero() {
        tests::immediate::test_0x49_EOR_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x49_EOR_bitwise_xor_with_a,
        );
    }

    #[test]
    fn test_ADC() {
        tests::immediate::test_0x69_ADC(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x69_ADC_add_to_a_with_carry,
        );
    }

    #[test]
    fn test_ADC_overflow_zero() {
        tests::immediate::test_0x69_ADC_overflow_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x69_ADC_add_to_a_with_carry,
        );
    }

    #[test]
    fn test_ADC_negative() {
        tests::immediate::test_0x69_ADC_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x69_ADC_add_to_a_with_carry,
        );
    }

    #[test]
    fn test_ADC_decimal_mode() {
        tests::immediate::test_0x69_ADC_decimal_mode(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x69_ADC_add_to_a_with_carry,
        );
    }

    #[test]
    fn test_ADC_decimal_mode_carry() {
        tests::immediate::test_0x69_ADC_decimal_mode_carry(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x69_ADC_add_to_a_with_carry,
        );
    }

    #[test]
    fn test_LDY() {
        tests::immediate::test_0xA0_LDY(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xA0_LDY_load_y_with_value,
        );
    }

    #[test]
    fn test_LDY_negative() {
        tests::immediate::test_0xA0_LDY_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xA0_LDY_load_y_with_value,
        );
    }

    #[test]
    fn test_LDY_zero() {
        tests::immediate::test_0xA0_LDY_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xA0_LDY_load_y_with_value,
        );
    }

    #[test]
    fn test_LDX() {
        tests::immediate::test_0xA2_LDX(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xA2_LDX_load_x_with_value,
        );
    }

    #[test]
    fn test_LDX_negative() {
        tests::immediate::test_0xA2_LDX_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xA2_LDX_load_x_with_value,
        );
    }

    #[test]
    fn test_LDX_zero() {
        tests::immediate::test_0xA2_LDX_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xA2_LDX_load_x_with_value,
        );
    }

    #[test]
    fn test_LDA() {
        tests::immediate::test_0xA9_LDA(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xA9_LDA_load_a_with_value,
        );
    }

    #[test]
    fn test_LDA_negative() {
        tests::immediate::test_0xA9_LDA_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xA9_LDA_load_a_with_value,
        );
    }

    #[test]
    fn test_LDA_zero() {
        tests::immediate::test_0xA9_LDA_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xA9_LDA_load_a_with_value,
        );
    }

    #[test]
    fn test_CPY_negative() {
        tests::immediate::test_0xC0_CPY_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xC0_CPY_compare_with_y,
        );
    }

    #[test]
    fn test_CPY_carry_zero() {
        tests::immediate::test_0xC0_CPY_zero_carry(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xC0_CPY_compare_with_y,
        );
    }

    #[test]
    fn test_CPY_carry() {
        tests::immediate::test_0xC0_CPY_carry(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xC0_CPY_compare_with_y,
        );
    }

    #[test]
    fn test_CMP_negative() {
        tests::immediate::test_0xC9_CMP_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xC9_CMP_compare_with_a,
        );
    }

    #[test]
    fn test_CMP_carry_zero() {
        tests::immediate::test_0xC9_CMP_zero_carry(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xC9_CMP_compare_with_a,
        );
    }

    #[test]
    fn test_CMP_carry() {
        tests::immediate::test_0xC9_CMP_carry(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xC9_CMP_compare_with_a,
        );
    }

    #[test]
    fn test_CPX_negative() {
        tests::immediate::test_0xE0_CPX_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xE0_CPX_compare_with_x,
        );
    }

    #[test]
    fn test_CPX_carry_zero() {
        tests::immediate::test_0xE0_CPX_zero_carry(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xE0_CPX_compare_with_x,
        );
    }

    #[test]
    fn test_CPX_carry() {
        tests::immediate::test_0xE0_CPX_carry(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xE0_CPX_compare_with_x,
        );
    }

    #[test]
    fn test_SBC() {
        tests::immediate::test_0xE9_SBC(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xE9_SBC_subtract_from_a_with_carry,
        );
    }

    #[test]
    fn test_SBC_zero() {
        tests::immediate::test_0xE9_SBC_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xE9_SBC_subtract_from_a_with_carry,
        );
    }

    #[test]
    fn test_SBC_overflow_negative() {
        tests::immediate::test_0xE9_SBC_overflow_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xE9_SBC_subtract_from_a_with_carry,
        );
    }

    #[test]
    fn test_SBC_decimal_mode() {
        tests::immediate::test_0xE9_SBC_decimal_mode(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xE9_SBC_subtract_from_a_with_carry,
        );
    }

    #[test]
    fn test_SBC_decimal_mode_overflow() {
        tests::immediate::test_0xE9_SBC_decimal_mode_overflow(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xE9_SBC_subtract_from_a_with_carry,
        );
    }
}
