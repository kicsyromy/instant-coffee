// https://www.csh.rit.edu/~moffitt/docs/6502.html

use std::cell::RefCell;
use std::rc::Rc;

use bitflags::bitflags;
use num_traits::{FromPrimitive, ToPrimitive, Unsigned};
use static_assertions::const_assert;

use crate::bus::{tests::TestDataBus, DataBus};
use crate::mos_6502::op_codes::OP_CODE_STR;

mod op_codes;
mod ops_absolute_addressing;
mod ops_absolute_indexed_addressing;
mod ops_accumulator_addressing;
mod ops_immediate_addressing;
mod ops_implicit_addressing;
mod ops_indirect_addressing;
mod ops_relative_addressing;
mod ops_zero_page_addressing;
mod ops_zero_page_indexed_addressing;

#[cfg(test)]
mod tests;

bitflags! {
    struct StatusFlags: u8 {
        const CARRY= 0b00000001;
        const ZERO= 0b00000010;
        const INTERRUPT_DISABLE= 0b00000100;
        const DECIMAL = 0b00001000;
        const BREAK = 0b00010000;
        const UNUSED = 0b00100000;
        const OVERFLOW= 0b01000000;
        const NEGATIVE = 0b10000000;
    }
}
const_assert!(core::mem::size_of::<StatusFlags>() == 1);

impl StatusFlags {
    #[inline]
    const fn carry(&self) -> bool {
        self.bits & StatusFlags::CARRY.bits > 0
    }

    #[inline]
    const fn zero(&self) -> bool {
        self.bits & StatusFlags::ZERO.bits > 0
    }

    #[inline]
    const fn interrupt_disabled(&self) -> bool {
        self.bits & StatusFlags::INTERRUPT_DISABLE.bits > 0
    }

    #[inline]
    const fn decimal_mode(&self) -> bool {
        self.bits & StatusFlags::DECIMAL.bits > 0
    }

    #[inline]
    const fn overflow(&self) -> bool {
        self.bits & StatusFlags::OVERFLOW.bits > 0
    }

    #[inline]
    const fn negative(&self) -> bool {
        self.bits & StatusFlags::NEGATIVE.bits > 0
    }
}

pub const DECIMAL_MODE_ENABLED: bool = true;
pub const DECIMAL_MODE_DISABLED: bool = false;

pub struct MOS6502<const DECIMAL_SUPPORT: bool, Bus: DataBus> {
    a: u8,
    x: u8,
    y: u8,
    pc: u16,
    sp: u8,

    bus: Rc<RefCell<Bus>>,

    p: StatusFlags,
    cycles: usize,

    op_code_handlers: [fn(&mut Self, u8, u8); 0xFF],
}

impl<const DECIMAL_SUPPORT: bool, Bus: DataBus> MOS6502<DECIMAL_SUPPORT, Bus> {
    pub fn op_not_implemented(&mut self) {
        self.pc += 1;
    }

    #[inline]
    fn ora_bitwise_or_with_a(&mut self, value: u8, advance_pc_by: u16, cycles: usize) {
        self.a |= value;
        self.p.set(StatusFlags::NEGATIVE, self.a & 0b10000000 > 0);
        self.p.set(StatusFlags::ZERO, self.a == 0);

        self.pc += advance_pc_by;
        self.cycles += cycles;
    }

    #[inline]
    fn asl_arithmetic_shift_left(&mut self, value: u8, advance_pc_by: u16, cycles: usize) -> u8 {
        self.p.set(StatusFlags::CARRY, (value >> 7) > 0);
        let result = value << 1;
        self.p.set(StatusFlags::ZERO, result == 0);
        self.p.set(StatusFlags::NEGATIVE, result & 0b10000000 > 0);

        self.pc += advance_pc_by;
        self.cycles += cycles;

        result
    }

    #[inline]
    fn and_bitwise_and_with_a(&mut self, value: u8, advance_pc_by: u16, cycles: usize) {
        self.a &= value;
        self.p.set(StatusFlags::NEGATIVE, self.a & 0b10000000 > 0);
        self.p.set(StatusFlags::ZERO, self.a == 0);

        self.pc += advance_pc_by;
        self.cycles += cycles;
    }

    #[inline]
    fn rol_rotate_left(&mut self, value: u8, advance_pc_by: u16, cycles: usize) -> u8 {
        let carry = value >> 7;
        let mut value = value << 1;
        value |= self.p.bits & 0b00000001;

        self.p.set(StatusFlags::CARRY, carry > 0);
        self.p.set(StatusFlags::ZERO, value == 0);
        self.p.set(StatusFlags::NEGATIVE, value & 0b10000000 > 0);

        self.pc += advance_pc_by;
        self.cycles += cycles;

        value
    }

    #[inline]
    pub fn lsr_logical_shift_right(&mut self, value: u8, advance_pc_by: u16, cycles: usize) -> u8 {
        self.p.remove(StatusFlags::NEGATIVE);
        self.p.set(StatusFlags::CARRY, (value & 0b00000001) > 0);
        let result = value >> 1;
        self.p.set(StatusFlags::ZERO, result == 0);

        self.pc += advance_pc_by;
        self.cycles += cycles;

        result
    }

    #[inline]
    pub fn eor_bitwise_xor_with_a(&mut self, value: u8, advance_pc_by: u16, cycles: usize) {
        self.a ^= value;
        self.p.set(StatusFlags::NEGATIVE, self.a & 0b10000000 > 0);
        self.p.set(StatusFlags::ZERO, self.a == 0);

        self.pc += advance_pc_by;
        self.cycles += cycles;
    }

    #[inline]
    pub fn adc_add_to_a_with_carry(&mut self, value: u8, advance_pc_by: u16, cycles: usize) {
        let mut a = self.a as u16 + value as u16 + (self.p & StatusFlags::CARRY).bits as u16;
        self.p.set(
            StatusFlags::OVERFLOW,
            a & 0b10000000 != (self.a & 0b10000000) as u16,
        );
        self.p.set(StatusFlags::NEGATIVE, a & 0b10000000 > 0);
        self.p.set(StatusFlags::ZERO, (a & 0xFF) == 0);

        if self.p.decimal_mode() {
            a = Self::decimal_add(self.a, value) + (self.p & StatusFlags::CARRY).bits as u16;
            self.p.set(StatusFlags::CARRY, a > 0x99);
        } else {
            self.p.set(StatusFlags::CARRY, a > u8::MAX as u16);
        }

        self.a = (a & 0xFF) as u8;

        self.pc += advance_pc_by;
        self.cycles += cycles;
    }

    // As explained on Reddit (https://www.reddit.com/r/EmuDev/comments/k5hzuo/6502_sbc/):
    // These two operations do exactly the same thing:
    // ADC #$00
    // SBC #$ff
    //
    // Indeed, in general:
    // void sbc(uint8_t operand) {
    //     adc(~operand);
    // }
    //
    // It comes down to two's complement. In two's complement you can calculate -x by performing ~x + 1. When turning a subtraction into an addition, the 6502 does the ~ but does not do the + 1. You do the + 1 by setting the carry flag.
    // So, a complete example:
    // LDA #$08
    // SEC
    // SBC #$08
    //
    // This is the same as:
    // LDA #$08
    // SEC
    // ADC #$F7
    // Which is $08 + $F7 + 1 = $100. So it's an 8-bit result of $00 with a 'carry' of 1, ready to have no effect on the next SBC.
    #[inline]
    pub fn sbc_subtract_from_a_with_carry(&mut self, value: u8, advance_pc_by: u16, cycles: usize) {
        let mut a = self.a as u16 + !value as u16 + (self.p & StatusFlags::CARRY).bits as u16;

        if self.p.decimal_mode() {
            let (res, carry) = Self::decimal_sub(self.a, value);
            a = res - !(self.p & StatusFlags::CARRY).carry() as u16;
            self.p.set(StatusFlags::OVERFLOW, !carry);
            self.p.set(StatusFlags::CARRY, carry);
        } else {
            self.p.set(
                StatusFlags::OVERFLOW,
                a & 0b10000000 != (self.a & 0b10000000) as u16,
            );
            self.p.set(StatusFlags::CARRY, a > u8::MAX as u16);
        }

        self.p.set(StatusFlags::NEGATIVE, a & 0b10000000 > 0);
        self.p.set(StatusFlags::ZERO, (a & 0xFF) == 0);

        self.a = (a & 0xFF) as u8;

        self.pc += advance_pc_by;
        self.cycles += cycles;
    }

    #[inline]
    pub fn ror_rotate_right(&mut self, value: u8, advance_pc_by: u16, cycles: usize) -> u8 {
        let carry = value & 0b00000001;
        let mut result = value >> 1;
        result |= (self.p.bits & 0b00000001) << 7;

        self.p.set(StatusFlags::CARRY, carry > 0);
        self.p.set(StatusFlags::ZERO, result == 0);
        self.p.set(StatusFlags::NEGATIVE, result & 0b10000000 > 0);

        self.pc += advance_pc_by;
        self.cycles += cycles;

        result
    }

    #[inline]
    pub fn ldy_load_y_with_value(&mut self, value: u8, advance_pc_by: u16, cycles: usize) {
        self.y = value;
        self.p.set(StatusFlags::NEGATIVE, self.y & 0b10000000 > 0);
        self.p.set(StatusFlags::ZERO, self.y == 0);

        self.pc += advance_pc_by;
        self.cycles += cycles;
    }

    #[inline]
    pub fn ldx_load_x_with_value(&mut self, value: u8, advance_pc_by: u16, cycles: usize) {
        self.x = value;
        self.p.set(StatusFlags::NEGATIVE, self.x & 0b10000000 > 0);
        self.p.set(StatusFlags::ZERO, self.x == 0);

        self.pc += advance_pc_by;
        self.cycles += cycles;
    }

    #[inline]
    pub fn lda_load_a_with_value(&mut self, value: u8, advance_pc_by: u16, cycles: usize) {
        self.a = value;
        self.p.set(StatusFlags::NEGATIVE, self.a & 0b10000000 > 0);
        self.p.set(StatusFlags::ZERO, self.a == 0);

        self.pc += advance_pc_by;
        self.cycles += cycles;
    }

    #[inline]
    pub fn cpy_compare_with_y(&mut self, value: u8, advance_pc_by: u16, cycles: usize) {
        self.p.set(StatusFlags::NEGATIVE, self.y < value);
        self.p.set(StatusFlags::ZERO, self.y == value);
        self.p.set(StatusFlags::CARRY, self.y >= value);

        self.pc += advance_pc_by;
        self.cycles += cycles;
    }

    #[inline]
    pub fn cmp_compare_with_a(&mut self, value: u8, advance_pc_by: u16, cycles: usize) {
        self.p.set(StatusFlags::NEGATIVE, self.a < value);
        self.p.set(StatusFlags::ZERO, self.a == value);
        self.p.set(StatusFlags::CARRY, self.a >= value);

        self.pc += advance_pc_by;
        self.cycles += cycles;
    }

    #[inline]
    pub fn cpx_compare_with_x(&mut self, value: u8, advance_pc_by: u16, cycles: usize) {
        self.p.set(StatusFlags::NEGATIVE, self.x < value);
        self.p.set(StatusFlags::ZERO, self.x == value);
        self.p.set(StatusFlags::CARRY, self.x >= value);

        self.pc += advance_pc_by;
        self.cycles += cycles;
    }

    #[inline]
    pub fn bit_test_bits_in_a_with_m(&mut self, value: u8, advance_pc_by: u16, cycles: usize) {
        let test = self.a & value;
        self.p |=
            StatusFlags::from_bits(test & 0b10000000).expect("Failed to extract flags from bitset");
        self.p |=
            StatusFlags::from_bits(test & 0b01000000).expect("Failed to extract flags from bitset");
        self.p.set(StatusFlags::ZERO, test == 0);

        self.pc += advance_pc_by;
        self.cycles += cycles;
    }

    #[inline]
    pub fn dec_decrement_by_one(&mut self, value: u8, advance_pc_by: u16, cycles: usize) -> u8 {
        let result = value.wrapping_sub(1);

        self.p.set(StatusFlags::NEGATIVE, result & 0b10000000 > 0);
        self.p.set(StatusFlags::ZERO, result == 0);

        self.pc += advance_pc_by;
        self.cycles += cycles;

        result
    }

    #[inline]
    pub fn inc_increment_by_one(&mut self, value: u8, advance_pc_by: u16, cycles: usize) -> u8 {
        let result = value.wrapping_add(1);

        self.p.set(StatusFlags::NEGATIVE, result & 0b10000000 > 0);
        self.p.set(StatusFlags::ZERO, result == 0);

        self.pc += advance_pc_by;
        self.cycles += cycles;

        result
    }

    // http://6502.org/tutorials/decimal_mode.html
    fn decimal_add(a: u8, b: u8) -> u16 {
        let mut lower_sum = (a & 0x0F) + (b & 0x0F);
        if lower_sum > 0x09 {
            // Add 0x06 to skip over A->F range of values
            lower_sum += 0x06;
        }
        let carry = (lower_sum & 0xF0) >> 4;

        let mut upper_sum = (a >> 4) + (b >> 4) + carry;
        if upper_sum > 0x09 {
            // Add 0x06 to skip over A->F range of values
            upper_sum += 0x06;
        }

        ((upper_sum as u16) << 4) | (lower_sum as u16 & 0x0F)
    }

    // http://6502.org/tutorials/decimal_mode.html
    fn decimal_sub(mut a: u8, mut b: u8) -> (u16, bool) {
        let negative = if b > a {
            core::mem::swap(&mut a, &mut b);
            true
        } else {
            false
        };
        let mut lower_diff = (a & 0x0F) as i8 - (b & 0x0F) as i8;
        let borrow = if lower_diff < 0 {
            lower_diff += 10;
            if lower_diff > 0x09 {
                // Sub 0x06 to skip over A->F range of values
                lower_diff -= 0x06;
            }
            1_u8
        } else {
            0_u8
        };

        let upper_diff = (a >> 4) - (b >> 4) - borrow;

        let result = ((upper_diff as u16) << 4) | (lower_diff as u16 & 0x0F);
        if negative {
            (0x100_u16 - result - 0x66, false)
        } else {
            (result, true)
        }
    }

    #[inline]
    fn read(&self, high_addr: u8, low_addr: u8) -> u8 {
        let addr = ((high_addr as u16) << 8) | low_addr as u16;
        self.read_simple(addr)
    }

    #[inline]
    fn read_indexed(&self, high_addr: u8, low_addr: u8, index: u8) -> (u8, usize) {
        let addr = ((high_addr as u16) << 8) | low_addr as u16;
        let addr_indexed = addr.wrapping_add(index as u16);

        let extra_cycle = ((addr / 256) != (addr_indexed / 256)) as usize;
        (self.read_simple(addr_indexed), extra_cycle)
    }

    #[inline]
    fn read_simple(&self, addr: u16) -> u8 {
        self.bus.borrow().read(addr)
    }

    #[inline]
    fn write(&mut self, high_addr: u8, low_addr: u8, value: u8) {
        let addr = ((high_addr as u16) << 8) | low_addr as u16;
        self.write_simple(addr, value);
    }

    #[inline]
    fn write_indexed(&mut self, high_addr: u8, low_addr: u8, index: u8, value: u8) -> usize {
        let addr = ((high_addr as u16) << 8) | low_addr as u16;
        let addr_indexed = addr.wrapping_add(index as u16);

        self.write_simple(addr_indexed, value);

        let extra_cycle = ((addr / 256) != (addr_indexed / 256)) as usize;
        extra_cycle
    }

    #[inline]
    fn write_simple(&mut self, addr: u16, value: u8) {
        self.bus.borrow_mut().write(addr, value);
    }

    #[inline]
    fn stack_push<UInt>(&mut self, value: UInt)
    where
        UInt: Unsigned + ToPrimitive + FromPrimitive,
    {
        let value_size = core::mem::size_of::<UInt>();

        self.sp = self.sp.wrapping_sub(value_size as u8);

        let value = value
            .to_usize()
            .expect("Failed to push value to stack; conversion to usize failed");

        for i in 0..value_size {
            self.write_simple(
                0x0100 + self.sp.wrapping_add(i as u8 + 1) as u16,
                ((value >> (i * 8)) & 0xFF) as u8,
            );
        }
    }

    #[inline]
    fn stack_pop<UInt>(&mut self) -> UInt
    where
        UInt: Unsigned + ToPrimitive + FromPrimitive,
    {
        let value_size = core::mem::size_of::<UInt>();

        let mut result = 0_usize;
        for i in 0..value_size {
            let addr = 0x0100 + self.sp.wrapping_add(i as u8 + 1) as u16;
            let value = self.read_simple(addr) as usize;
            result |= (value) << (i * 8);
        }

        self.sp = self.sp.wrapping_add(value_size as u8);

        UInt::from_usize(result).expect("Stack value conversion from usize failed")
    }

    pub fn run_next_instruction(&mut self) {
        let opcode = self.read_simple(self.pc);

        let opcode_str = OP_CODE_STR[opcode as usize];
        let byte0 = self.read_simple(self.pc + 1);
        let byte1 = self.read_simple(self.pc + 2);
        let addr = ((byte1 as u16) << 8) | byte0 as u16;
        dbg!("Running opcode: {}({}, {})", opcode_str, byte0, byte1);

        self.op_code_handlers[opcode as usize](
            self,
            self.read_simple(self.pc + 1),
            self.read_simple(self.pc + 2),
        );
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_stack_push() {
        const VALUE: u16 = 0xDEAD;

        let mut cpu = MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::new(Rc::new(RefCell::new(
            TestDataBus::new(),
        )));
        cpu.sp = 6;

        cpu.stack_push(VALUE);
        assert_eq!(cpu.sp, 4);
        assert_eq!(cpu.read_simple(0x0100 + 5), 0xAD);
        assert_eq!(cpu.read_simple(0x0100 + 6), 0xDE);
    }

    #[test]
    fn test_stack_pop() {
        let mut cpu = MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::new(Rc::new(RefCell::new(
            TestDataBus::new(),
        )));
        cpu.sp = 4;
        cpu.write_simple(0x0100 + 5, 0xAD);
        cpu.write_simple(0x0100 + 6, 0xDE);

        assert_eq!(cpu.stack_pop::<u16>(), 0xDEAD);
        assert_eq!(cpu.sp, 6);
    }

    #[test]
    fn functional_tests() {
        let data_bus = Rc::new(RefCell::new(TestDataBus::new()));
        let mut cpu = MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::new(Rc::clone(&data_bus));

        cpu.pc = 0x0400;
        data_bus.borrow_mut().write_bytes(
            0x0000,
            include_bytes!("../../test/6502_functional_test.bin"),
        );

        let mut pc = [0xdead, 0xbeef];
        let mut ip = 0;

        loop {
            pc[ip % 2] = cpu.pc;
            ip += 1;

            if cpu.pc == 0x3469 {
                break;
            }
            if pc[0] == pc[1] {
                dbg!(cpu.pc);
                assert!(false);
            }

            cpu.run_next_instruction();
        }
    }
}
