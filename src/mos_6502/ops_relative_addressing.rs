use super::*;

#[allow(non_snake_case)]
impl<const DECIMAL_SUPPORT: bool, Bus: DataBus> MOS6502<DECIMAL_SUPPORT, Bus> {
    #[inline]
    fn branch_if_flag(&mut self, conditional_state: bool, offset: u8) {
        if conditional_state {
            let start_page = self.pc / 256;

            self.pc += offset as u16;
            let end_page = self.pc / 256;

            if start_page == end_page {
                self.cycles += 1;
            } else {
                self.cycles += 2;
            }
        } else {
            self.pc += 2;
        }

        self.cycles += 2;
    }

    pub fn op_0x10_BPL_branch_if_n_clear(&mut self, offset: u8) {
        self.branch_if_flag(!self.p.negative(), offset);
    }

    pub fn op_0x30_BMI_branch_if_n_set(&mut self, offset: u8) {
        self.branch_if_flag(self.p.negative(), offset);
    }

    pub fn op_0x50_BVC_branch_if_v_clear(&mut self, offset: u8) {
        self.branch_if_flag(!self.p.overflow(), offset);
    }

    pub fn op_0x70_BVS_branch_if_v_set(&mut self, offset: u8) {
        self.branch_if_flag(self.p.overflow(), offset);
    }

    pub fn op_0x90_BCC_branch_if_c_clear(&mut self, offset: u8) {
        self.branch_if_flag(!self.p.carry(), offset);
    }

    pub fn op_0xB0_BCS_branch_if_c_set(&mut self, offset: u8) {
        self.branch_if_flag(self.p.carry(), offset);
    }

    pub fn op_0xD0_BNE_branch_if_z_clear(&mut self, offset: u8) {
        self.branch_if_flag(!self.p.zero(), offset);
    }

    pub fn op_0xF0_BEQ_branch_if_z_set(&mut self, offset: u8) {
        self.branch_if_flag(self.p.zero(), offset);
    }
}

#[allow(non_snake_case)]
#[cfg(test)]
mod test {
    use super::super::*;

    #[test]
    fn test_BPL_negative() {
        tests::relative::test_0x10_BPL_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x10_BPL_branch_if_n_clear,
        );
    }

    #[test]
    fn test_BPL_positive_same_page() {
        tests::relative::test_0x10_BPL_positive_same_page(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x10_BPL_branch_if_n_clear,
        );
    }

    #[test]
    fn test_BPL_positive_different_page() {
        tests::relative::test_0x10_BPL_positive_different_page(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x10_BPL_branch_if_n_clear,
        );
    }

    #[test]
    fn test_BMI_positive() {
        tests::relative::test_0x30_BMI_positive(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x30_BMI_branch_if_n_set,
        );
    }

    #[test]
    fn test_BMI_negative_same_page() {
        tests::relative::test_0x30_BMI_negative_same_page(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x30_BMI_branch_if_n_set,
        );
    }

    #[test]
    fn test_BMI_negative_different_page() {
        tests::relative::test_0x30_BMI_negative_different_page(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x30_BMI_branch_if_n_set,
        );
    }

    #[test]
    fn test_BVC_overflow() {
        tests::relative::test_0x50_BVC_overflow(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x50_BVC_branch_if_v_clear,
        );
    }

    #[test]
    fn test_BVC_no_overflow_same_page() {
        tests::relative::test_0x50_BVC_no_overflow_same_page(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x50_BVC_branch_if_v_clear,
        );
    }

    #[test]
    fn test_BVC_no_overflow_different_page() {
        tests::relative::test_0x50_BVC_no_overflow_different_page(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x50_BVC_branch_if_v_clear,
        );
    }

    #[test]
    fn test_BVS_no_overflow() {
        tests::relative::test_0x70_BVS_no_overflow(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x70_BVS_branch_if_v_set,
        );
    }

    #[test]
    fn test_BVS_overflow_same_page() {
        tests::relative::test_0x70_BVS_overflow_same_page(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x70_BVS_branch_if_v_set,
        );
    }

    #[test]
    fn test_BVS_overflow_different_page() {
        tests::relative::test_0x70_BVS_overflow_different_page(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x70_BVS_branch_if_v_set,
        );
    }

    #[test]
    fn test_BCC_carry() {
        tests::relative::test_0x90_BCC_carry(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x90_BCC_branch_if_c_clear,
        );
    }

    #[test]
    fn test_BCC_no_carry_same_page() {
        tests::relative::test_0x90_BCC_no_carry_same_page(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x90_BCC_branch_if_c_clear,
        );
    }

    #[test]
    fn test_BCC_no_carry_different_page() {
        tests::relative::test_0x90_BCC_no_carry_different_page(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x90_BCC_branch_if_c_clear,
        );
    }

    #[test]
    fn test_BVS_no_carry() {
        tests::relative::test_0xB0_BCS_no_carry(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xB0_BCS_branch_if_c_set,
        );
    }

    #[test]
    fn test_BVS_carry_same_page() {
        tests::relative::test_0xB0_BCS_carry_same_page(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xB0_BCS_branch_if_c_set,
        );
    }

    #[test]
    fn test_BVS_carry_different_page() {
        tests::relative::test_0xB0_BCS_carry_different_page(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xB0_BCS_branch_if_c_set,
        );
    }

    #[test]
    fn test_BNE_zero() {
        tests::relative::test_0xD0_BNE_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xD0_BNE_branch_if_z_clear,
        );
    }

    #[test]
    fn test_BNE_non_zero_same_page() {
        tests::relative::test_0xD0_BNE_non_zero_same_page(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xD0_BNE_branch_if_z_clear,
        );
    }

    #[test]
    fn test_BNE_non_zero_different_page() {
        tests::relative::test_0xD0_BNE_non_zero_different_page(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xD0_BNE_branch_if_z_clear,
        );
    }

    #[test]
    fn test_BEQ_non_zero() {
        tests::relative::test_0xF0_BEQ_non_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xF0_BEQ_branch_if_z_set,
        );
    }

    #[test]
    fn test_BEQ_zero_same_page() {
        tests::relative::test_0xF0_BEQ_zero_same_page(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xF0_BEQ_branch_if_z_set,
        );
    }

    #[test]
    fn test_BEQ_zero_different_page() {
        tests::relative::test_0xF0_BEQ_zero_different_page(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xF0_BEQ_branch_if_z_set,
        );
    }
}
