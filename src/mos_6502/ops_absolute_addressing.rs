use super::*;

#[allow(non_snake_case)]
impl<const DECIMAL_SUPPORT: bool, Bus: DataBus> MOS6502<DECIMAL_SUPPORT, Bus> {
    pub fn op_0x0D_ORA_bitwise_or_a_with_memory(&mut self, low_addr: u8, high_addr: u8) {
        let (value, _) = self.read_indexed(high_addr, low_addr, 0);
        self.ora_bitwise_or_with_a(value, 3, 4);
    }

    pub fn op_0x0E_ASL_arithmetic_shift_left(&mut self, low_addr: u8, high_addr: u8) {
        let (value, _) = self.read_indexed(high_addr, low_addr, 0);

        let result = self.asl_arithmetic_shift_left(value, 3, 6);
        let _ = self.write_indexed(high_addr, low_addr, 0, result);
    }

    pub fn op_0x20_JSR_jump_to_subroutine(&mut self, low_addr: u8, high_addr: u8) {
        let location = ((high_addr as u16) << 8) | low_addr as u16;

        self.pc += 3;
        self.stack_push(self.pc - 1);
        self.pc = location;

        self.cycles += 6;
    }

    pub fn op_0x2C_BIT_test_bits_in_a_with_m(&mut self, low_addr: u8, high_addr: u8) {
        let (value, _) = self.read_indexed(high_addr, low_addr, 0);
        self.bit_test_bits_in_a_with_m(value, 3, 4);
    }

    pub fn op_0x2D_AND_bitwise_and_with_a(&mut self, low_addr: u8, high_addr: u8) {
        let (value, _) = self.read_indexed(high_addr, low_addr, 0);
        self.and_bitwise_and_with_a(value, 3, 4);
    }

    pub fn op_0x2E_ROL_rotate_left(&mut self, low_addr: u8, high_addr: u8) {
        let (value, _) = self.read_indexed(high_addr, low_addr, 0);

        let result = self.rol_rotate_left(value, 3, 6);
        let _ = self.write_indexed(high_addr, low_addr, 0, result);
    }

    pub fn op_0x4C_JMP_jump_to_address(&mut self, low_addr: u8, high_addr: u8) {
        let location = ((high_addr as u16) << 8) | low_addr as u16;

        self.pc = location;
        self.cycles += 3;
    }

    pub fn op_0x4D_EOR_bitwise_xor_with_a(&mut self, low_addr: u8, high_addr: u8) {
        let (value, _) = self.read_indexed(high_addr, low_addr, 0);
        self.eor_bitwise_xor_with_a(value, 3, 4);
    }

    pub fn op_0x4E_LSR_logical_shift_right(&mut self, low_addr: u8, high_addr: u8) {
        let (value, _) = self.read_indexed(high_addr, low_addr, 0);

        let result = self.lsr_logical_shift_right(value, 3, 6);
        let _ = self.write_indexed(high_addr, low_addr, 0, result);
    }

    pub fn op_0x6D_ADC_add_to_a_with_carry(&mut self, low_addr: u8, high_addr: u8) {
        let (value, _) = self.read_indexed(high_addr, low_addr, 0);
        self.adc_add_to_a_with_carry(value, 3, 4);
    }

    pub fn op_0x6E_ROR_rotate_right(&mut self, low_addr: u8, high_addr: u8) {
        let (value, _) = self.read_indexed(high_addr, low_addr, 0);

        let result = self.ror_rotate_right(value, 3, 6);
        let _ = self.write_indexed(high_addr, low_addr, 0, result);
    }

    pub fn op_0x8C_STY_store_y_in_memory(&mut self, low_addr: u8, high_addr: u8) {
        let result = self.y;
        let _ = self.write_indexed(high_addr, low_addr, 0, result);

        self.pc += 3;
        self.cycles += 4;
    }

    pub fn op_0x8D_STA_store_a_in_memory(&mut self, low_addr: u8, high_addr: u8) {
        let result = self.a;
        let _ = self.write_indexed(high_addr, low_addr, 0, result);

        self.pc += 3;
        self.cycles += 4;
    }

    pub fn op_0x8E_STX_store_x_in_memory(&mut self, low_addr: u8, high_addr: u8) {
        let result = self.x;
        let _ = self.write_indexed(high_addr, low_addr, 0, result);

        self.pc += 3;
        self.cycles += 4;
    }

    pub fn op_0xAC_LDY_load_y_with_memory(&mut self, low_addr: u8, high_addr: u8) {
        let (value, _) = self.read_indexed(high_addr, low_addr, 0);
        self.ldy_load_y_with_value(value, 3, 4);
    }

    pub fn op_0xAD_LDA_load_a_with_memory(&mut self, low_addr: u8, high_addr: u8) {
        let (value, _) = self.read_indexed(high_addr, low_addr, 0);
        self.lda_load_a_with_value(value, 3, 4);
    }

    pub fn op_0xAE_LDX_load_x_with_memory(&mut self, low_addr: u8, high_addr: u8) {
        let (value, _) = self.read_indexed(high_addr, low_addr, 0);
        self.ldx_load_x_with_value(value, 3, 4);
    }

    pub fn op_0xCC_CPY_compare_with_y(&mut self, low_addr: u8, high_addr: u8) {
        let (value, _) = self.read_indexed(high_addr, low_addr, 0);
        self.cpy_compare_with_y(value, 3, 4);
    }

    pub fn op_0xCD_CMP_compare_with_a(&mut self, low_addr: u8, high_addr: u8) {
        let (value, _) = self.read_indexed(high_addr, low_addr, 0);
        self.cmp_compare_with_a(value, 3, 4);
    }

    pub fn op_0xCE_DEC_decrement_by_one(&mut self, low_addr: u8, high_addr: u8) {
        let (value, _) = self.read_indexed(high_addr, low_addr, 0);

        let result = self.dec_decrement_by_one(value, 3, 6);
        let _ = self.write_indexed(high_addr, low_addr, 0, result);
    }

    pub fn op_0xEC_CPX_compare_with_x(&mut self, low_addr: u8, high_addr: u8) {
        let (value, _) = self.read_indexed(high_addr, low_addr, 0);
        self.cpx_compare_with_x(value, 3, 4);
    }

    pub fn op_0xED_SBC_subtract_from_a_with_carry(&mut self, low_addr: u8, high_addr: u8) {
        let (value, _) = self.read_indexed(high_addr, low_addr, 0);
        self.sbc_subtract_from_a_with_carry(value, 3, 4);
    }

    pub fn op_0xEE_INC_increment_by_one(&mut self, low_addr: u8, high_addr: u8) {
        let (value, _) = self.read_indexed(high_addr, low_addr, 0);

        let result = self.inc_increment_by_one(value, 3, 6);
        let _ = self.write_indexed(high_addr, low_addr, 0, result);
    }
}

#[allow(non_snake_case)]
#[cfg(test)]
mod test {
    use super::super::*;

    #[test]
    fn test_ORA_negative() {
        tests::absolute::test_0x0D_ORA_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x0D_ORA_bitwise_or_a_with_memory,
        );
    }

    #[test]
    fn test_ORA_zero() {
        tests::absolute::test_0x0D_ORA_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x0D_ORA_bitwise_or_a_with_memory,
        );
    }

    #[test]
    fn test_ASL_negative() {
        tests::absolute::test_0x0E_ASL_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x0E_ASL_arithmetic_shift_left,
        );
    }

    #[test]
    fn test_ASL_carry_zero() {
        tests::absolute::test_0x0E_ASL_carry_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x0E_ASL_arithmetic_shift_left,
        );
    }

    #[test]
    fn test_JSR() {
        tests::absolute::test_0x20_JSR(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x20_JSR_jump_to_subroutine,
        );
    }

    #[test]
    fn test_BIT_negative() {
        tests::absolute::test_0x2C_BIT_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x2C_BIT_test_bits_in_a_with_m,
        );
    }

    #[test]
    fn test_BIT_overflow() {
        tests::absolute::test_0x2C_BIT_overflow(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x2C_BIT_test_bits_in_a_with_m,
        );
    }

    #[test]
    fn test_BIT_zero() {
        tests::absolute::test_0x2C_BIT_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x2C_BIT_test_bits_in_a_with_m,
        );
    }

    #[test]
    fn test_AND_negative() {
        tests::absolute::test_0x2D_AND_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x2D_AND_bitwise_and_with_a,
        );
    }

    #[test]
    fn test_AND_zero() {
        tests::absolute::test_0x2D_AND_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x2D_AND_bitwise_and_with_a,
        );
    }

    #[test]
    fn test_ROL_negative() {
        tests::absolute::test_0x2E_ROL_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x2E_ROL_rotate_left,
        );
    }

    #[test]
    fn test_ROL_carry_zero() {
        tests::absolute::test_0x2E_ROL_carry_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x2E_ROL_rotate_left,
        );
    }

    #[test]
    fn test_JMP() {
        tests::absolute::test_0x4C_JMP(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x4C_JMP_jump_to_address,
        );
    }

    #[test]
    fn test_EOR_negative() {
        tests::absolute::test_0x4D_EOR_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x4D_EOR_bitwise_xor_with_a,
        );
    }

    #[test]
    fn test_EOR_zero() {
        tests::absolute::test_0x4D_EOR_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x4D_EOR_bitwise_xor_with_a,
        );
    }

    #[test]
    fn test_LSR() {
        tests::absolute::test_0x4E_LSR(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x4E_LSR_logical_shift_right,
        );
    }

    #[test]
    fn test_ADC() {
        tests::absolute::test_0x6D_ADC(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x6D_ADC_add_to_a_with_carry,
        );
    }

    #[test]
    fn test_ADC_overflow_zero() {
        tests::absolute::test_0x6D_ADC_overflow_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x6D_ADC_add_to_a_with_carry,
        );
    }

    #[test]
    fn test_ADC_negative() {
        tests::absolute::test_0x6D_ADC_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x6D_ADC_add_to_a_with_carry,
        );
    }

    #[test]
    fn test_ADC_decimal_mode() {
        tests::absolute::test_0x6D_ADC_decimal_mode(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x6D_ADC_add_to_a_with_carry,
        );
    }

    #[test]
    fn test_ADC_decimal_mode_carry() {
        tests::absolute::test_0x6D_ADC_decimal_mode_carry(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x6D_ADC_add_to_a_with_carry,
        );
    }

    #[test]
    fn test_ROR_negative() {
        tests::absolute::test_0x6E_ROR_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x6E_ROR_rotate_right,
        );
    }

    #[test]
    fn test_ROR_carry_zero() {
        tests::absolute::test_0x6E_ROR_carry_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x6E_ROR_rotate_right,
        );
    }

    #[test]
    fn test_STY() {
        tests::absolute::test_0x8C_STY(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x8C_STY_store_y_in_memory,
        );
    }

    #[test]
    fn test_STA() {
        tests::absolute::test_0x8D_STA(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x8D_STA_store_a_in_memory,
        );
    }

    #[test]
    fn test_STX() {
        tests::absolute::test_0x8E_STX(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x8E_STX_store_x_in_memory,
        );
    }

    #[test]
    fn test_LDY() {
        tests::absolute::test_0xAC_LDY(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xAC_LDY_load_y_with_memory,
        );
    }

    #[test]
    fn test_LDY_negative() {
        tests::absolute::test_0xAC_LDY_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xAC_LDY_load_y_with_memory,
        );
    }

    #[test]
    fn test_LDY_zero() {
        tests::absolute::test_0xAC_LDY_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xAC_LDY_load_y_with_memory,
        );
    }

    #[test]
    fn test_LDA() {
        tests::absolute::test_0xAD_LDA(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xAD_LDA_load_a_with_memory,
        );
    }

    #[test]
    fn test_LDA_negative() {
        tests::absolute::test_0xAD_LDA_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xAD_LDA_load_a_with_memory,
        );
    }

    #[test]
    fn test_LDA_zero() {
        tests::absolute::test_0xAD_LDA_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xAD_LDA_load_a_with_memory,
        );
    }

    #[test]
    fn test_LDX() {
        tests::absolute::test_0xAE_LDX(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xAE_LDX_load_x_with_memory,
        );
    }

    #[test]
    fn test_LDX_negative() {
        tests::absolute::test_0xAE_LDX_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xAE_LDX_load_x_with_memory,
        );
    }

    #[test]
    fn test_LDX_zero() {
        tests::absolute::test_0xAE_LDX_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xAE_LDX_load_x_with_memory,
        );
    }

    #[test]
    fn test_CPY_negative() {
        tests::absolute::test_0xCC_CPY_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xCC_CPY_compare_with_y,
        );
    }

    #[test]
    fn test_CPY_carry_zero() {
        tests::absolute::test_0xCC_CPY_zero_carry(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xCC_CPY_compare_with_y,
        );
    }

    #[test]
    fn test_CPY_carry() {
        tests::absolute::test_0xCC_CPY_carry(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xCC_CPY_compare_with_y,
        );
    }

    #[test]
    fn test_CMP_negative() {
        tests::absolute::test_0xCD_CMP_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xCD_CMP_compare_with_a,
        );
    }

    #[test]
    fn test_CMP_carry_zero() {
        tests::absolute::test_0xCD_CMP_zero_carry(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xCD_CMP_compare_with_a,
        );
    }

    #[test]
    fn test_CMP_carry() {
        tests::absolute::test_0xCD_CMP_carry(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xCD_CMP_compare_with_a,
        );
    }

    #[test]
    fn test_DEC() {
        tests::absolute::test_0xCE_DEC(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xCE_DEC_decrement_by_one,
        );
    }

    #[test]
    fn test_DEC_negative() {
        tests::absolute::test_0xCE_DEC_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xCE_DEC_decrement_by_one,
        );
    }

    #[test]
    fn test_DEC_zero() {
        tests::absolute::test_0xCE_DEC_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xCE_DEC_decrement_by_one,
        );
    }

    #[test]
    fn test_CPX_negative() {
        tests::absolute::test_0xEC_CPX_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xEC_CPX_compare_with_x,
        );
    }

    #[test]
    fn test_CPX_carry_zero() {
        tests::absolute::test_0xEC_CPX_zero_carry(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xEC_CPX_compare_with_x,
        );
    }

    #[test]
    fn test_CPX_carry() {
        tests::absolute::test_0xEC_CPX_carry(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xEC_CPX_compare_with_x,
        );
    }

    #[test]
    fn test_SBC() {
        tests::absolute::test_0xED_SBC(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xED_SBC_subtract_from_a_with_carry,
        );
    }

    #[test]
    fn test_SBC_zero() {
        tests::absolute::test_0xED_SBC_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xED_SBC_subtract_from_a_with_carry,
        );
    }

    #[test]
    fn test_SBC_overflow_negative() {
        tests::absolute::test_0xED_SBC_overflow_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xED_SBC_subtract_from_a_with_carry,
        );
    }

    #[test]
    fn test_SBC_decimal_mode() {
        tests::absolute::test_0xED_SBC_decimal_mode(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xED_SBC_subtract_from_a_with_carry,
        );
    }

    #[test]
    fn test_SBC_decimal_mode_overflow() {
        tests::absolute::test_0xED_SBC_decimal_mode_overflow(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xED_SBC_subtract_from_a_with_carry,
        );
    }

    #[test]
    fn test_INC() {
        tests::absolute::test_0xEE_INC(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xEE_INC_increment_by_one,
        );
    }

    #[test]
    fn test_INC_negative() {
        tests::absolute::test_0xEE_INC_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xEE_INC_increment_by_one,
        );
    }

    #[test]
    fn test_INC_zero() {
        tests::absolute::test_0xEE_INC_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xEE_INC_increment_by_one,
        );
    }
}
