use super::*;

#[allow(non_snake_case)]
impl<const DECIMAL_SUPPORT: bool, Bus: DataBus> MOS6502<DECIMAL_SUPPORT, Bus> {
    #[inline]
    fn addr_indirect_x(&self, addr_loc: u8) -> u16 {
        let index = self.x;

        let low = self.read_simple(addr_loc.wrapping_add(index) as u16);
        let high = self.read_simple(addr_loc.wrapping_add(index).wrapping_add(1) as u16);

        (high as u16) << 8 | low as u16
    }

    #[inline]
    fn addr_indirect_y(&self, addr_loc: u8) -> (u16, usize) {
        let index = self.y;

        let low = self.read_simple(addr_loc as u16);
        let high = self.read_simple(addr_loc.wrapping_add(1) as u16);

        let addr = (high as u16) << 8 | low as u16;
        let addr_indexed = addr.wrapping_add(index as u16);

        let extra_cycles = if (addr / 256) != (addr_indexed / 256) {
            1_usize
        } else {
            0_usize
        };

        (addr_indexed, extra_cycles)
    }

    pub fn op_0x01_ORA_increment_by_one_x(&mut self, addr: u8) {
        let value = self.read_simple(self.addr_indirect_x(addr));
        self.ora_bitwise_or_with_a(value, 3, 6);
    }

    pub fn op_0x11_ORA_increment_by_one_y(&mut self, addr: u8) {
        let (addr, extra_cycles) = self.addr_indirect_y(addr);
        self.ora_bitwise_or_with_a(self.read_simple(addr), 3, 5 + extra_cycles);
    }

    pub fn op_0x21_AND_bitwise_and_with_a_x(&mut self, addr: u8) {
        let value = self.read_simple(self.addr_indirect_x(addr));
        self.and_bitwise_and_with_a(value, 3, 6);
    }

    pub fn op_0x31_AND_bitwise_and_with_a_y(&mut self, addr: u8) {
        let (addr, extra_cycles) = self.addr_indirect_y(addr);
        self.and_bitwise_and_with_a(self.read_simple(addr), 3, 5 + extra_cycles);
    }

    pub fn op_0x41_EOR_bitwise_xor_with_a_x(&mut self, addr: u8) {
        let value = self.read_simple(self.addr_indirect_x(addr));
        self.eor_bitwise_xor_with_a(value, 3, 6);
    }

    pub fn op_0x51_EOR_bitwise_xor_with_a_y(&mut self, addr: u8) {
        let (addr, extra_cycles) = self.addr_indirect_y(addr);
        self.eor_bitwise_xor_with_a(self.read_simple(addr), 3, 5 + extra_cycles);
    }

    pub fn op_0x61_ADC_add_to_a_with_carry_x(&mut self, addr: u8) {
        let value = self.read_simple(self.addr_indirect_x(addr));
        self.adc_add_to_a_with_carry(value, 3, 6);
    }

    pub fn op_0x6C_JMP_jump_to_address(&mut self, addr: u8) {
        let low = self.read_simple(addr as u16);
        let high = self.read_simple(addr.wrapping_add(1) as u16);

        let location = (high as u16) << 8 | low as u16;

        self.pc = location;
        self.cycles += 5;
    }

    pub fn op_0x71_ADC_add_to_a_with_carry_y(&mut self, addr: u8) {
        let (addr, extra_cycles) = self.addr_indirect_y(addr);
        self.adc_add_to_a_with_carry(self.read_simple(addr), 3, 5 + extra_cycles);
    }

    pub fn op_0x81_STA_store_a_in_memory_x(&mut self, addr: u8) {
        self.write_simple(self.addr_indirect_x(addr), self.a);

        self.pc += 3;
        self.cycles += 6;
    }

    pub fn op_0x91_STA_store_a_in_memory_y(&mut self, addr: u8) {
        let (addr, extra_cycles) = self.addr_indirect_y(addr);
        self.write_simple(addr, self.a);

        self.pc += 3;
        self.cycles += 5 + extra_cycles;
    }

    pub fn op_0xA1_LDA_load_a_with_value_x(&mut self, addr: u8) {
        let value = self.read_simple(self.addr_indirect_x(addr));
        self.lda_load_a_with_value(value, 3, 6);
    }

    pub fn op_0xB1_LDA_load_a_with_value_y(&mut self, addr: u8) {
        let (addr, extra_cycles) = self.addr_indirect_y(addr);
        self.lda_load_a_with_value(self.read_simple(addr), 3, 5 + extra_cycles);
    }

    pub fn op_0xC1_CMP_compare_with_a_x(&mut self, addr: u8) {
        let value = self.read_simple(self.addr_indirect_x(addr));
        self.cmp_compare_with_a(value, 3, 6);
    }

    pub fn op_0xD1_CMP_compare_with_a_y(&mut self, addr: u8) {
        let (addr, extra_cycles) = self.addr_indirect_y(addr);
        self.cmp_compare_with_a(self.read_simple(addr), 3, 5 + extra_cycles);
    }

    pub fn op_0xE1_SBC_subtract_from_a_with_carry_x(&mut self, addr: u8) {
        let value = self.read_simple(self.addr_indirect_x(addr));
        self.sbc_subtract_from_a_with_carry(value, 3, 6);
    }

    pub fn op_0xF1_SBC_subtract_from_a_with_carry_y(&mut self, addr: u8) {
        let (addr, extra_cycles) = self.addr_indirect_y(addr);
        self.sbc_subtract_from_a_with_carry(self.read_simple(addr), 3, 5 + extra_cycles);
    }
}

#[allow(non_snake_case)]
#[cfg(test)]
mod test {
    //use super::super::*;

    #[test]
    fn test_ORA_negative() {}
}
