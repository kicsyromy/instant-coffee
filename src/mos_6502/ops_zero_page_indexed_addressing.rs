use super::*;

#[allow(non_snake_case)]
impl<const DECIMAL_SUPPORT: bool, Bus: DataBus> MOS6502<DECIMAL_SUPPORT, Bus> {
    pub fn op_0x15_ORA_bitwise_or_a_with_memory_x(&mut self, addr: u8) {
        let value = self.read_simple(addr.wrapping_add(self.x) as u16);
        self.ora_bitwise_or_with_a(value, 2, 3);
    }

    pub fn op_0x16_ASL_arithmetic_shift_left_x(&mut self, addr: u8) {
        let value = self.read_simple(addr.wrapping_add(self.x) as u16);

        let result = self.asl_arithmetic_shift_left(value, 2, 6);
        self.write_simple(addr as u16, result);
    }

    pub fn op_0x35_AND_bitwise_and_with_a_x(&mut self, addr: u8) {
        let value = self.read_simple(addr.wrapping_add(self.x) as u16);
        self.and_bitwise_and_with_a(value, 2, 3);
    }

    pub fn op_0x36_ROL_rotate_left_x(&mut self, addr: u8) {
        let value = self.read_simple(addr.wrapping_add(self.x) as u16);

        let result = self.rol_rotate_left(value, 2, 6);
        self.write_simple(addr as u16, result);
    }

    pub fn op_0x55_EOR_bitwise_xor_with_a_x(&mut self, addr: u8) {
        let value = self.read_simple(addr.wrapping_add(self.x) as u16);
        self.eor_bitwise_xor_with_a(value, 2, 4);
    }

    pub fn op_0x56_LSR_logical_shift_right_x(&mut self, addr: u8) {
        let value = self.read_simple(addr.wrapping_add(self.x) as u16);

        let result = self.lsr_logical_shift_right(value, 2, 6);
        self.write_simple(addr as u16, result);
    }

    pub fn op_0x75_ADC_add_to_a_with_carry_x(&mut self, addr: u8) {
        let value = self.read_simple(addr.wrapping_add(self.x) as u16);
        self.adc_add_to_a_with_carry(value, 2, 4);
    }

    pub fn op_0x76_ROR_rotate_right_x(&mut self, addr: u8) {
        let value = self.read_simple(addr.wrapping_add(self.x) as u16);

        let result = self.ror_rotate_right(value, 2, 6);
        self.write_simple(addr as u16, result);
    }

    pub fn op_0x94_STY_store_y_in_memory_x(&mut self, addr: u8) {
        self.write_simple(addr.wrapping_add(self.x) as u16, self.y);

        self.pc += 2;
        self.cycles += 4;
    }

    pub fn op_0x95_STA_store_a_in_memory_x(&mut self, addr: u8) {
        self.write_simple(addr.wrapping_add(self.x) as u16, self.a);

        self.pc += 2;
        self.cycles += 4;
    }

    pub fn op_0x96_STX_store_x_in_memory_y(&mut self, addr: u8) {
        self.write_simple(addr.wrapping_add(self.y) as u16, self.x);

        self.pc += 2;
        self.cycles += 4;
    }

    pub fn op_0xB4_LDY_load_y_with_memory_x(&mut self, addr: u8) {
        let value = self.read_simple(addr.wrapping_add(self.x) as u16);
        self.ldy_load_y_with_value(value, 2, 4);
    }

    pub fn op_0xB5_LDA_load_a_with_memory_x(&mut self, addr: u8) {
        let value = self.read_simple(addr.wrapping_add(self.x) as u16);
        self.lda_load_a_with_value(value, 2, 4);
    }

    pub fn op_0xB6_LDX_load_x_with_memory_y(&mut self, addr: u8) {
        let value = self.read_simple(addr.wrapping_add(self.y) as u16);
        self.ldx_load_x_with_value(value, 2, 4);
    }

    pub fn op_0xD5_CMP_compare_with_a_x(&mut self, addr: u8) {
        let value = self.read_simple(addr.wrapping_add(self.x) as u16);
        self.cmp_compare_with_a(value, 2, 4);
    }

    pub fn op_0xD6_DEC_decrement_by_one_x(&mut self, addr: u8) {
        let value = self.read_simple(addr.wrapping_add(self.x) as u16);

        let result = self.dec_decrement_by_one(value, 2, 6);
        self.write_simple(addr as u16, result);
    }

    pub fn op_0xF5_SBC_subtract_from_a_with_carry_x(&mut self, addr: u8) {
        let value = self.read_simple(addr.wrapping_add(self.x) as u16);
        self.sbc_subtract_from_a_with_carry(value, 2, 4);
    }

    pub fn op_0xF6_INC_increment_by_one_x(&mut self, addr: u8) {
        let value = self.read_simple(addr.wrapping_add(self.x) as u16);

        let result = self.inc_increment_by_one(value, 2, 6);
        self.write_simple(addr as u16, result);
    }
}

#[allow(non_snake_case)]
#[cfg(test)]
mod test {
    use super::super::*;

    #[test]
    fn test_ORA_negative() {
        tests::zero_page_indexed::test_0x15_ORA_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x15_ORA_bitwise_or_a_with_memory_x,
        );
    }

    #[test]
    fn test_ORA_zero() {
        tests::zero_page_indexed::test_0x15_ORA_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x15_ORA_bitwise_or_a_with_memory_x,
        );
    }

    #[test]
    fn test_ASL_negative() {
        tests::zero_page_indexed::test_0x16_ASL_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x16_ASL_arithmetic_shift_left_x,
        );
    }

    #[test]
    fn test_ASL_carry_zero() {
        tests::zero_page_indexed::test_0x16_ASL_carry_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x16_ASL_arithmetic_shift_left_x,
        );
    }

    #[test]
    fn test_AND_negative() {
        tests::zero_page_indexed::test_0x35_AND_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x35_AND_bitwise_and_with_a_x,
        );
    }

    #[test]
    fn test_AND_zero() {
        tests::zero_page_indexed::test_0x35_AND_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x35_AND_bitwise_and_with_a_x,
        );
    }

    #[test]
    fn test_ROL_negative() {
        tests::zero_page_indexed::test_0x36_ROL_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x36_ROL_rotate_left_x,
        );
    }

    #[test]
    fn test_ROL_carry_zero() {
        tests::zero_page_indexed::test_0x36_ROL_carry_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x36_ROL_rotate_left_x,
        );
    }

    #[test]
    fn test_EOR_negative() {
        tests::zero_page_indexed::test_0x55_EOR_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x55_EOR_bitwise_xor_with_a_x,
        );
    }

    #[test]
    fn test_EOR_zero() {
        tests::zero_page_indexed::test_0x55_EOR_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x55_EOR_bitwise_xor_with_a_x,
        );
    }

    #[test]
    fn test_LSR() {
        tests::zero_page_indexed::test_0x56_LSR(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x56_LSR_logical_shift_right_x,
        );
    }

    #[test]
    fn test_ADC() {
        tests::zero_page_indexed::test_0x75_ADC(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x75_ADC_add_to_a_with_carry_x,
        );
    }

    #[test]
    fn test_ADC_overflow_zero() {
        tests::zero_page_indexed::test_0x75_ADC_overflow_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x75_ADC_add_to_a_with_carry_x,
        );
    }

    #[test]
    fn test_ADC_negative() {
        tests::zero_page_indexed::test_0x75_ADC_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x75_ADC_add_to_a_with_carry_x,
        );
    }

    #[test]
    fn test_ADC_decimal_mode() {
        tests::zero_page_indexed::test_0x75_ADC_decimal_mode(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x75_ADC_add_to_a_with_carry_x,
        );
    }

    #[test]
    fn test_ADC_decimal_mode_carry() {
        tests::zero_page_indexed::test_0x75_ADC_decimal_mode_carry(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x75_ADC_add_to_a_with_carry_x,
        );
    }

    #[test]
    fn test_ROR_negative() {
        tests::zero_page_indexed::test_0x76_ROR_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x76_ROR_rotate_right_x,
        );
    }

    #[test]
    fn test_ROR_carry_zero() {
        tests::zero_page_indexed::test_0x76_ROR_carry_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x76_ROR_rotate_right_x,
        );
    }

    #[test]
    fn test_STY() {
        tests::zero_page_indexed::test_0x94_STY(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x94_STY_store_y_in_memory_x,
        );
    }

    #[test]
    fn test_STA() {
        tests::zero_page_indexed::test_0x95_STA(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x95_STA_store_a_in_memory_x,
        );
    }

    #[test]
    fn test_STX() {
        tests::zero_page_indexed::test_0x96_STX(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x96_STX_store_x_in_memory_y,
        );
    }

    #[test]
    fn test_LDY() {
        tests::zero_page_indexed::test_0xB4_LDY(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xB4_LDY_load_y_with_memory_x,
        );
    }

    #[test]
    fn test_LDY_negative() {
        tests::zero_page_indexed::test_0xB4_LDY_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xB4_LDY_load_y_with_memory_x,
        );
    }

    #[test]
    fn test_LDY_zero() {
        tests::zero_page_indexed::test_0xB4_LDY_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xB4_LDY_load_y_with_memory_x,
        );
    }

    #[test]
    fn test_LDA() {
        tests::zero_page_indexed::test_0xB5_LDA(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xB5_LDA_load_a_with_memory_x,
        );
    }

    #[test]
    fn test_LDA_negative() {
        tests::zero_page_indexed::test_0xB5_LDA_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xB5_LDA_load_a_with_memory_x,
        );
    }

    #[test]
    fn test_LDA_zero() {
        tests::zero_page_indexed::test_0xB5_LDA_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xB5_LDA_load_a_with_memory_x,
        );
    }

    #[test]
    fn test_LDX() {
        tests::zero_page_indexed::test_0xB6_LDX(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xB6_LDX_load_x_with_memory_y,
        );
    }

    #[test]
    fn test_LDX_negative() {
        tests::zero_page_indexed::test_0xB6_LDX_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xB6_LDX_load_x_with_memory_y,
        );
    }

    #[test]
    fn test_LDX_zero() {
        tests::zero_page_indexed::test_0xB6_LDX_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xB6_LDX_load_x_with_memory_y,
        );
    }

    #[test]
    fn test_CMP_negative() {
        tests::zero_page_indexed::test_0xD5_CMP_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xD5_CMP_compare_with_a_x,
        );
    }

    #[test]
    fn test_CMP_carry_zero() {
        tests::zero_page_indexed::test_0xD5_CMP_zero_carry(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xD5_CMP_compare_with_a_x,
        );
    }

    #[test]
    fn test_CMP_carry() {
        tests::zero_page_indexed::test_0xD5_CMP_carry(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xD5_CMP_compare_with_a_x,
        );
    }

    #[test]
    fn test_DEC() {
        tests::zero_page_indexed::test_0xD6_DEC(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xD6_DEC_decrement_by_one_x,
        );
    }

    #[test]
    fn test_DEC_negative() {
        tests::zero_page_indexed::test_0xD6_DEC_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xD6_DEC_decrement_by_one_x,
        );
    }

    #[test]
    fn test_DEC_zero() {
        tests::zero_page_indexed::test_0xD6_DEC_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xD6_DEC_decrement_by_one_x,
        );
    }

    #[test]
    fn test_SBC() {
        tests::zero_page_indexed::test_0xF5_SBC(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xF5_SBC_subtract_from_a_with_carry_x,
        );
    }

    #[test]
    fn test_SBC_zero() {
        tests::zero_page_indexed::test_0xF5_SBC_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xF5_SBC_subtract_from_a_with_carry_x,
        );
    }

    #[test]
    fn test_SBC_overflow_negative() {
        tests::zero_page_indexed::test_0xF5_SBC_overflow_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xF5_SBC_subtract_from_a_with_carry_x,
        );
    }

    #[test]
    fn test_SBC_decimal_mode() {
        tests::zero_page_indexed::test_0xF5_SBC_decimal_mode(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xF5_SBC_subtract_from_a_with_carry_x,
        );
    }

    #[test]
    fn test_SBC_decimal_mode_overflow() {
        tests::zero_page_indexed::test_0xF5_SBC_decimal_mode_overflow(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xF5_SBC_subtract_from_a_with_carry_x,
        );
    }

    #[test]
    fn test_INC() {
        tests::zero_page_indexed::test_0xF6_INC(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xF6_INC_increment_by_one_x,
        );
    }

    #[test]
    fn test_INC_negative() {
        tests::zero_page_indexed::test_0xF6_INC_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xF6_INC_increment_by_one_x,
        );
    }

    #[test]
    fn test_INC_zero() {
        tests::zero_page_indexed::test_0xF6_INC_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xF6_INC_increment_by_one_x,
        );
    }
}
