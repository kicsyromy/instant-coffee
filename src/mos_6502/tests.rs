use super::*;

#[allow(non_snake_case)]
pub mod absolute {
    use super::{super::*, *};

    pub fn test_0x0D_ORA_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b10101010;
        cpu.write_simple(0xBEEF, 0b01010101);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0b11111111);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x0D_ORA_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0;
        cpu.write_simple(0xBEEF, 0);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x0E_ASL_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.write_simple(0xBEEF, 0b01010101);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 6);

        assert_eq!(cpu.read_simple(0xBEEF), 0b10101010);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x0E_ASL_carry_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.write_simple(0xBEEF, 0b10000000);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 6);

        assert_eq!(cpu.read_simple(0xBEEF), 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x20_JSR<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0xFEEB;
        cpu.sp = u8::MAX;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 0xBEEF);
        assert_eq!(cpu.sp, u8::MAX - 2);
        assert_eq!(cpu.read_simple(0x0100 + (cpu.sp + 1) as u16), 0xEB + 2);
        assert_eq!(cpu.read_simple(0x0100 + (cpu.sp + 2) as u16), 0xFE);
    }

    pub fn test_0x2C_BIT_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b10101010;
        cpu.write_simple(0xBEEF, 0b10000000);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b01000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x2C_BIT_overflow<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b01010101;
        cpu.write_simple(0xBEEF, 0b01000000);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b01000000, 0b01000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x2C_BIT_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b10101010;
        cpu.write_simple(0xBEEF, 0b01010101);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b01000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x2D_AND_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b11010101;
        cpu.write_simple(0xBEEF, 0b10101010);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0b10000000);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x2D_AND_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0;
        cpu.write_simple(0xBEEF, 0b11111111);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x2E_ROL_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::CARRY;

        cpu.write_simple(0xBEEF, 0b01010101);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 6);

        assert_eq!(cpu.read_simple(0xBEEF), 0b10101011);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x2E_ROL_carry_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.write_simple(0xBEEF, 0b10000000);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 6);

        assert_eq!(cpu.read_simple(0xBEEF), 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x4C_JMP<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 0xBEEF);
        assert_eq!(cpu.cycles, 3);
    }

    pub fn test_0x4D_EOR_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b11010101;
        cpu.write_simple(0xBEEF, 0b00101010);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0b11111111);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x4D_EOR_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b11111111;
        cpu.write_simple(0xBEEF, 0b11111111);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x4E_LSR<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.write_simple(0xBEEF, 0b00000001);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 6);

        assert_eq!(cpu.read_simple(0xBEEF), 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x6D_ADC<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 11;
        cpu.write_simple(0xBEEF, 11);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);
        assert_eq!(cpu.p.bits, 0);

        assert_eq!(cpu.a, 22);
    }

    pub fn test_0x6D_ADC_overflow_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 250;
        cpu.write_simple(0xBEEF, 6);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b01000000, 0b01000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
        assert_eq!(cpu.a, 0);
    }

    pub fn test_0x6D_ADC_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b01111111;
        cpu.write_simple(0xBEEF, 1);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b01000000, 0b01000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
        assert_eq!(cpu.a, 128);
    }

    pub fn test_0x6D_ADC_decimal_mode<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::DECIMAL;

        cpu.a = 0x11;
        cpu.write_simple(0xBEEF, 0x11);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);
        assert_eq!(cpu.p.bits, 0b00001000);

        assert_eq!(cpu.a, 0x22);
    }

    pub fn test_0x6D_ADC_decimal_mode_carry<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::DECIMAL;

        cpu.a = 0x49;
        cpu.write_simple(0xBEEF, 0x55);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b01000000, 0b01000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
        assert_eq!(cpu.a, 0x04);
    }

    pub fn test_0x6E_ROR_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::CARRY;

        cpu.write_simple(0xBEEF, 0b10101010);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 6);

        assert_eq!(cpu.read_simple(0xBEEF), 0b11010101);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x6E_ROR_carry_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.write_simple(0xBEEF, 0b00000001);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 6);

        assert_eq!(cpu.read_simple(0xBEEF), 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x8C_STY<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 42;
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.read_simple(0xBEEF), 42);
        assert_eq!(cpu.p.bits, 0);
    }

    pub fn test_0x8D_STA<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 42;
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.read_simple(0xBEEF), 42);
        assert_eq!(cpu.p.bits, 0);
    }

    pub fn test_0x8E_STX<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 42;
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.read_simple(0xBEEF), 42);
        assert_eq!(cpu.p.bits, 0);
    }

    pub fn test_0xAC_LDY<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 0xAB;
        cpu.write_simple(0xBEEF, 0x7F);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.y, 0x7F);
        assert_eq!(cpu.p.bits, 0);
    }

    pub fn test_0xAC_LDY_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 0xAB;
        cpu.write_simple(0xBEEF, 0xFF);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.y, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
    }

    pub fn test_0xAC_LDY_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 0xAB;
        cpu.write_simple(0xBEEF, 0);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.y, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0xAD_LDA<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 0xAB;
        cpu.write_simple(0xBEEF, 0x7F);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0x7F);
        assert_eq!(cpu.p.bits, 0);
    }

    pub fn test_0xAD_LDA_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 0xAB;
        cpu.write_simple(0xBEEF, 0xFF);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
    }

    pub fn test_0xAD_LDA_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 0xAB;
        cpu.write_simple(0xBEEF, 0);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0xAE_LDX<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 0xAB;
        cpu.write_simple(0xBEEF, 0x7F);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.x, 0x7F);
        assert_eq!(cpu.p.bits, 0);
    }

    pub fn test_0xAE_LDX_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 0xAB;
        cpu.write_simple(0xBEEF, 0xFF);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.x, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
    }

    pub fn test_0xAE_LDX_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 0xAB;
        cpu.write_simple(0xBEEF, 0);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.x, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0xCC_CPY_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 0x0F;
        cpu.write_simple(0xBEEF, 0xFF);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.y, 0x0F);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
    }

    pub fn test_0xCC_CPY_zero_carry<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 0xFF;
        cpu.write_simple(0xBEEF, 0xFF);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.y, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
    }

    pub fn test_0xCC_CPY_carry<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 0xFF;
        cpu.write_simple(0xBEEF, 0xF0);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.y, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
    }

    pub fn test_0xCD_CMP_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 0x0F;
        cpu.write_simple(0xBEEF, 0xFF);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0x0F);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
    }

    pub fn test_0xCD_CMP_zero_carry<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 0xFF;
        cpu.write_simple(0xBEEF, 0xFF);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
    }

    pub fn test_0xCD_CMP_carry<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 0xFF;
        cpu.write_simple(0xBEEF, 0xF0);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
    }

    pub fn test_0xCE_DEC<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.write_simple(0xBEEF, 0x0B);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 6);

        assert_eq!(cpu.read_simple(0xBEEF), 0x0A);
        assert_eq!(cpu.p.bits & 0b11111111, 0);
    }

    pub fn test_0xCE_DEC_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.write_simple(0xBEEF, 0x00);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 6);

        assert_eq!(cpu.read_simple(0xBEEF), 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0xCE_DEC_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.write_simple(0xBEEF, 0x01);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 6);

        assert_eq!(cpu.read_simple(0xBEEF), 0x00);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0xEC_CPX_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 0x0F;
        cpu.write_simple(0xBEEF, 0xFF);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.x, 0x0F);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
    }

    pub fn test_0xEC_CPX_zero_carry<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 0xFF;
        cpu.write_simple(0xBEEF, 0xFF);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.x, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
    }

    pub fn test_0xEC_CPX_carry<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 0xFF;
        cpu.write_simple(0xBEEF, 0xF0);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.x, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
    }

    pub fn test_0xED_SBC<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        // Carry flag needs to be set when performing a subtraction
        cpu.p = StatusFlags::CARRY;

        cpu.a = 23;
        cpu.write_simple(0xBEEF, 14);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b01000000, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);

        assert_eq!(cpu.a, 9);
    }

    pub fn test_0xED_SBC_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        // Carry flag needs to be set when performing a subtraction
        cpu.p = StatusFlags::CARRY;

        cpu.a = 14;
        cpu.write_simple(0xBEEF, 14);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b01000000, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);

        assert_eq!(cpu.a, 0);
    }

    pub fn test_0xED_SBC_overflow_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        // Carry flag needs to be set when performing a subtraction
        cpu.p = StatusFlags::CARRY;

        cpu.a = 14;
        cpu.write_simple(0xBEEF, 23);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b01000000, 0b01000000);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0);

        assert_eq!(cpu.a, 247);
    }

    pub fn test_0xED_SBC_decimal_mode<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        // Carry flag needs to be set when performing a subtraction
        cpu.p = StatusFlags::CARRY | StatusFlags::DECIMAL;

        cpu.a = 0x23;
        cpu.write_simple(0xBEEF, 0x14);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b01000000, 0);
        assert_eq!(cpu.p.bits & 0b00001000, 0b00001000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);

        assert_eq!(cpu.a, 0x9);
    }

    pub fn test_0xED_SBC_decimal_mode_overflow<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        // Carry flag needs to be set when performing a subtraction
        cpu.p = StatusFlags::CARRY | StatusFlags::DECIMAL;

        cpu.a = 0x12;
        cpu.write_simple(0xBEEF, 0x21);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b01000000, 0b01000000);
        assert_eq!(cpu.p.bits & 0b00001000, 0b00001000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0);

        assert_eq!(cpu.a, 0x91);
    }

    pub fn test_0xEE_INC<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.write_simple(0xBEEF, 0x0B);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 6);

        assert_eq!(cpu.read_simple(0xBEEF), 0x0C);
        assert_eq!(cpu.p.bits & 0b11111111, 0);
    }

    pub fn test_0xEE_INC_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.write_simple(0xBEEF, 0b01111111);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 6);

        assert_eq!(cpu.read_simple(0xBEEF), 0b10000000);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0xEE_INC_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.write_simple(0xBEEF, 0xFF);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 6);

        assert_eq!(cpu.read_simple(0xBEEF), 0x00);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }
}

#[allow(non_snake_case)]
pub mod absolute_indexed {
    use super::{super::*, *};

    pub fn test_0x19_ORA_negative_y<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.y = 6;
        cpu.a = 0b10101010;
        cpu.write_simple(0xBEF5, 0b01010101);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0b11111111);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x19_ORA_zero_y<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.y = 6;
        cpu.a = 0;
        cpu.write_simple(0xBEF5, 0);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x1D_ORA_negative_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.x = 6;
        cpu.a = 0b10101010;
        cpu.write_simple(0xBEF5, 0b01010101);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0b11111111);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x1D_ORA_zero_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.x = 6;
        cpu.a = 0;
        cpu.write_simple(0xBEF5, 0);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x1E_ASL_negative_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.write_simple(0xBEF5, 0b01010101);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 7);

        assert_eq!(cpu.read_simple(0xBEEF), 0b10101010);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x1E_ASL_carry_zero_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.write_simple(0xBEF5, 0b10000000);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 7);

        assert_eq!(cpu.read_simple(0xBEEF), 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x39_AND_negative_y<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b11010101;
        cpu.y = 6;
        cpu.write_simple(0xBEF5, 0b10101010);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0b10000000);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x39_AND_zero_y<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0;
        cpu.y = 6;
        cpu.write_simple(0xBEF5, 0b11111111);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x3D_AND_negative_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b11010101;
        cpu.x = 6;
        cpu.write_simple(0xBEF5, 0b10101010);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0b10000000);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x3D_AND_zero_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0;
        cpu.x = 6;
        cpu.write_simple(0xBEF5, 0b11111111);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x3E_ROL_negative_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::CARRY;

        cpu.x = 6;
        cpu.write_simple(0xBEF5, 0b01010101);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 7);

        assert_eq!(cpu.read_simple(0xBEEF), 0b10101011);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x3E_ROL_carry_zero_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.write_simple(0xBEF5, 0b10000000);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 7);

        assert_eq!(cpu.read_simple(0xBEEF), 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x59_EOR_negative_y<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b11010101;
        cpu.y = 6;
        cpu.write_simple(0xBEF5, 0b00101010);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0b11111111);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x59_EOR_zero_y<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b11111111;
        cpu.y = 6;
        cpu.write_simple(0xBEF5, 0b11111111);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x5D_EOR_negative_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b11010101;
        cpu.x = 6;
        cpu.write_simple(0xBEF5, 0b00101010);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0b11111111);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x5D_EOR_zero_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b11111111;
        cpu.x = 6;
        cpu.write_simple(0xBEF5, 0b11111111);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x5E_LSR_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.write_simple(0xBEF5, 0b00000001);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 7);

        assert_eq!(cpu.read_simple(0xBEEF), 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x79_ADC_y<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 6;
        cpu.a = 11;
        cpu.write_simple(0xBEF5, 11);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);
        assert_eq!(cpu.p.bits, 0);

        assert_eq!(cpu.a, 22);
    }

    pub fn test_0x79_ADC_overflow_zero_y<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 6;
        cpu.a = 250;
        cpu.write_simple(0xBEF5, 6);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b01000000, 0b01000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
        assert_eq!(cpu.a, 0);
    }

    pub fn test_0x79_ADC_negative_y<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b01111111;
        cpu.y = 6;
        cpu.write_simple(0xBEF5, 1);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b01000000, 0b01000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
        assert_eq!(cpu.a, 128);
    }

    pub fn test_0x79_ADC_decimal_mode_y<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::DECIMAL;

        cpu.y = 6;
        cpu.a = 0x11;
        cpu.write_simple(0xBEF5, 0x11);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);
        assert_eq!(cpu.p.bits, 0b00001000);

        assert_eq!(cpu.a, 0x22);
    }

    pub fn test_0x79_ADC_decimal_mode_carry_y<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::DECIMAL;

        cpu.y = 6;
        cpu.a = 0x49;
        cpu.write_simple(0xBEF5, 0x55);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b01000000, 0b01000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
        assert_eq!(cpu.a, 0x04);
    }

    pub fn test_0x7D_ADC_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.a = 11;
        cpu.write_simple(0xBEF5, 11);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);
        assert_eq!(cpu.p.bits, 0);

        assert_eq!(cpu.a, 22);
    }

    pub fn test_0x7D_ADC_overflow_zero_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.a = 250;
        cpu.write_simple(0xBEF5, 6);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b01000000, 0b01000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
        assert_eq!(cpu.a, 0);
    }

    pub fn test_0x7D_ADC_negative_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b01111111;
        cpu.x = 6;
        cpu.write_simple(0xBEF5, 1);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b01000000, 0b01000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
        assert_eq!(cpu.a, 128);
    }

    pub fn test_0x7D_ADC_decimal_mode_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::DECIMAL;

        cpu.x = 6;
        cpu.a = 0x11;
        cpu.write_simple(0xBEF5, 0x11);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);
        assert_eq!(cpu.p.bits, 0b00001000);

        assert_eq!(cpu.a, 0x22);
    }

    pub fn test_0x7D_ADC_decimal_mode_carry_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::DECIMAL;

        cpu.x = 6;
        cpu.a = 0x49;
        cpu.write_simple(0xBEF5, 0x55);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b01000000, 0b01000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
        assert_eq!(cpu.a, 0x04);
    }

    pub fn test_0x7E_ROR_negative_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::CARRY;

        cpu.x = 6;
        cpu.write_simple(0xBEF5, 0b10101010);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 7);

        assert_eq!(cpu.read_simple(0xBEEF), 0b11010101);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x7E_ROR_carry_zero_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.write_simple(0xBEF5, 0b00000001);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 7);

        assert_eq!(cpu.read_simple(0xBEEF), 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x99_STA_y<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 6;
        cpu.a = 42;
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 5);

        assert_eq!(cpu.read_simple(0xBEF5), 42);
        assert_eq!(cpu.p.bits, 0);
    }

    pub fn test_0x9D_STA_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.a = 42;
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 5);

        assert_eq!(cpu.read_simple(0xBEF5), 42);
        assert_eq!(cpu.p.bits, 0);
    }

    pub fn test_0xB9_LDA_y<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 6;
        cpu.a = 0xAB;
        cpu.write_simple(0xBEF5, 0x7F);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0x7F);
        assert_eq!(cpu.p.bits, 0);
    }

    pub fn test_0xB9_LDA_negative_y<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 6;
        cpu.a = 0xAB;
        cpu.write_simple(0xBEF5, 0xFF);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
    }

    pub fn test_0xB9_LDA_zero_y<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 6;
        cpu.a = 0xAB;
        cpu.write_simple(0xBEF5, 0);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0xBC_LDY_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.y = 0xAB;
        cpu.write_simple(0xBEF5, 0x7F);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.y, 0x7F);
        assert_eq!(cpu.p.bits, 0);
    }

    pub fn test_0xBC_LDY_negative_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.y = 0xB5;
        cpu.write_simple(0xBEF5, 0xFF);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.y, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
    }

    pub fn test_0xBC_LDY_zero_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.y = 0xAB;
        cpu.write_simple(0xBEF5, 0);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.y, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0xBD_LDA_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.a = 0xAB;
        cpu.write_simple(0xBEF5, 0x7F);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0x7F);
        assert_eq!(cpu.p.bits, 0);
    }

    pub fn test_0xBD_LDA_negative_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.a = 0xAB;
        cpu.write_simple(0xBEF5, 0xFF);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
    }

    pub fn test_0xBD_LDA_zero_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.a = 0xAB;
        cpu.write_simple(0xBEF5, 0);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0xBE_LDX_y<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 6;
        cpu.x = 0xAB;
        cpu.write_simple(0xBEF5, 0x7F);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.x, 0x7F);
        assert_eq!(cpu.p.bits, 0);
    }

    pub fn test_0xBE_LDX_negative_y<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 6;
        cpu.x = 0xAB;
        cpu.write_simple(0xBEF5, 0xFF);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.x, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
    }

    pub fn test_0xBE_LDX_zero_y<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 6;
        cpu.x = 0xAB;
        cpu.write_simple(0xBEF5, 0);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.x, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0xD9_CMP_negative_y<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 6;
        cpu.a = 0x0F;
        cpu.write_simple(0xBEF5, 0xFF);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0x0F);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
    }

    pub fn test_0xD9_CMP_zero_carry_y<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 6;
        cpu.a = 0xFF;
        cpu.write_simple(0xBEF5, 0xFF);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
    }

    pub fn test_0xD9_CMP_carry_y<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 6;
        cpu.a = 0xFF;
        cpu.write_simple(0xBEF5, 0xF0);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
    }

    pub fn test_0xDD_CMP_negative_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.a = 0x0F;
        cpu.write_simple(0xBEF5, 0xFF);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0x0F);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
    }

    pub fn test_0xDD_CMP_zero_carry_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.a = 0xFF;
        cpu.write_simple(0xBEF5, 0xFF);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
    }

    pub fn test_0xDD_CMP_carry_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.a = 0xFF;
        cpu.write_simple(0xBEF5, 0xF0);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
    }

    pub fn test_0xDE_DEC_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.write_simple(0xBEF5, 0x0B);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 7);

        assert_eq!(cpu.read_simple(0xBEEF), 0x0A);
        assert_eq!(cpu.p.bits & 0b11111111, 0);
    }

    pub fn test_0xDE_DEC_negative_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.write_simple(0xBEF5, 0x00);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 7);

        assert_eq!(cpu.read_simple(0xBEEF), 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0xDE_DEC_zero_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.write_simple(0xBEF5, 0x01);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 7);

        assert_eq!(cpu.read_simple(0xBEEF), 0x00);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0xF9_SBC_y<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        // Carry flag needs to be set when performing a subtraction
        cpu.p = StatusFlags::CARRY;

        cpu.y = 6;
        cpu.a = 23;
        cpu.write_simple(0xBEF5, 14);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b01000000, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);

        assert_eq!(cpu.a, 9);
    }

    pub fn test_0xF9_SBC_zero_y<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        // Carry flag needs to be set when performing a subtraction
        cpu.p = StatusFlags::CARRY;

        cpu.y = 6;
        cpu.a = 14;
        cpu.write_simple(0xBEF5, 14);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b01000000, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);

        assert_eq!(cpu.a, 0);
    }

    pub fn test_0xF9_SBC_overflow_negative_y<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        // Carry flag needs to be set when performing a subtraction
        cpu.p = StatusFlags::CARRY;

        cpu.y = 6;
        cpu.a = 14;
        cpu.write_simple(0xBEF5, 23);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b01000000, 0b01000000);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0);

        assert_eq!(cpu.a, 247);
    }

    pub fn test_0xF9_SBC_decimal_mode_y<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        // Carry flag needs to be set when performing a subtraction
        cpu.p = StatusFlags::CARRY | StatusFlags::DECIMAL;

        cpu.y = 6;
        cpu.a = 0x23;
        cpu.write_simple(0xBEF5, 0x14);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b01000000, 0);
        assert_eq!(cpu.p.bits & 0b00001000, 0b00001000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);

        assert_eq!(cpu.a, 0x9);
    }

    pub fn test_0xF9_SBC_decimal_mode_overflow_y<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        // Carry flag needs to be set when performing a subtraction
        cpu.p = StatusFlags::CARRY | StatusFlags::DECIMAL;

        cpu.y = 6;
        cpu.a = 0x12;
        cpu.write_simple(0xBEF5, 0x21);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b01000000, 0b01000000);
        assert_eq!(cpu.p.bits & 0b00001000, 0b00001000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0);

        assert_eq!(cpu.a, 0x91);
    }

    pub fn test_0xFD_SBC_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        // Carry flag needs to be set when performing a subtraction
        cpu.p = StatusFlags::CARRY;

        cpu.x = 6;
        cpu.a = 23;
        cpu.write_simple(0xBEF5, 14);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b01000000, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);

        assert_eq!(cpu.a, 9);
    }

    pub fn test_0xFD_SBC_zero_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        // Carry flag needs to be set when performing a subtraction
        cpu.p = StatusFlags::CARRY;

        cpu.x = 6;
        cpu.a = 14;
        cpu.write_simple(0xBEF5, 14);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b01000000, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);

        assert_eq!(cpu.a, 0);
    }

    pub fn test_0xFD_SBC_overflow_negative_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        // Carry flag needs to be set when performing a subtraction
        cpu.p = StatusFlags::CARRY;

        cpu.x = 6;
        cpu.a = 14;
        cpu.write_simple(0xBEF5, 23);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b01000000, 0b01000000);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0);

        assert_eq!(cpu.a, 247);
    }

    pub fn test_0xFD_SBC_decimal_mode_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        // Carry flag needs to be set when performing a subtraction
        cpu.p = StatusFlags::CARRY | StatusFlags::DECIMAL;

        cpu.x = 6;
        cpu.a = 0x23;
        cpu.write_simple(0xBEF5, 0x14);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b01000000, 0);
        assert_eq!(cpu.p.bits & 0b00001000, 0b00001000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);

        assert_eq!(cpu.a, 0x9);
    }

    pub fn test_0xFD_SBC_decimal_mode_overflow_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        // Carry flag needs to be set when performing a subtraction
        cpu.p = StatusFlags::CARRY | StatusFlags::DECIMAL;

        cpu.x = 6;
        cpu.a = 0x12;
        cpu.write_simple(0xBEF5, 0x21);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b01000000, 0b01000000);
        assert_eq!(cpu.p.bits & 0b00001000, 0b00001000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0);

        assert_eq!(cpu.a, 0x91);
    }

    pub fn test_0xFE_INC_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.write_simple(0xBEF5, 0x0B);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 7);

        assert_eq!(cpu.read_simple(0xBEEF), 0x0C);
        assert_eq!(cpu.p.bits & 0b11111111, 0);
    }

    pub fn test_0xFE_INC_negative_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.write_simple(0xBEF5, 0b01111111);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 7);

        assert_eq!(cpu.read_simple(0xBEEF), 0b10000000);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0xFE_INC_zero_x<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, low_addr: u8, high_addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.write_simple(0xBEF5, 0xFF);
        f(&mut cpu, 0xEF, 0xBE);

        assert_eq!(cpu.pc, 3);
        assert_eq!(cpu.cycles, 7);

        assert_eq!(cpu.read_simple(0xBEEF), 0x00);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }
}

#[allow(non_snake_case)]
pub mod accumulator {
    use super::{super::*, *};

    pub fn test_0x0A_ASL_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 0b01010101;
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.a, 0b10101010);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x0A_ASL_carry_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 0b10000000;
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.a, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x2A_ROL_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::CARRY;

        cpu.a = 0b01010101;
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.a, 0b10101011);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x2A_ROL_carry_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 0b10000000;
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.a, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x4A_LSR<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 0b00000001;
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.a, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x6A_ROR_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::CARRY;

        cpu.a = 0b10101010;
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.a, 0b11010101);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x6A_ROR_carry_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 0b00000001;
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.a, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }
}

#[allow(non_snake_case)]
pub mod immediate {
    use super::{super::*, *};

    pub fn test_0x09_ORA_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b10101010;
        f(&mut cpu, 0b01010101);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.a, 0b11111111);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x09_ORA_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0;
        f(&mut cpu, 0);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.a, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x29_AND_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b11010101;
        f(&mut cpu, 0b10101010);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.a, 0b10000000);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x29_AND_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0;
        f(&mut cpu, 0b11111111);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.a, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x49_EOR_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b11010101;
        f(&mut cpu, 0b00101010);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.a, 0b11111111);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x49_EOR_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b11111111;
        f(&mut cpu, 0b11111111);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.a, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x69_ADC<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 11;
        f(&mut cpu, 11);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);
        assert_eq!(cpu.p.bits, 0);

        assert_eq!(cpu.a, 22);
    }

    pub fn test_0x69_ADC_overflow_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 250;
        f(&mut cpu, 6);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b01000000, 0b01000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
        assert_eq!(cpu.a, 0);
    }

    pub fn test_0x69_ADC_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b01111111;
        f(&mut cpu, 1);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b01000000, 0b01000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
        assert_eq!(cpu.a, 128);
    }

    pub fn test_0x69_ADC_decimal_mode<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::DECIMAL;

        cpu.a = 0x11;
        f(&mut cpu, 0x11);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);
        assert_eq!(cpu.p.bits, 0b00001000);

        assert_eq!(cpu.a, 0x22);
    }

    pub fn test_0x69_ADC_decimal_mode_carry<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::DECIMAL;

        cpu.a = 0x49;
        f(&mut cpu, 0x55);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b01000000, 0b01000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
        assert_eq!(cpu.a, 0x04);
    }

    pub fn test_0xA0_LDY<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 0xAB;
        f(&mut cpu, 0x7F);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.y, 0x7F);
        assert_eq!(cpu.p.bits, 0);
    }

    pub fn test_0xA0_LDY_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 0xAB;
        f(&mut cpu, 0xFF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.y, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
    }

    pub fn test_0xA0_LDY_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 0xAB;
        f(&mut cpu, 0);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.y, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0xA2_LDX<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 0xAB;
        f(&mut cpu, 0x7F);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.x, 0x7F);
        assert_eq!(cpu.p.bits, 0);
    }

    pub fn test_0xA2_LDX_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 0xAB;
        f(&mut cpu, 0xFF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.x, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
    }

    pub fn test_0xA2_LDX_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 0xAB;
        f(&mut cpu, 0);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.x, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0xA9_LDA<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 0xAB;
        f(&mut cpu, 0x7F);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.a, 0x7F);
        assert_eq!(cpu.p.bits, 0);
    }

    pub fn test_0xA9_LDA_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 0xAB;
        f(&mut cpu, 0xFF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.a, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
    }

    pub fn test_0xA9_LDA_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 0xAB;
        f(&mut cpu, 0);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.a, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0xC0_CPY_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 0x0F;
        f(&mut cpu, 0xFF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.y, 0x0F);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
    }

    pub fn test_0xC0_CPY_zero_carry<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 0xFF;
        f(&mut cpu, 0xFF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.y, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
    }

    pub fn test_0xC0_CPY_carry<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 0xFF;
        f(&mut cpu, 0xF0);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.y, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
    }

    pub fn test_0xC9_CMP_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 0x0F;
        f(&mut cpu, 0xFF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.a, 0x0F);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
    }

    pub fn test_0xC9_CMP_zero_carry<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 0xFF;
        f(&mut cpu, 0xFF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.a, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
    }

    pub fn test_0xC9_CMP_carry<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 0xFF;
        f(&mut cpu, 0xF0);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.a, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
    }

    pub fn test_0xE0_CPX_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 0x0F;
        f(&mut cpu, 0xFF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.x, 0x0F);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
    }

    pub fn test_0xE0_CPX_zero_carry<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 0xFF;
        f(&mut cpu, 0xFF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.x, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
    }

    pub fn test_0xE0_CPX_carry<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 0xFF;
        f(&mut cpu, 0xF0);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.x, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
    }

    pub fn test_0xE9_SBC<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        // Carry flag needs to be set when performing a subtraction
        cpu.p = StatusFlags::CARRY;

        cpu.a = 23;
        f(&mut cpu, 14);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.p.bits & 0b01000000, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);

        assert_eq!(cpu.a, 9);
    }

    pub fn test_0xE9_SBC_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        // Carry flag needs to be set when performing a subtraction
        cpu.p = StatusFlags::CARRY;

        cpu.a = 14;
        f(&mut cpu, 14);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.p.bits & 0b01000000, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);

        assert_eq!(cpu.a, 0);
    }

    pub fn test_0xE9_SBC_overflow_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        // Carry flag needs to be set when performing a subtraction
        cpu.p = StatusFlags::CARRY;

        cpu.a = 14;
        f(&mut cpu, 23);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.p.bits & 0b01000000, 0b01000000);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0);

        assert_eq!(cpu.a, 247);
    }

    pub fn test_0xE9_SBC_decimal_mode<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        // Carry flag needs to be set when performing a subtraction
        cpu.p = StatusFlags::CARRY | StatusFlags::DECIMAL;

        cpu.a = 0x23;
        f(&mut cpu, 0x14);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b01000000, 0);
        assert_eq!(cpu.p.bits & 0b00001000, 0b00001000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);

        assert_eq!(cpu.a, 0x9);
    }

    pub fn test_0xE9_SBC_decimal_mode_overflow<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, value: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        // Carry flag needs to be set when performing a subtraction
        cpu.p = StatusFlags::CARRY | StatusFlags::DECIMAL;

        cpu.a = 0x12;
        f(&mut cpu, 0x21);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b01000000, 0b01000000);
        assert_eq!(cpu.p.bits & 0b00001000, 0b00001000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0);

        assert_eq!(cpu.a, 0x91);
    }
}

#[allow(non_snake_case)]
pub mod implicit {
    use super::{super::*, *};

    // No good test
    pub fn test_0x00_BRK<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.sp = 16;
        cpu.cycles = 0;

        cpu.p = StatusFlags::empty();
        f(&mut cpu);

        assert_eq!(cpu.pc, 0);
        assert_eq!(cpu.cycles, 7);
        assert_eq!(cpu.read_simple(16), 0);
        assert_eq!(cpu.read_simple(15), 0);
        assert_eq!(cpu.read_simple(14), 0);
        assert_eq!(cpu.sp, 13);
        assert_eq!(cpu.p.bits & 0b00010000, 0b00010000);
    }

    pub fn test_0x08_PHP<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.sp = 16;
        cpu.cycles = 0;

        cpu.p = StatusFlags::all();
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 3);
        assert_eq!(cpu.read_simple(0x0100 + 16), StatusFlags::all().bits);
        assert_eq!(cpu.sp, 15);
        assert_eq!(cpu.p, StatusFlags::all());
    }

    pub fn test_0x18_CLC<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.p = StatusFlags::all();
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.p.bits & 0b00000001, 0);
    }

    pub fn test_0x28_PLP<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.sp = 16;
        cpu.cycles = 0;

        cpu.p = StatusFlags::empty();
        cpu.write_simple(0x0100 + cpu.sp as u16 + 1, 0b10101010);
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.sp, 17);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits, 0b10101010);
    }

    pub fn test_0x38_SEC<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.p = StatusFlags::empty();
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.p.bits & 0b00000001, 1);
    }

    pub fn test_0x40_RTI<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.sp = 16;

        cpu.p = StatusFlags::empty();

        cpu.write_simple(0x0100 + cpu.sp as u16 + 1, 0b10101010);
        cpu.write_simple(0x0100 + cpu.sp as u16 + 2, 0x12);
        cpu.write_simple(0x0100 + cpu.sp as u16 + 3, 0x34);
        f(&mut cpu);

        assert_eq!(cpu.pc, 0x3412);
        assert_eq!(cpu.sp, 19);
        assert_eq!(cpu.cycles, 6);

        assert_eq!(cpu.p.bits, 0b10101010);
    }

    pub fn test_0x48_PHA<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.sp = 16;
        cpu.cycles = 0;

        cpu.a = 42;
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 3);
        assert_eq!(cpu.read_simple(0x0100 + 16), 42);
        assert_eq!(cpu.sp, 15);
        assert_eq!(cpu.a, 42);
    }

    pub fn test_0x58_CLI<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.p = StatusFlags::all();
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.p.bits & 0b00000100, 0);
    }

    pub fn test_0x60_RTS<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.sp = 16;

        cpu.write_simple(0x0100 + cpu.sp as u16 + 1, 0x12);
        cpu.write_simple(0x0100 + cpu.sp as u16 + 2, 0x34);
        f(&mut cpu);

        assert_eq!(cpu.pc, 0x3413);
        assert_eq!(cpu.sp, 18);
        assert_eq!(cpu.cycles, 6);
    }

    pub fn test_0x68_PLA_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.sp = 16;
        cpu.cycles = 0;

        cpu.a = 0;
        cpu.write_simple(0x0100 + cpu.sp as u16 + 1, 0b10101010);
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.sp, 17);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0b10101010);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x68_PLA_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.sp = 16;
        cpu.cycles = 0;

        cpu.a = 0xFF;
        cpu.write_simple(cpu.sp as u16 + 1, 0);
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.sp, 17);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x78_SEI<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.p = StatusFlags::empty();
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.p.bits & 0b00000100, 0b00000100);
    }

    pub fn test_0x88_DEY<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.y = 12;
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.y, 11);
    }

    pub fn test_0x88_DEY_overflow_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.y = 0;
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.y, u8::MAX);
    }

    pub fn test_0x88_DEY_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.y = 1;
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
        assert_eq!(cpu.y, 0);
    }

    pub fn test_0x8A_TXA_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.x = 0b10101010;
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.x, 0b10101010);
        assert_eq!(cpu.a, cpu.x);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x8A_TXA_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.x = 0;
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.x, 0);
        assert_eq!(cpu.a, cpu.x);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x98_TYA_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.y = 0b10101010;
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.y, 0b10101010);
        assert_eq!(cpu.a, cpu.y);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x98_TYA_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.y = 0;
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.y, 0);
        assert_eq!(cpu.a, cpu.y);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x9A_TXS<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.x = 12;
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.x, 12);
        assert_eq!(cpu.sp, cpu.x);
    }

    pub fn test_0xA8_TAY_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b10101010;
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.a, 0b10101010);
        assert_eq!(cpu.a, cpu.y);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0xA8_TAY_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0;
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.a, 0);
        assert_eq!(cpu.a, cpu.y);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0xB8_CLV<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.p = StatusFlags::all();
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.p.bits & 0b01000000, 0);
    }

    pub fn test_0xC8_INY<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.y = 11;
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.y, 12);
    }

    pub fn test_0xC8_INY_overflow_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.y = 255;
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
        assert_eq!(cpu.y, 0);
    }

    pub fn test_0xC8_INY_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.y = 0b01111111;
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.y, 128);
    }

    pub fn test_0xCA_DEX<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.x = 12;
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.x, 11);
    }

    pub fn test_0xCA_DEX_overflow_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.x = 0;
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.x, u8::MAX);
    }

    pub fn test_0xCA_DEX_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.x = 1;
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
        assert_eq!(cpu.x, 0);
    }

    pub fn test_0xD8_CLD<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.p = StatusFlags::all();
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.p.bits & 0b00001000, 0);
    }

    pub fn test_0xE8_INX<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.x = 11;
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.x, 12);
    }

    pub fn test_0xE8_INX_overflow_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.x = 255;
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
        assert_eq!(cpu.x, 0);
    }

    pub fn test_0xE8_INX_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.x = 0b01111111;
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.x, 128);
    }

    pub fn test_0xEA_NOP<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);
    }

    pub fn test_0xF8_SED<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.p = StatusFlags::empty();
        f(&mut cpu);

        assert_eq!(cpu.pc, 1);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.p.bits & 0b00001000, 0b00001000);
    }
}

#[allow(non_snake_case)]
pub mod relative {
    use super::{super::*, *};

    #[inline]
    fn test_no_branch<const DECIMAL_SUPPORT: bool>(
        bit: u8,
        set: bool,
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, offset: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        let flag = (set as u8) << bit;

        cpu.p = StatusFlags::from_bits(flag).unwrap();
        f(&mut cpu, 0xFF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        if set {
            assert_eq!(cpu.p.bits & flag, flag);
        } else {
            assert_eq!(cpu.p.bits & 0b11111111, 0);
        }
    }

    #[inline]
    fn test_branch_same_page<const DECIMAL_SUPPORT: bool>(
        bit: u8,
        set: bool,
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, offset: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        let flag = (set as u8) << bit;

        cpu.p = StatusFlags::from_bits(flag).unwrap();
        f(&mut cpu, 0xFE);

        assert_eq!(cpu.pc, 0xFE);
        assert_eq!(cpu.cycles, 3);

        if set {
            assert_eq!(cpu.p.bits & flag, flag);
        } else {
            assert_eq!(cpu.p.bits & 0b11111111, 0);
        }
    }

    #[inline]
    fn test_branch_different_page<const DECIMAL_SUPPORT: bool>(
        bit: u8,
        set: bool,
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, offset: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 1;
        cpu.cycles = 0;

        let flag = (set as u8) << bit;

        cpu.p = StatusFlags::from_bits(flag).unwrap();
        f(&mut cpu, 0xFF);

        assert_eq!(cpu.pc, 0x0100);
        assert_eq!(cpu.cycles, 4);

        if set {
            assert_eq!(cpu.p.bits & flag, flag);
        } else {
            assert_eq!(cpu.p.bits & 0b11111111, 0);
        }
    }

    pub fn test_0x10_BPL_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, offset: u8),
    ) {
        test_no_branch(7, true, f);
    }

    pub fn test_0x10_BPL_positive_same_page<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, offset: u8),
    ) {
        test_branch_same_page(7, false, f);
    }

    pub fn test_0x10_BPL_positive_different_page<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, offset: u8),
    ) {
        test_branch_different_page(7, false, f);
    }

    pub fn test_0x30_BMI_positive<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, offset: u8),
    ) {
        test_no_branch(7, false, f);
    }

    pub fn test_0x30_BMI_negative_same_page<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, offset: u8),
    ) {
        test_branch_same_page(7, true, f);
    }

    pub fn test_0x30_BMI_negative_different_page<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, offset: u8),
    ) {
        test_branch_different_page(7, true, f);
    }

    pub fn test_0x50_BVC_overflow<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, offset: u8),
    ) {
        test_no_branch(6, true, f);
    }

    pub fn test_0x50_BVC_no_overflow_same_page<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, offset: u8),
    ) {
        test_branch_same_page(6, false, f);
    }

    pub fn test_0x50_BVC_no_overflow_different_page<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, offset: u8),
    ) {
        test_branch_different_page(6, false, f);
    }

    pub fn test_0x70_BVS_no_overflow<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, offset: u8),
    ) {
        test_no_branch(6, false, f);
    }

    pub fn test_0x70_BVS_overflow_same_page<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, offset: u8),
    ) {
        test_branch_same_page(6, true, f);
    }

    pub fn test_0x70_BVS_overflow_different_page<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, offset: u8),
    ) {
        test_branch_different_page(6, true, f);
    }

    pub fn test_0x90_BCC_carry<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, offset: u8),
    ) {
        test_no_branch(0, true, f);
    }

    pub fn test_0x90_BCC_no_carry_same_page<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, offset: u8),
    ) {
        test_branch_same_page(0, false, f);
    }

    pub fn test_0x90_BCC_no_carry_different_page<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, offset: u8),
    ) {
        test_branch_different_page(0, false, f);
    }

    pub fn test_0xB0_BCS_no_carry<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, offset: u8),
    ) {
        test_no_branch(0, false, f);
    }

    pub fn test_0xB0_BCS_carry_same_page<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, offset: u8),
    ) {
        test_branch_same_page(0, true, f);
    }

    pub fn test_0xB0_BCS_carry_different_page<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, offset: u8),
    ) {
        test_branch_different_page(0, true, f);
    }

    pub fn test_0xD0_BNE_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, offset: u8),
    ) {
        test_no_branch(1, true, f);
    }

    pub fn test_0xD0_BNE_non_zero_same_page<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, offset: u8),
    ) {
        test_branch_same_page(1, false, f);
    }

    pub fn test_0xD0_BNE_non_zero_different_page<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, offset: u8),
    ) {
        test_branch_different_page(1, false, f);
    }

    pub fn test_0xF0_BEQ_non_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, offset: u8),
    ) {
        test_no_branch(1, false, f);
    }

    pub fn test_0xF0_BEQ_zero_same_page<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, offset: u8),
    ) {
        test_branch_same_page(1, true, f);
    }

    pub fn test_0xF0_BEQ_zero_different_page<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, offset: u8),
    ) {
        test_branch_different_page(1, true, f);
    }
}

#[allow(non_snake_case)]
pub mod zero_page {
    use super::{super::*, *};

    pub fn test_0x05_ORA_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b10101010;
        cpu.write_simple(0xEF, 0b01010101);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.a, 0b11111111);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x05_ORA_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0;
        cpu.write_simple(0xEF, 0);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.a, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x06_ASL_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.write_simple(0xEF, 0b01010101);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 5);

        assert_eq!(cpu.read_simple(0xEF), 0b10101010);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x06_ASL_carry_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.write_simple(0xEF, 0b10000000);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 5);

        assert_eq!(cpu.read_simple(0xEF), 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x24_BIT_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b10101010;
        cpu.write_simple(0xEF, 0b10000000);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b01000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x24_BIT_overflow<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b01010101;
        cpu.write_simple(0xEF, 0b01000000);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b01000000, 0b01000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x24_BIT_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b10101010;
        cpu.write_simple(0xEF, 0b01010101);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b01000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x25_AND_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b11010101;
        cpu.write_simple(0xEF, 0b10101010);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.a, 0b10000000);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x25_AND_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0;
        cpu.write_simple(0xEF, 0b11111111);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 2);

        assert_eq!(cpu.a, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x26_ROL_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::CARRY;

        cpu.write_simple(0xEF, 0b01010101);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 5);

        assert_eq!(cpu.read_simple(0xEF), 0b10101011);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x26_ROL_carry_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.write_simple(0xEF, 0b10000000);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 5);

        assert_eq!(cpu.read_simple(0xEF), 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x45_EOR_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b11010101;
        cpu.write_simple(0xEF, 0b00101010);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.a, 0b11111111);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x45_EOR_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b11111111;
        cpu.write_simple(0xEF, 0b11111111);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.a, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x46_LSR<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.write_simple(0xEF, 0b00000001);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 5);

        assert_eq!(cpu.read_simple(0xEF), 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x65_ADC<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 11;
        cpu.write_simple(0xEF, 11);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.p.bits, 0);
        assert_eq!(cpu.a, 22);
    }

    pub fn test_0x65_ADC_overflow_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 250;
        cpu.write_simple(0xEF, 6);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b01000000, 0b01000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
        assert_eq!(cpu.a, 0);
    }

    pub fn test_0x65_ADC_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b01111111;
        cpu.write_simple(0xEF, 1);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b01000000, 0b01000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
        assert_eq!(cpu.a, 128);
    }

    pub fn test_0x65_ADC_decimal_mode<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::DECIMAL;

        cpu.a = 0x11;
        cpu.write_simple(0xEF, 0x11);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.p.bits, 0b00001000);
        assert_eq!(cpu.a, 0x22);
    }

    pub fn test_0x65_ADC_decimal_mode_carry<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::DECIMAL;

        cpu.a = 0x49;
        cpu.write_simple(0xEF, 0x55);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b01000000, 0b01000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
        assert_eq!(cpu.a, 0x04);
    }

    pub fn test_0x66_ROR_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::CARRY;

        cpu.write_simple(0xEF, 0b10101010);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 5);

        assert_eq!(cpu.read_simple(0xEF), 0b11010101);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x66_ROR_carry_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.write_simple(0xEF, 0b00000001);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 5);

        assert_eq!(cpu.read_simple(0xEF), 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x84_STY<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 42;
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.read_simple(0xEF), 42);
        assert_eq!(cpu.p.bits, 0);
    }

    pub fn test_0x85_STA<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 42;
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.read_simple(0xEF), 42);
        assert_eq!(cpu.p.bits, 0);
    }

    pub fn test_0x86_STX<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 42;
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.read_simple(0xEF), 42);
        assert_eq!(cpu.p.bits, 0);
    }

    pub fn test_0xA4_LDY<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 0xAB;
        cpu.write_simple(0xEF, 0x7F);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.y, 0x7F);
        assert_eq!(cpu.p.bits, 0);
    }

    pub fn test_0xA4_LDY_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 0xAB;
        cpu.write_simple(0xEF, 0xFF);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.y, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
    }

    pub fn test_0xA4_LDY_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 0xAB;
        cpu.write_simple(0xEF, 0);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.y, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0xA5_LDA<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 0xAB;
        cpu.write_simple(0xEF, 0x7F);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.a, 0x7F);
        assert_eq!(cpu.p.bits, 0);
    }

    pub fn test_0xA5_LDA_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 0xAB;
        cpu.write_simple(0xEF, 0xFF);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.a, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
    }

    pub fn test_0xA5_LDA_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 0xAB;
        cpu.write_simple(0xEF, 0);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.a, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0xA6_LDX<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 0xAB;
        cpu.write_simple(0xEF, 0x7F);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.x, 0x7F);
        assert_eq!(cpu.p.bits, 0);
    }

    pub fn test_0xA6_LDX_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 0xAB;
        cpu.write_simple(0xEF, 0xFF);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.x, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
    }

    pub fn test_0xA6_LDX_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 0xAB;
        cpu.write_simple(0xEF, 0);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.x, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0xC4_CPY_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 0x0F;
        cpu.write_simple(0xEF, 0xFF);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.y, 0x0F);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
    }

    pub fn test_0xC4_CPY_zero_carry<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 0xFF;
        cpu.write_simple(0xEF, 0xFF);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.y, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
    }

    pub fn test_0xC4_CPY_carry<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 0xFF;
        cpu.write_simple(0xEF, 0xF0);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.y, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
    }

    pub fn test_0xC5_CMP_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 0x0F;
        cpu.write_simple(0xEF, 0xFF);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.a, 0x0F);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
    }

    pub fn test_0xC5_CMP_zero_carry<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 0xFF;
        cpu.write_simple(0xEF, 0xFF);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.a, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
    }

    pub fn test_0xC5_CMP_carry<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 0xFF;
        cpu.write_simple(0xEF, 0xF0);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.a, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
    }

    pub fn test_0xC6_DEC<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.write_simple(0xEF, 0x0B);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 5);

        assert_eq!(cpu.read_simple(0xEF), 0x0A);
        assert_eq!(cpu.p.bits & 0b11111111, 0);
    }

    pub fn test_0xC6_DEC_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.write_simple(0xEF, 0x00);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 5);

        assert_eq!(cpu.read_simple(0xEF), 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0xC6_DEC_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.write_simple(0xEF, 0x01);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 5);

        assert_eq!(cpu.read_simple(0xEF), 0x00);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0xE4_CPX_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 0x0F;
        cpu.write_simple(0xEF, 0xFF);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.x, 0x0F);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
    }

    pub fn test_0xE4_CPX_zero_carry<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 0xFF;
        cpu.write_simple(0xEF, 0xFF);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.x, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
    }

    pub fn test_0xE4_CPX_carry<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 0xFF;
        cpu.write_simple(0xEF, 0xF0);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.x, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
    }

    pub fn test_0xE5_SBC<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        // Carry flag needs to be set when performing a subtraction
        cpu.p = StatusFlags::CARRY;

        cpu.a = 23;
        cpu.write_simple(0xEF, 14);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.p.bits & 0b01000000, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);

        assert_eq!(cpu.a, 9);
    }

    pub fn test_0xE5_SBC_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        // Carry flag needs to be set when performing a subtraction
        cpu.p = StatusFlags::CARRY;

        cpu.a = 14;
        cpu.write_simple(0xEF, 14);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.p.bits & 0b01000000, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);

        assert_eq!(cpu.a, 0);
    }

    pub fn test_0xE5_SBC_overflow_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        // Carry flag needs to be set when performing a subtraction
        cpu.p = StatusFlags::CARRY;

        cpu.a = 14;
        cpu.write_simple(0xEF, 23);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.p.bits & 0b01000000, 0b01000000);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0);

        assert_eq!(cpu.a, 247);
    }

    pub fn test_0xE5_SBC_decimal_mode<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        // Carry flag needs to be set when performing a subtraction
        cpu.p = StatusFlags::CARRY | StatusFlags::DECIMAL;

        cpu.a = 0x23;
        cpu.write_simple(0xEF, 0x14);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b01000000, 0);
        assert_eq!(cpu.p.bits & 0b00001000, 0b00001000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);

        assert_eq!(cpu.a, 0x9);
    }

    pub fn test_0xE5_SBC_decimal_mode_overflow<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        // Carry flag needs to be set when performing a subtraction
        cpu.p = StatusFlags::CARRY | StatusFlags::DECIMAL;

        cpu.a = 0x12;
        cpu.write_simple(0xEF, 0x21);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b01000000, 0b01000000);
        assert_eq!(cpu.p.bits & 0b00001000, 0b00001000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0);

        assert_eq!(cpu.a, 0x91);
    }

    pub fn test_0xE6_INC<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.write_simple(0xEF, 0x0B);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 5);

        assert_eq!(cpu.read_simple(0xEF), 0x0C);
        assert_eq!(cpu.p.bits & 0b11111111, 0);
    }

    pub fn test_0xE6_INC_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.write_simple(0xEF, 0b01111111);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 5);

        assert_eq!(cpu.read_simple(0xEF), 0b10000000);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0xE6_INC_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.write_simple(0xEF, 0xFF);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 5);

        assert_eq!(cpu.read_simple(0xEF), 0x00);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }
}

#[allow(non_snake_case)]
pub mod zero_page_indexed {
    use super::{super::*, *};

    pub fn test_0x15_ORA_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.x = 6;
        cpu.a = 0b10101010;
        cpu.write_simple(0xF5, 0b01010101);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.a, 0b11111111);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x15_ORA_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0;
        cpu.x = 6;
        cpu.write_simple(0xF5, 0);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.a, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x16_ASL_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.write_simple(0xF5, 0b01010101);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 6);

        assert_eq!(cpu.read_simple(0xEF), 0b10101010);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x16_ASL_carry_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.write_simple(0xF5, 0b10000000);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 6);

        assert_eq!(cpu.read_simple(0xEF), 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x35_AND_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b11010101;
        cpu.x = 6;
        cpu.write_simple(0xF5, 0b10101010);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.a, 0b10000000);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x35_AND_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0;
        cpu.x = 6;
        cpu.write_simple(0xF5, 0b11111111);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 3);

        assert_eq!(cpu.a, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x36_ROL_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::CARRY;

        cpu.x = 6;
        cpu.write_simple(0xF5, 0b01010101);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 6);

        assert_eq!(cpu.read_simple(0xEF), 0b10101011);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x36_ROL_carry_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.write_simple(0xF5, 0b10000000);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 6);

        assert_eq!(cpu.read_simple(0xEF), 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x55_EOR_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b11010101;
        cpu.x = 6;
        cpu.write_simple(0xF5, 0b00101010);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0b11111111);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x55_EOR_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b11111111;
        cpu.x = 6;
        cpu.write_simple(0xF5, 0b11111111);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x56_LSR<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.write_simple(0xF5, 0b00000001);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 6);

        assert_eq!(cpu.read_simple(0xEF), 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x75_ADC<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 11;
        cpu.x = 6;
        cpu.write_simple(0xF5, 11);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits, 0);
        assert_eq!(cpu.a, 22);
    }

    pub fn test_0x75_ADC_overflow_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 250;
        cpu.x = 6;
        cpu.write_simple(0xF5, 6);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b01000000, 0b01000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
        assert_eq!(cpu.a, 0);
    }

    pub fn test_0x75_ADC_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;

        cpu.a = 0b01111111;
        cpu.x = 6;
        cpu.write_simple(0xF5, 1);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b01000000, 0b01000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
        assert_eq!(cpu.a, 128);
    }

    pub fn test_0x75_ADC_decimal_mode<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::DECIMAL;

        cpu.a = 0x11;
        cpu.x = 6;
        cpu.write_simple(0xF5, 0x11);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits, 0b00001000);
        assert_eq!(cpu.a, 0x22);
    }

    pub fn test_0x75_ADC_decimal_mode_carry<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::DECIMAL;

        cpu.a = 0x49;
        cpu.x = 6;
        cpu.write_simple(0xF5, 0x55);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b01000000, 0b01000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
        assert_eq!(cpu.a, 0x04);
    }

    pub fn test_0x76_ROR_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::CARRY;

        cpu.x = 6;
        cpu.write_simple(0xF5, 0b10101010);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 6);

        assert_eq!(cpu.read_simple(0xEF), 0b11010101);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0x76_ROR_carry_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.write_simple(0xF5, 0b00000001);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 6);

        assert_eq!(cpu.read_simple(0xEF), 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0x94_STY<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 42;
        cpu.x = 6;
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.read_simple(0xF5), 42);
        assert_eq!(cpu.p.bits, 0);
    }

    pub fn test_0x95_STA<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 42;
        cpu.x = 6;
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.read_simple(0xF5), 42);
        assert_eq!(cpu.p.bits, 0);
    }

    pub fn test_0x96_STX<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 42;
        cpu.y = 6;
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.read_simple(0xF5), 42);
        assert_eq!(cpu.p.bits, 0);
    }

    pub fn test_0xB4_LDY<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 0xAB;
        cpu.x = 6;
        cpu.write_simple(0xF5, 0x7F);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.y, 0x7F);
        assert_eq!(cpu.p.bits, 0);
    }

    pub fn test_0xB4_LDY_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 0xAB;
        cpu.x = 6;
        cpu.write_simple(0xF5, 0xFF);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.y, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
    }

    pub fn test_0xB4_LDY_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.y = 0xAB;
        cpu.x = 6;
        cpu.write_simple(0xF5, 0);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.y, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0xB5_LDA<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 0xAB;
        cpu.x = 6;
        cpu.write_simple(0xF5, 0x7F);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0x7F);
        assert_eq!(cpu.p.bits, 0);
    }

    pub fn test_0xB5_LDA_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 0xAB;
        cpu.x = 6;
        cpu.write_simple(0xF5, 0xFF);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
    }

    pub fn test_0xB5_LDA_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 0xAB;
        cpu.x = 6;
        cpu.write_simple(0xF5, 0);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0xB6_LDX<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 0xAB;
        cpu.y = 6;
        cpu.write_simple(0xF5, 0x7F);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.x, 0x7F);
        assert_eq!(cpu.p.bits, 0);
    }

    pub fn test_0xB6_LDX_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 0xAB;
        cpu.y = 6;
        cpu.write_simple(0xF5, 0xFF);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.x, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
    }

    pub fn test_0xB6_LDX_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 0xAB;
        cpu.y = 6;
        cpu.write_simple(0xF5, 0);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.x, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0xD5_CMP_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 0x0F;
        cpu.x = 6;
        cpu.write_simple(0xF5, 0xFF);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0x0F);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0);
    }

    pub fn test_0xD5_CMP_zero_carry<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 0xFF;
        cpu.x = 6;
        cpu.write_simple(0xF5, 0xFF);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
    }

    pub fn test_0xD5_CMP_carry<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.a = 0xFF;
        cpu.x = 6;
        cpu.write_simple(0xF5, 0xF0);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.a, 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);
    }

    pub fn test_0xD6_DEC<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.write_simple(0xF5, 0x0B);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 6);

        assert_eq!(cpu.read_simple(0xEF), 0x0A);
        assert_eq!(cpu.p.bits & 0b11111111, 0);
    }

    pub fn test_0xD6_DEC_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.write_simple(0xF5, 0x00);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 6);

        assert_eq!(cpu.read_simple(0xEF), 0xFF);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0xD6_DEC_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.write_simple(0xF5, 0x01);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 6);

        assert_eq!(cpu.read_simple(0xEF), 0x00);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }

    pub fn test_0xF5_SBC<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        // Carry flag needs to be set when performing a subtraction
        cpu.p = StatusFlags::CARRY;

        cpu.a = 23;
        cpu.x = 6;
        cpu.write_simple(0xF5, 14);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b01000000, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);

        assert_eq!(cpu.a, 9);
    }

    pub fn test_0xF5_SBC_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        // Carry flag needs to be set when performing a subtraction
        cpu.p = StatusFlags::CARRY;

        cpu.a = 14;
        cpu.x = 6;
        cpu.write_simple(0xF5, 14);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b01000000, 0);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);

        assert_eq!(cpu.a, 0);
    }

    pub fn test_0xF5_SBC_overflow_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        // Carry flag needs to be set when performing a subtraction
        cpu.p = StatusFlags::CARRY;

        cpu.a = 14;
        cpu.x = 6;
        cpu.write_simple(0xF5, 23);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b01000000, 0b01000000);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0);

        assert_eq!(cpu.a, 247);
    }

    pub fn test_0xF5_SBC_decimal_mode<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        // Carry flag needs to be set when performing a subtraction
        cpu.p = StatusFlags::CARRY | StatusFlags::DECIMAL;

        cpu.a = 0x23;
        cpu.x = 6;
        cpu.write_simple(0xF5, 0x14);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b01000000, 0);
        assert_eq!(cpu.p.bits & 0b00001000, 0b00001000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0b00000001);

        assert_eq!(cpu.a, 0x9);
    }

    pub fn test_0xF5_SBC_decimal_mode_overflow<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        // Carry flag needs to be set when performing a subtraction
        cpu.p = StatusFlags::CARRY | StatusFlags::DECIMAL;

        cpu.a = 0x12;
        cpu.x = 6;
        cpu.write_simple(0xF5, 0x21);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 4);

        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b01000000, 0b01000000);
        assert_eq!(cpu.p.bits & 0b00001000, 0b00001000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
        assert_eq!(cpu.p.bits & 0b00000001, 0);

        assert_eq!(cpu.a, 0x91);
    }

    pub fn test_0xF6_INC<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.write_simple(0xF5, 0x0B);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 6);

        assert_eq!(cpu.read_simple(0xEF), 0x0C);
        assert_eq!(cpu.p.bits & 0b11111111, 0);
    }

    pub fn test_0xF6_INC_negative<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.write_simple(0xF5, 0b01111111);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 6);

        assert_eq!(cpu.read_simple(0xEF), 0b10000000);
        assert_eq!(cpu.p.bits & 0b10000000, 0b10000000);
        assert_eq!(cpu.p.bits & 0b00000010, 0);
    }

    pub fn test_0xF6_INC_zero<const DECIMAL_SUPPORT: bool>(
        f: fn(&mut MOS6502<DECIMAL_SUPPORT, TestDataBus>, addr: u8),
    ) {
        let mut cpu =
            MOS6502::<DECIMAL_SUPPORT, TestDataBus>::new(Rc::new(RefCell::new(TestDataBus::new())));
        cpu.pc = 0;
        cpu.cycles = 0;
        cpu.p = StatusFlags::empty();

        cpu.x = 6;
        cpu.write_simple(0xF5, 0xFF);
        f(&mut cpu, 0xEF);

        assert_eq!(cpu.pc, 2);
        assert_eq!(cpu.cycles, 6);

        assert_eq!(cpu.read_simple(0xEF), 0x00);
        assert_eq!(cpu.p.bits & 0b10000000, 0);
        assert_eq!(cpu.p.bits & 0b00000010, 0b00000010);
    }
}
