use super::*;

#[allow(non_snake_case)]
impl<const DECIMAL_SUPPORT: bool, Bus: DataBus> MOS6502<DECIMAL_SUPPORT, Bus> {
    // No good implementation
    pub fn op_0x00_BRK_simulate_interrupt_request(&mut self) {
        self.pc += 1;

        self.stack_push(self.pc);

        let mut flags = self.p;
        flags.insert(StatusFlags::BREAK);
        flags.insert(StatusFlags::UNUSED);

        self.stack_push(self.pc);
        self.stack_push(flags.bits);

        self.p.insert(StatusFlags::INTERRUPT_DISABLE);

        //self.pc = self.read::<u16>(0xFFFE);
        self.cycles += 7;
    }

    pub fn op_0x08_PHP_push_p_to_stack(&mut self) {
        self.stack_push(self.p.bits);

        self.pc += 1;
        self.cycles += 3;
    }

    pub fn op_0x18_CLC_clear_carry(&mut self) {
        self.p.remove(StatusFlags::CARRY);

        self.pc += 1;
        self.cycles += 2;
    }

    pub fn op_0x28_PLP_pull_p_from_stack(&mut self) {
        self.p = StatusFlags::from_bits(self.stack_pop::<u8>()).unwrap();

        self.pc += 1;
        self.cycles += 4;
    }

    pub fn op_0x38_SEC_set_carry(&mut self) {
        self.p.insert(StatusFlags::CARRY);

        self.pc += 1;
        self.cycles += 2;
    }

    pub fn op_0x40_RTI_return_from_interrupt(&mut self) {
        self.p = StatusFlags::from_bits(self.stack_pop::<u8>()).unwrap();
        self.pc = self.stack_pop::<u16>();

        self.cycles += 6;
    }

    pub fn op_0x48_PHA_push_a_to_stack(&mut self) {
        self.stack_push(self.a);

        self.pc += 1;
        self.cycles += 3;
    }

    pub fn op_0x58_CLI_clear_interrupt(&mut self) {
        self.p.remove(StatusFlags::INTERRUPT_DISABLE);

        self.pc += 1;
        self.cycles += 2;
    }

    pub fn op_0x60_RTS_return_from_subroutine(&mut self) {
        self.pc = self.stack_pop::<u16>();

        self.pc += 1;
        self.cycles += 6;
    }

    pub fn op_0x68_PLA_pull_a_from_stack(&mut self) {
        self.a = self.stack_pop::<u8>();
        self.p.set(StatusFlags::NEGATIVE, self.a & 0b10000000 > 0);
        self.p.set(StatusFlags::ZERO, self.a == 0);

        self.pc += 1;
        self.cycles += 4;
    }

    pub fn op_0x78_SEI_set_interrupt_disable(&mut self) {
        self.p.insert(StatusFlags::INTERRUPT_DISABLE);

        self.pc += 1;
        self.cycles += 2;
    }

    pub fn op_0x88_DEY_decrement_y(&mut self) {
        let (new_y, _) = self.y.overflowing_sub(1);
        self.y = new_y;
        self.p.set(StatusFlags::ZERO, self.y == 0);
        self.p.set(StatusFlags::NEGATIVE, self.y & 0b10000000 > 0);

        self.pc += 1;
        self.cycles += 2;
    }

    pub fn op_0x8A_TXA_transfer_x_to_a(&mut self) {
        self.a = self.x;
        self.p.set(StatusFlags::ZERO, self.a == 0);
        self.p.set(StatusFlags::NEGATIVE, self.a & 0b10000000 > 0);

        self.pc += 1;
        self.cycles += 2;
    }

    pub fn op_0x98_TYA_transfer_y_to_a(&mut self) {
        self.a = self.y;
        self.p.set(StatusFlags::ZERO, self.a == 0);
        self.p.set(StatusFlags::NEGATIVE, self.a & 0b10000000 > 0);

        self.pc += 1;
        self.cycles += 2;
    }

    pub fn op_0x9A_TXS_transfer_x_to_sp(&mut self) {
        self.sp = self.x;

        self.pc += 1;
        self.cycles += 2;
    }

    pub fn op_0xA8_TAY_transfer_a_to_y(&mut self) {
        self.y = self.a;
        self.p.set(StatusFlags::ZERO, self.y == 0);
        self.p.set(StatusFlags::NEGATIVE, self.y & 0b10000000 > 0);

        self.pc += 1;
        self.cycles += 2;
    }

    pub fn op_0xB8_CLV_clear_overflow(&mut self) {
        self.p.remove(StatusFlags::OVERFLOW);

        self.pc += 1;
        self.cycles += 2;
    }

    pub fn op_0xC8_INY_increment_y(&mut self) {
        let (new_y, _) = self.y.overflowing_add(1);
        self.y = new_y;
        self.p.set(StatusFlags::ZERO, self.y == 0);
        self.p.set(StatusFlags::NEGATIVE, self.y & 0b10000000 > 0);

        self.pc += 1;
        self.cycles += 2;
    }

    pub fn op_0xCA_DEX_decrement_x(&mut self) {
        let (new_x, _) = self.x.overflowing_sub(1);
        self.x = new_x;
        self.p.set(StatusFlags::ZERO, self.x == 0);
        self.p.set(StatusFlags::NEGATIVE, self.x & 0b10000000 > 0);

        self.pc += 1;
        self.cycles += 2;
    }

    pub fn op_0xD8_CLD_clear_decimal_flag(&mut self) {
        if DECIMAL_SUPPORT {
            self.p.remove(StatusFlags::DECIMAL);
        }

        self.pc += 1;
        self.cycles += 2;
    }

    pub fn op_0xE8_INX_increment_x(&mut self) {
        let (new_x, _) = self.x.overflowing_add(1);
        self.x = new_x;
        self.p.set(StatusFlags::ZERO, self.x == 0);
        self.p.set(StatusFlags::NEGATIVE, self.x & 0b10000000 > 0);

        self.pc += 1;
        self.cycles += 2;
    }

    pub fn op_0xEA_NOP(&mut self) {
        self.pc += 1;
        self.cycles += 2;
    }

    pub fn op_0xF8_SED_set_decimal_flag(&mut self) {
        if DECIMAL_SUPPORT {
            self.p.insert(StatusFlags::DECIMAL);
        }

        self.pc += 1;
        self.cycles += 2;
    }
}

#[allow(non_snake_case)]
#[cfg(test)]
mod test {
    use super::super::*;

    #[test]
    fn test_BRK() {
        tests::implicit::test_0x00_BRK(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x00_BRK_simulate_interrupt_request,
        );
    }

    #[test]
    fn test_PHP() {
        tests::implicit::test_0x08_PHP(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x08_PHP_push_p_to_stack,
        );
    }

    #[test]
    fn test_CLC() {
        tests::implicit::test_0x18_CLC(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x18_CLC_clear_carry,
        );
    }

    #[test]
    fn test_PLP() {
        tests::implicit::test_0x28_PLP(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x28_PLP_pull_p_from_stack,
        );
    }

    #[test]
    fn test_SEC() {
        tests::implicit::test_0x38_SEC(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x38_SEC_set_carry,
        );
    }

    #[test]
    fn test_RTI() {
        tests::implicit::test_0x40_RTI(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x40_RTI_return_from_interrupt,
        );
    }

    #[test]
    fn test_PHA() {
        tests::implicit::test_0x48_PHA(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x48_PHA_push_a_to_stack,
        );
    }

    #[test]
    fn test_CLI() {
        tests::implicit::test_0x58_CLI(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x58_CLI_clear_interrupt,
        );
    }

    #[test]
    fn test_RTS() {
        tests::implicit::test_0x60_RTS(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x60_RTS_return_from_subroutine,
        );
    }

    #[test]
    fn test_PLA_negative() {
        tests::implicit::test_0x68_PLA_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x68_PLA_pull_a_from_stack,
        );
    }

    #[test]
    fn test_PLA_zero() {
        tests::implicit::test_0x68_PLA_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x68_PLA_pull_a_from_stack,
        );
    }

    #[test]
    fn test_SEI() {
        tests::implicit::test_0x78_SEI(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x78_SEI_set_interrupt_disable,
        );
    }

    #[test]
    fn test_DEY() {
        tests::implicit::test_0x88_DEY(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x88_DEY_decrement_y,
        );
    }

    #[test]
    fn test_DEY_overflow_negative() {
        tests::implicit::test_0x88_DEY_overflow_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x88_DEY_decrement_y,
        );
    }

    #[test]
    fn test_DEY_zero() {
        tests::implicit::test_0x88_DEY_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x88_DEY_decrement_y,
        );
    }

    #[test]
    fn test_TXA_negative() {
        tests::implicit::test_0x8A_TXA_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x8A_TXA_transfer_x_to_a,
        );
    }

    #[test]
    fn test_TXA_zero() {
        tests::implicit::test_0x8A_TXA_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x8A_TXA_transfer_x_to_a,
        );
    }

    #[test]
    fn test_TYA_negative() {
        tests::implicit::test_0x98_TYA_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x98_TYA_transfer_y_to_a,
        );
    }

    #[test]
    fn test_TYA_zero() {
        tests::implicit::test_0x98_TYA_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x98_TYA_transfer_y_to_a,
        );
    }

    #[test]
    fn test_TXS() {
        tests::implicit::test_0x9A_TXS(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x9A_TXS_transfer_x_to_sp,
        );
    }

    #[test]
    fn test_TAY_negative() {
        tests::implicit::test_0xA8_TAY_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xA8_TAY_transfer_a_to_y,
        );
    }

    #[test]
    fn test_TAY_zero() {
        tests::implicit::test_0xA8_TAY_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xA8_TAY_transfer_a_to_y,
        );
    }

    #[test]
    fn test_CLV() {
        tests::implicit::test_0xB8_CLV(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xB8_CLV_clear_overflow,
        );
    }

    #[test]
    fn test_INY() {
        tests::implicit::test_0xC8_INY(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xC8_INY_increment_y,
        );
    }

    #[test]
    fn test_INY_overflow_zero() {
        tests::implicit::test_0xC8_INY_overflow_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xC8_INY_increment_y,
        );
    }

    #[test]
    fn test_INY_negative() {
        tests::implicit::test_0xC8_INY_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xC8_INY_increment_y,
        );
    }

    #[test]
    fn test_DEX() {
        tests::implicit::test_0xCA_DEX(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xCA_DEX_decrement_x,
        );
    }

    #[test]
    fn test_DEX_overflow_negative() {
        tests::implicit::test_0xCA_DEX_overflow_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xCA_DEX_decrement_x,
        );
    }

    #[test]
    fn test_DEX_zero() {
        tests::implicit::test_0xCA_DEX_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xCA_DEX_decrement_x,
        );
    }

    #[test]
    fn test_CLD() {
        tests::implicit::test_0xD8_CLD(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xD8_CLD_clear_decimal_flag,
        );
    }

    #[test]
    fn test_INX() {
        tests::implicit::test_0xE8_INX(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xE8_INX_increment_x,
        );
    }

    #[test]
    fn test_INX_overflow_zero() {
        tests::implicit::test_0xE8_INX_overflow_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xE8_INX_increment_x,
        );
    }

    #[test]
    fn test_INX_negative() {
        tests::implicit::test_0xE8_INX_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xE8_INX_increment_x,
        );
    }

    #[test]
    fn test_NOP() {
        tests::implicit::test_0xEA_NOP(MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xEA_NOP);
    }

    #[test]
    fn test_SED() {
        tests::implicit::test_0xF8_SED(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xF8_SED_set_decimal_flag,
        );
    }
}
