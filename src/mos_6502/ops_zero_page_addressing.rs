use super::*;

#[allow(non_snake_case)]
impl<const DECIMAL_SUPPORT: bool, Bus: DataBus> MOS6502<DECIMAL_SUPPORT, Bus> {
    pub fn op_0x05_ORA_bitwise_or_a_with_memory(&mut self, addr: u8) {
        let value = self.read_simple(addr as u16);
        self.ora_bitwise_or_with_a(value, 2, 2);
    }

    pub fn op_0x06_ASL_arithmetic_shift_left(&mut self, addr: u8) {
        let value = self.read_simple(addr as u16);

        let result = self.asl_arithmetic_shift_left(value, 2, 5);
        self.write_simple(addr as u16, result);
    }

    pub fn op_0x24_BIT_test_bits_in_a_with_m(&mut self, addr: u8) {
        let value = self.read_simple(addr as u16);
        self.bit_test_bits_in_a_with_m(value, 2, 3);
    }

    pub fn op_0x25_AND_bitwise_and_with_a(&mut self, addr: u8) {
        let value = self.read_simple(addr as u16);
        self.and_bitwise_and_with_a(value, 2, 2);
    }

    pub fn op_0x26_ROL_rotate_left(&mut self, addr: u8) {
        let value = self.read_simple(addr as u16);

        let result = self.rol_rotate_left(value, 2, 5);
        self.write_simple(addr as u16, result);
    }

    pub fn op_0x45_EOR_bitwise_xor_with_a(&mut self, addr: u8) {
        let value = self.read_simple(addr as u16);
        self.eor_bitwise_xor_with_a(value, 2, 3);
    }

    pub fn op_0x46_LSR_logical_shift_right(&mut self, addr: u8) {
        let value = self.read_simple(addr as u16);

        let result = self.lsr_logical_shift_right(value, 2, 5);
        self.write_simple(addr as u16, result);
    }

    pub fn op_0x65_ADC_add_to_a_with_carry(&mut self, addr: u8) {
        let value = self.read_simple(addr as u16);
        self.adc_add_to_a_with_carry(value, 2, 3);
    }

    pub fn op_0x66_ROR_rotate_right(&mut self, addr: u8) {
        let value = self.read_simple(addr as u16);

        let result = self.ror_rotate_right(value, 2, 5);
        self.write_simple(addr as u16, result);
    }

    pub fn op_0x84_STY_store_y_in_memory(&mut self, addr: u8) {
        self.write_simple(addr as u16, self.y);

        self.pc += 2;
        self.cycles += 3;
    }

    pub fn op_0x85_STA_store_a_in_memory(&mut self, addr: u8) {
        self.write_simple(addr as u16, self.a);

        self.pc += 2;
        self.cycles += 3;
    }

    pub fn op_0x86_STX_store_x_in_memory(&mut self, addr: u8) {
        self.write_simple(addr as u16, self.x);

        self.pc += 2;
        self.cycles += 3;
    }

    pub fn op_0xA4_LDY_load_y_with_memory(&mut self, addr: u8) {
        let value = self.read_simple(addr as u16);
        self.ldy_load_y_with_value(value, 2, 3);
    }

    pub fn op_0xA5_LDA_load_a_with_memory(&mut self, addr: u8) {
        let value = self.read_simple(addr as u16);
        self.lda_load_a_with_value(value, 2, 3);
    }

    pub fn op_0xA6_LDX_load_x_with_memory(&mut self, addr: u8) {
        let value = self.read_simple(addr as u16);
        self.ldx_load_x_with_value(value, 2, 3);
    }

    pub fn op_0xC4_CPY_compare_with_y(&mut self, addr: u8) {
        let value = self.read_simple(addr as u16);
        self.cpy_compare_with_y(value, 2, 3);
    }

    pub fn op_0xC5_CMP_compare_with_a(&mut self, addr: u8) {
        let value = self.read_simple(addr as u16);
        self.cmp_compare_with_a(value, 2, 3);
    }

    pub fn op_0xC6_DEC_decrement_by_one(&mut self, addr: u8) {
        let value = self.read_simple(addr as u16);

        let result = self.dec_decrement_by_one(value, 2, 5);
        self.write_simple(addr as u16, result);
    }

    pub fn op_0xE4_CPX_compare_with_x(&mut self, addr: u8) {
        let value = self.read_simple(addr as u16);
        self.cpx_compare_with_x(value, 2, 3);
    }

    pub fn op_0xE5_SBC_subtract_from_a_with_carry(&mut self, addr: u8) {
        let value = self.read_simple(addr as u16);
        self.sbc_subtract_from_a_with_carry(value, 2, 3);
    }

    pub fn op_0xE6_INC_increment_by_one(&mut self, addr: u8) {
        let value = self.read_simple(addr as u16);

        let result = self.inc_increment_by_one(value, 2, 5);
        self.write_simple(addr as u16, result);
    }
}

#[allow(non_snake_case)]
#[cfg(test)]
mod test {
    use super::super::*;

    #[test]
    fn test_ORA_negative() {
        tests::zero_page::test_0x05_ORA_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x05_ORA_bitwise_or_a_with_memory,
        );
    }

    #[test]
    fn test_ORA_zero() {
        tests::zero_page::test_0x05_ORA_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x05_ORA_bitwise_or_a_with_memory,
        );
    }

    #[test]
    fn test_ASL_negative() {
        tests::zero_page::test_0x06_ASL_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x06_ASL_arithmetic_shift_left,
        );
    }

    #[test]
    fn test_ASL_carry_zero() {
        tests::zero_page::test_0x06_ASL_carry_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x06_ASL_arithmetic_shift_left,
        );
    }

    #[test]
    fn test_BIT_negative() {
        tests::zero_page::test_0x24_BIT_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x24_BIT_test_bits_in_a_with_m,
        );
    }

    #[test]
    fn test_BIT_overflow() {
        tests::zero_page::test_0x24_BIT_overflow(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x24_BIT_test_bits_in_a_with_m,
        );
    }

    #[test]
    fn test_BIT_zero() {
        tests::zero_page::test_0x24_BIT_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x24_BIT_test_bits_in_a_with_m,
        );
    }

    #[test]
    fn test_AND_negative() {
        tests::zero_page::test_0x25_AND_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x25_AND_bitwise_and_with_a,
        );
    }

    #[test]
    fn test_AND_zero() {
        tests::zero_page::test_0x25_AND_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x25_AND_bitwise_and_with_a,
        );
    }

    #[test]
    fn test_ROL_negative() {
        tests::zero_page::test_0x26_ROL_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x26_ROL_rotate_left,
        );
    }

    #[test]
    fn test_ROL_carry_zero() {
        tests::zero_page::test_0x26_ROL_carry_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x26_ROL_rotate_left,
        );
    }

    #[test]
    fn test_EOR_negative() {
        tests::zero_page::test_0x45_EOR_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x45_EOR_bitwise_xor_with_a,
        );
    }

    #[test]
    fn test_EOR_zero() {
        tests::zero_page::test_0x45_EOR_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x45_EOR_bitwise_xor_with_a,
        );
    }

    #[test]
    fn test_LSR() {
        tests::zero_page::test_0x46_LSR(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x46_LSR_logical_shift_right,
        );
    }

    #[test]
    fn test_ADC() {
        tests::zero_page::test_0x65_ADC(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x65_ADC_add_to_a_with_carry,
        );
    }

    #[test]
    fn test_ADC_overflow_zero() {
        tests::zero_page::test_0x65_ADC_overflow_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x65_ADC_add_to_a_with_carry,
        );
    }

    #[test]
    fn test_ADC_negative() {
        tests::zero_page::test_0x65_ADC_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x65_ADC_add_to_a_with_carry,
        );
    }

    #[test]
    fn test_ADC_decimal_mode() {
        tests::zero_page::test_0x65_ADC_decimal_mode(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x65_ADC_add_to_a_with_carry,
        );
    }

    #[test]
    fn test_ADC_decimal_mode_carry() {
        tests::zero_page::test_0x65_ADC_decimal_mode_carry(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x65_ADC_add_to_a_with_carry,
        );
    }

    #[test]
    fn test_ROR_negative() {
        tests::zero_page::test_0x66_ROR_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x66_ROR_rotate_right,
        );
    }

    #[test]
    fn test_ROR_carry_zero() {
        tests::zero_page::test_0x66_ROR_carry_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x66_ROR_rotate_right,
        );
    }

    #[test]
    fn test_STY() {
        tests::zero_page::test_0x84_STY(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x84_STY_store_y_in_memory,
        );
    }

    #[test]
    fn test_STA() {
        tests::zero_page::test_0x85_STA(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x85_STA_store_a_in_memory,
        );
    }

    #[test]
    fn test_STX() {
        tests::zero_page::test_0x86_STX(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x86_STX_store_x_in_memory,
        );
    }

    #[test]
    fn test_LDY() {
        tests::zero_page::test_0xA4_LDY(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xA4_LDY_load_y_with_memory,
        );
    }

    #[test]
    fn test_LDY_negative() {
        tests::zero_page::test_0xA4_LDY_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xA4_LDY_load_y_with_memory,
        );
    }

    #[test]
    fn test_LDY_zero() {
        tests::zero_page::test_0xA4_LDY_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xA4_LDY_load_y_with_memory,
        );
    }

    #[test]
    fn test_LDA() {
        tests::zero_page::test_0xA5_LDA(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xA5_LDA_load_a_with_memory,
        );
    }

    #[test]
    fn test_LDA_negative() {
        tests::zero_page::test_0xA5_LDA_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xA5_LDA_load_a_with_memory,
        );
    }

    #[test]
    fn test_LDA_zero() {
        tests::zero_page::test_0xA5_LDA_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xA5_LDA_load_a_with_memory,
        );
    }

    #[test]
    fn test_LDX() {
        tests::zero_page::test_0xA6_LDX(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xA6_LDX_load_x_with_memory,
        );
    }

    #[test]
    fn test_LDX_negative() {
        tests::zero_page::test_0xA6_LDX_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xA6_LDX_load_x_with_memory,
        );
    }

    #[test]
    fn test_LDX_zero() {
        tests::zero_page::test_0xA6_LDX_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xA6_LDX_load_x_with_memory,
        );
    }

    #[test]
    fn test_CPY_negative() {
        tests::zero_page::test_0xC4_CPY_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xC4_CPY_compare_with_y,
        );
    }

    #[test]
    fn test_CPY_carry_zero() {
        tests::zero_page::test_0xC4_CPY_zero_carry(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xC4_CPY_compare_with_y,
        );
    }

    #[test]
    fn test_CPY_carry() {
        tests::zero_page::test_0xC4_CPY_carry(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xC4_CPY_compare_with_y,
        );
    }

    #[test]
    fn test_CMP_negative() {
        tests::zero_page::test_0xC5_CMP_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xC5_CMP_compare_with_a,
        );
    }

    #[test]
    fn test_CMP_carry_zero() {
        tests::zero_page::test_0xC5_CMP_zero_carry(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xC5_CMP_compare_with_a,
        );
    }

    #[test]
    fn test_CMP_carry() {
        tests::zero_page::test_0xC5_CMP_carry(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xC5_CMP_compare_with_a,
        );
    }

    #[test]
    fn test_DEC() {
        tests::zero_page::test_0xC6_DEC(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xC6_DEC_decrement_by_one,
        );
    }

    #[test]
    fn test_DEC_negative() {
        tests::zero_page::test_0xC6_DEC_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xC6_DEC_decrement_by_one,
        );
    }

    #[test]
    fn test_DEC_zero() {
        tests::zero_page::test_0xC6_DEC_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xC6_DEC_decrement_by_one,
        );
    }

    #[test]
    fn test_CPX_negative() {
        tests::zero_page::test_0xE4_CPX_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xE4_CPX_compare_with_x,
        );
    }

    #[test]
    fn test_CPX_carry_zero() {
        tests::zero_page::test_0xE4_CPX_zero_carry(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xE4_CPX_compare_with_x,
        );
    }

    #[test]
    fn test_CPX_carry() {
        tests::zero_page::test_0xE4_CPX_carry(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xE4_CPX_compare_with_x,
        );
    }

    #[test]
    fn test_SBC() {
        tests::zero_page::test_0xE5_SBC(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xE5_SBC_subtract_from_a_with_carry,
        );
    }

    #[test]
    fn test_SBC_zero() {
        tests::zero_page::test_0xE5_SBC_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xE5_SBC_subtract_from_a_with_carry,
        );
    }

    #[test]
    fn test_SBC_overflow_negative() {
        tests::zero_page::test_0xE5_SBC_overflow_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xE5_SBC_subtract_from_a_with_carry,
        );
    }

    #[test]
    fn test_SBC_decimal_mode() {
        tests::zero_page::test_0xE5_SBC_decimal_mode(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xE5_SBC_subtract_from_a_with_carry,
        );
    }

    #[test]
    fn test_SBC_decimal_mode_overflow() {
        tests::zero_page::test_0xE5_SBC_decimal_mode_overflow(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xE5_SBC_subtract_from_a_with_carry,
        );
    }

    #[test]
    fn test_INC() {
        tests::zero_page::test_0xE6_INC(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xE6_INC_increment_by_one,
        );
    }

    #[test]
    fn test_INC_negative() {
        tests::zero_page::test_0xE6_INC_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xE6_INC_increment_by_one,
        );
    }

    #[test]
    fn test_INC_zero() {
        tests::zero_page::test_0xE6_INC_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xE6_INC_increment_by_one,
        );
    }
}
