use super::*;

#[allow(non_snake_case)]
impl<const DECIMAL_SUPPORT: bool, Bus: DataBus> MOS6502<DECIMAL_SUPPORT, Bus> {
    pub fn op_0x19_ORA_bitwise_or_a_with_memory_y(&mut self, low_addr: u8, high_addr: u8) {
        let (value, extra_cycle) = self.read_indexed(high_addr, low_addr, self.y);
        self.ora_bitwise_or_with_a(value, 3, 4 + extra_cycle);
    }

    pub fn op_0x1D_ORA_bitwise_or_a_with_memory_x(&mut self, low_addr: u8, high_addr: u8) {
        let (value, extra_cycle) = self.read_indexed(high_addr, low_addr, self.x);
        self.ora_bitwise_or_with_a(value, 3, 4 + extra_cycle);
    }

    pub fn op_0x1E_ASL_arithmetic_shift_left_x(&mut self, low_addr: u8, high_addr: u8) {
        let (value, _) = self.read_indexed(high_addr, low_addr, self.x);

        let result = self.asl_arithmetic_shift_left(value, 3, 7);
        let _ = self.write_indexed(high_addr, low_addr, 0, result);
    }

    pub fn op_0x39_AND_bitwise_and_with_a_y(&mut self, low_addr: u8, high_addr: u8) {
        let (value, extra_cycle) = self.read_indexed(high_addr, low_addr, self.y);
        self.and_bitwise_and_with_a(value, 3, 4 + extra_cycle);
    }

    pub fn op_0x3D_AND_bitwise_and_with_a_x(&mut self, low_addr: u8, high_addr: u8) {
        let (value, extra_cycle) = self.read_indexed(high_addr, low_addr, self.x);
        self.and_bitwise_and_with_a(value, 3, 4 + extra_cycle);
    }

    pub fn op_0x3E_ROL_rotate_left_x(&mut self, low_addr: u8, high_addr: u8) {
        let (value, _) = self.read_indexed(high_addr, low_addr, self.x);

        let result = self.rol_rotate_left(value, 3, 7);
        let _ = self.write_indexed(high_addr, low_addr, 0, result);
    }

    pub fn op_0x59_EOR_bitwise_xor_with_a_y(&mut self, low_addr: u8, high_addr: u8) {
        let (value, extra_cycle) = self.read_indexed(high_addr, low_addr, self.y);
        self.eor_bitwise_xor_with_a(value, 3, 4 + extra_cycle);
    }

    pub fn op_0x5D_EOR_bitwise_xor_with_a_x(&mut self, low_addr: u8, high_addr: u8) {
        let (value, extra_cycle) = self.read_indexed(high_addr, low_addr, self.x);
        self.eor_bitwise_xor_with_a(value, 3, 4 + extra_cycle);
    }

    pub fn op_0x5E_LSR_logical_shift_right_x(&mut self, low_addr: u8, high_addr: u8) {
        let (value, _) = self.read_indexed(high_addr, low_addr, self.x);

        let result = self.lsr_logical_shift_right(value, 3, 7);
        let _ = self.write_indexed(high_addr, low_addr, 0, result);
    }

    pub fn op_0x79_ADC_add_to_a_with_carry_y(&mut self, low_addr: u8, high_addr: u8) {
        let (value, extra_cycle) = self.read_indexed(high_addr, low_addr, self.y);
        self.adc_add_to_a_with_carry(value, 3, 4 + extra_cycle);
    }

    pub fn op_0x7D_ADC_add_to_a_with_carry_x(&mut self, low_addr: u8, high_addr: u8) {
        let (value, extra_cycle) = self.read_indexed(high_addr, low_addr, self.x);
        self.adc_add_to_a_with_carry(value, 3, 4 + extra_cycle);
    }

    pub fn op_0x7E_ROR_rotate_right_x(&mut self, low_addr: u8, high_addr: u8) {
        let (value, _) = self.read_indexed(high_addr, low_addr, self.x);

        let result = self.ror_rotate_right(value, 3, 7);
        let _ = self.write_indexed(high_addr, low_addr, 0, result);
    }

    pub fn op_0x99_STA_store_a_in_memory_y(&mut self, low_addr: u8, high_addr: u8) {
        let _ = self.write_indexed(high_addr, low_addr, self.y, self.a);

        self.pc += 3;
        self.cycles += 5;
    }

    pub fn op_0x9D_STA_store_a_in_memory_x(&mut self, low_addr: u8, high_addr: u8) {
        let _ = self.write_indexed(high_addr, low_addr, self.x, self.a);

        self.pc += 3;
        self.cycles += 5;
    }

    pub fn op_0xB9_LDA_load_a_with_memory_y(&mut self, low_addr: u8, high_addr: u8) {
        let (value, extra_cycle) = self.read_indexed(high_addr, low_addr, self.y);
        self.lda_load_a_with_value(value, 3, 4 + extra_cycle);
    }

    pub fn op_0xBC_LDY_load_y_with_memory_x(&mut self, low_addr: u8, high_addr: u8) {
        let (value, extra_cycle) = self.read_indexed(high_addr, low_addr, self.x);
        self.ldy_load_y_with_value(value, 3, 4 + extra_cycle);
    }

    pub fn op_0xBD_LDA_load_a_with_memory_x(&mut self, low_addr: u8, high_addr: u8) {
        let (value, extra_cycle) = self.read_indexed(high_addr, low_addr, self.x);
        self.lda_load_a_with_value(value, 3, 4 + extra_cycle);
    }

    pub fn op_0xBE_LDX_load_x_with_memory_y(&mut self, low_addr: u8, high_addr: u8) {
        let (value, extra_cycle) = self.read_indexed(high_addr, low_addr, self.y);
        self.ldx_load_x_with_value(value, 3, 4 + extra_cycle);
    }

    pub fn op_0xD9_CMP_compare_with_a_y(&mut self, low_addr: u8, high_addr: u8) {
        let (value, extra_cycle) = self.read_indexed(high_addr, low_addr, self.y);
        self.cmp_compare_with_a(value, 3, 4 + extra_cycle);
    }

    pub fn op_0xDD_CMP_compare_with_a_x(&mut self, low_addr: u8, high_addr: u8) {
        let (value, extra_cycle) = self.read_indexed(high_addr, low_addr, self.x);
        self.cmp_compare_with_a(value, 3, 4 + extra_cycle);
    }

    pub fn op_0xDE_DEC_decrement_by_one_x(&mut self, low_addr: u8, high_addr: u8) {
        let (value, _) = self.read_indexed(high_addr, low_addr, self.x);

        let result = self.dec_decrement_by_one(value, 3, 7);
        let _ = self.write_indexed(high_addr, low_addr, 0, result);
    }

    pub fn op_0xF9_SBC_subtract_from_a_with_carry_y(&mut self, low_addr: u8, high_addr: u8) {
        let (value, extra_cycle) = self.read_indexed(high_addr, low_addr, self.y);
        self.sbc_subtract_from_a_with_carry(value, 3, 4 + extra_cycle);
    }

    pub fn op_0xFD_SBC_subtract_from_a_with_carry_x(&mut self, low_addr: u8, high_addr: u8) {
        let (value, extra_cycle) = self.read_indexed(high_addr, low_addr, self.x);
        self.sbc_subtract_from_a_with_carry(value, 3, 4 + extra_cycle);
    }

    pub fn op_0xFE_INC_increment_by_one_x(&mut self, low_addr: u8, high_addr: u8) {
        let (value, _) = self.read_indexed(high_addr, low_addr, self.x);

        let result = self.inc_increment_by_one(value, 3, 7);
        let _ = self.write_indexed(high_addr, low_addr, 0, result);
    }
}

#[allow(non_snake_case)]
#[cfg(test)]
mod test {
    use super::super::*;

    #[test]
    fn test_ORA_negative_y() {
        tests::absolute_indexed::test_0x19_ORA_negative_y(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x19_ORA_bitwise_or_a_with_memory_y,
        );
    }

    #[test]
    fn test_ORA_zero_y() {
        tests::absolute_indexed::test_0x19_ORA_zero_y(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x19_ORA_bitwise_or_a_with_memory_y,
        );
    }

    #[test]
    fn test_ORA_negative_x() {
        tests::absolute_indexed::test_0x1D_ORA_negative_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x1D_ORA_bitwise_or_a_with_memory_x,
        );
    }

    #[test]
    fn test_ORA_zero_x() {
        tests::absolute_indexed::test_0x1D_ORA_zero_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x1D_ORA_bitwise_or_a_with_memory_x,
        );
    }

    #[test]
    fn test_ASL_negative_x() {
        tests::absolute_indexed::test_0x1E_ASL_negative_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x1E_ASL_arithmetic_shift_left_x,
        );
    }

    #[test]
    fn test_ASL_carry_zero_x() {
        tests::absolute_indexed::test_0x1E_ASL_carry_zero_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x1E_ASL_arithmetic_shift_left_x,
        );
    }

    #[test]
    fn test_AND_negative_y() {
        tests::absolute_indexed::test_0x39_AND_negative_y(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x39_AND_bitwise_and_with_a_y,
        );
    }

    #[test]
    fn test_AND_zero_y() {
        tests::absolute_indexed::test_0x39_AND_zero_y(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x39_AND_bitwise_and_with_a_y,
        );
    }

    #[test]
    fn test_AND_negative_x() {
        tests::absolute_indexed::test_0x3D_AND_negative_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x3D_AND_bitwise_and_with_a_x,
        );
    }

    #[test]
    fn test_AND_zero_x() {
        tests::absolute_indexed::test_0x3D_AND_zero_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x3D_AND_bitwise_and_with_a_x,
        );
    }

    #[test]
    fn test_ROL_negative_x() {
        tests::absolute_indexed::test_0x3E_ROL_negative_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x3E_ROL_rotate_left_x,
        );
    }

    #[test]
    fn test_ROL_carry_zero_x() {
        tests::absolute_indexed::test_0x3E_ROL_carry_zero_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x3E_ROL_rotate_left_x,
        );
    }

    #[test]
    fn test_EOR_negative_y() {
        tests::absolute_indexed::test_0x59_EOR_negative_y(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x59_EOR_bitwise_xor_with_a_y,
        );
    }

    #[test]
    fn test_EOR_zero_y() {
        tests::absolute_indexed::test_0x59_EOR_zero_y(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x59_EOR_bitwise_xor_with_a_y,
        );
    }

    #[test]
    fn test_EOR_negative_x() {
        tests::absolute_indexed::test_0x5D_EOR_negative_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x5D_EOR_bitwise_xor_with_a_x,
        );
    }

    #[test]
    fn test_EOR_zero_x() {
        tests::absolute_indexed::test_0x5D_EOR_zero_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x5D_EOR_bitwise_xor_with_a_x,
        );
    }

    #[test]
    fn test_LSR_x() {
        tests::absolute_indexed::test_0x5E_LSR_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x5E_LSR_logical_shift_right_x,
        );
    }

    #[test]
    fn test_ADC_y() {
        tests::absolute_indexed::test_0x79_ADC_y(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x79_ADC_add_to_a_with_carry_y,
        );
    }

    #[test]
    fn test_ADC_overflow_zero_y() {
        tests::absolute_indexed::test_0x79_ADC_overflow_zero_y(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x79_ADC_add_to_a_with_carry_y,
        );
    }

    #[test]
    fn test_ADC_negative_y() {
        tests::absolute_indexed::test_0x79_ADC_negative_y(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x79_ADC_add_to_a_with_carry_y,
        );
    }

    #[test]
    fn test_ADC_decimal_mode_y() {
        tests::absolute_indexed::test_0x79_ADC_decimal_mode_y(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x79_ADC_add_to_a_with_carry_y,
        );
    }

    #[test]
    fn test_ADC_decimal_mode_carry_y() {
        tests::absolute_indexed::test_0x79_ADC_decimal_mode_carry_y(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x79_ADC_add_to_a_with_carry_y,
        );
    }

    #[test]
    fn test_ADC_x() {
        tests::absolute_indexed::test_0x7D_ADC_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x7D_ADC_add_to_a_with_carry_x,
        );
    }

    #[test]
    fn test_ADC_overflow_zero_x() {
        tests::absolute_indexed::test_0x7D_ADC_overflow_zero_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x7D_ADC_add_to_a_with_carry_x,
        );
    }

    #[test]
    fn test_ADC_negative_x() {
        tests::absolute_indexed::test_0x7D_ADC_negative_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x7D_ADC_add_to_a_with_carry_x,
        );
    }

    #[test]
    fn test_ADC_decimal_mode_x() {
        tests::absolute_indexed::test_0x7D_ADC_decimal_mode_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x7D_ADC_add_to_a_with_carry_x,
        );
    }

    #[test]
    fn test_ADC_decimal_mode_carry_x() {
        tests::absolute_indexed::test_0x7D_ADC_decimal_mode_carry_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x7D_ADC_add_to_a_with_carry_x,
        );
    }

    #[test]
    fn test_ROR_negative_x() {
        tests::absolute_indexed::test_0x7E_ROR_negative_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x7E_ROR_rotate_right_x,
        );
    }

    #[test]
    fn test_ROR_carry_zero_x() {
        tests::absolute_indexed::test_0x7E_ROR_carry_zero_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x7E_ROR_rotate_right_x,
        );
    }

    #[test]
    fn test_STA_y() {
        tests::absolute_indexed::test_0x99_STA_y(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x99_STA_store_a_in_memory_y,
        );
    }

    #[test]
    fn test_STA_x() {
        tests::absolute_indexed::test_0x9D_STA_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x9D_STA_store_a_in_memory_x,
        );
    }

    #[test]
    fn test_LDA_y() {
        tests::absolute_indexed::test_0xB9_LDA_y(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xB9_LDA_load_a_with_memory_y,
        );
    }

    #[test]
    fn test_LDA_negative_y() {
        tests::absolute_indexed::test_0xB9_LDA_negative_y(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xB9_LDA_load_a_with_memory_y,
        );
    }

    #[test]
    fn test_LDA_zero_y() {
        tests::absolute_indexed::test_0xB9_LDA_zero_y(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xB9_LDA_load_a_with_memory_y,
        );
    }

    #[test]
    fn test_LDY_x() {
        tests::absolute_indexed::test_0xBC_LDY_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xBC_LDY_load_y_with_memory_x,
        );
    }

    #[test]
    fn test_LDY_negative() {
        tests::absolute_indexed::test_0xBC_LDY_negative_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xBC_LDY_load_y_with_memory_x,
        );
    }

    #[test]
    fn test_LDY_zero() {
        tests::absolute_indexed::test_0xBC_LDY_zero_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xBC_LDY_load_y_with_memory_x,
        );
    }

    #[test]
    fn test_LDA_x() {
        tests::absolute_indexed::test_0xBD_LDA_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xBD_LDA_load_a_with_memory_x,
        );
    }

    #[test]
    fn test_LDA_negative_x() {
        tests::absolute_indexed::test_0xBD_LDA_negative_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xBD_LDA_load_a_with_memory_x,
        );
    }

    #[test]
    fn test_LDA_zero_x() {
        tests::absolute_indexed::test_0xBD_LDA_zero_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xBD_LDA_load_a_with_memory_x,
        );
    }

    #[test]
    fn test_LDX_y() {
        tests::absolute_indexed::test_0xBE_LDX_y(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xBE_LDX_load_x_with_memory_y,
        );
    }

    #[test]
    fn test_LDX_negative_y() {
        tests::absolute_indexed::test_0xBE_LDX_negative_y(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xBE_LDX_load_x_with_memory_y,
        );
    }

    #[test]
    fn test_LDX_zero_y() {
        tests::absolute_indexed::test_0xBE_LDX_zero_y(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xBE_LDX_load_x_with_memory_y,
        );
    }

    #[test]
    fn test_CMP_negative_y() {
        tests::absolute_indexed::test_0xD9_CMP_negative_y(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xD9_CMP_compare_with_a_y,
        );
    }

    #[test]
    fn test_CMP_carry_zero_y() {
        tests::absolute_indexed::test_0xD9_CMP_zero_carry_y(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xD9_CMP_compare_with_a_y,
        );
    }

    #[test]
    fn test_CMP_carry_y() {
        tests::absolute_indexed::test_0xD9_CMP_carry_y(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xD9_CMP_compare_with_a_y,
        );
    }

    #[test]
    fn test_CMP_negative_x() {
        tests::absolute_indexed::test_0xDD_CMP_negative_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xDD_CMP_compare_with_a_x,
        );
    }

    #[test]
    fn test_CMP_carry_zero_x() {
        tests::absolute_indexed::test_0xDD_CMP_zero_carry_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xDD_CMP_compare_with_a_x,
        );
    }

    #[test]
    fn test_CMP_carry_x() {
        tests::absolute_indexed::test_0xDD_CMP_carry_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xDD_CMP_compare_with_a_x,
        );
    }

    #[test]
    fn test_DEC_x() {
        tests::absolute_indexed::test_0xDE_DEC_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xDE_DEC_decrement_by_one_x,
        );
    }

    #[test]
    fn test_DEC_negative_x() {
        tests::absolute_indexed::test_0xDE_DEC_negative_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xDE_DEC_decrement_by_one_x,
        );
    }

    #[test]
    fn test_DEC_zero_x() {
        tests::absolute_indexed::test_0xDE_DEC_zero_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xDE_DEC_decrement_by_one_x,
        );
    }

    #[test]
    fn test_SBC_y() {
        tests::absolute_indexed::test_0xF9_SBC_y(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xF9_SBC_subtract_from_a_with_carry_y,
        );
    }

    #[test]
    fn test_SBC_zero_y() {
        tests::absolute_indexed::test_0xF9_SBC_zero_y(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xF9_SBC_subtract_from_a_with_carry_y,
        );
    }

    #[test]
    fn test_SBC_overflow_negative_y() {
        tests::absolute_indexed::test_0xF9_SBC_overflow_negative_y(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xF9_SBC_subtract_from_a_with_carry_y,
        );
    }

    #[test]
    fn test_SBC_decimal_mode_y() {
        tests::absolute_indexed::test_0xF9_SBC_decimal_mode_y(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xF9_SBC_subtract_from_a_with_carry_y,
        );
    }

    #[test]
    fn test_SBC_decimal_mode_overflow_y() {
        tests::absolute_indexed::test_0xF9_SBC_decimal_mode_overflow_y(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xF9_SBC_subtract_from_a_with_carry_y,
        );
    }

    #[test]
    fn test_SBC_x() {
        tests::absolute_indexed::test_0xFD_SBC_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xFD_SBC_subtract_from_a_with_carry_x,
        );
    }

    #[test]
    fn test_SBC_zero_x() {
        tests::absolute_indexed::test_0xFD_SBC_zero_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xFD_SBC_subtract_from_a_with_carry_x,
        );
    }

    #[test]
    fn test_SBC_overflow_negative_x() {
        tests::absolute_indexed::test_0xFD_SBC_overflow_negative_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xFD_SBC_subtract_from_a_with_carry_x,
        );
    }

    #[test]
    fn test_SBC_decimal_mode_x() {
        tests::absolute_indexed::test_0xFD_SBC_decimal_mode_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xFD_SBC_subtract_from_a_with_carry_x,
        );
    }

    #[test]
    fn test_SBC_decimal_mode_overflow_x() {
        tests::absolute_indexed::test_0xFD_SBC_decimal_mode_overflow_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xFD_SBC_subtract_from_a_with_carry_x,
        );
    }

    #[test]
    fn test_INC_x() {
        tests::absolute_indexed::test_0xFE_INC_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xFE_INC_increment_by_one_x,
        );
    }

    #[test]
    fn test_INC_negative_x() {
        tests::absolute_indexed::test_0xFE_INC_negative_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xFE_INC_increment_by_one_x,
        );
    }

    #[test]
    fn test_INC_zero_x() {
        tests::absolute_indexed::test_0xFE_INC_zero_x(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0xFE_INC_increment_by_one_x,
        );
    }
}
