use super::*;

#[allow(non_snake_case)]
impl<const DECIMAL_SUPPORT: bool, Bus: DataBus> MOS6502<DECIMAL_SUPPORT, Bus> {
    pub fn op_0x0A_ASL_arithmetic_shift_left(&mut self) {
        self.a = self.asl_arithmetic_shift_left(self.a, 1, 2);
    }

    pub fn op_0x2A_ROL_rotate_left(&mut self) {
        self.a = self.rol_rotate_left(self.a, 1, 2);
    }

    pub fn op_0x4A_LSR_logical_shift_right(&mut self) {
        self.a = self.lsr_logical_shift_right(self.a, 1, 2);
    }

    pub fn op_0x6A_ROR_rotate_right(&mut self) {
        self.a = self.ror_rotate_right(self.a, 1, 2);
    }
}

#[allow(non_snake_case)]
#[cfg(test)]
mod test {
    use super::super::*;

    #[test]
    fn test_ASL_negative() {
        tests::accumulator::test_0x0A_ASL_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x0A_ASL_arithmetic_shift_left,
        );
    }

    #[test]
    fn test_ASL_carry_zero() {
        tests::accumulator::test_0x0A_ASL_carry_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x0A_ASL_arithmetic_shift_left,
        );
    }

    #[test]
    fn test_ROL_negative() {
        tests::accumulator::test_0x2A_ROL_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x2A_ROL_rotate_left,
        );
    }

    #[test]
    fn test_ROL_carry_zero() {
        tests::accumulator::test_0x2A_ROL_carry_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x2A_ROL_rotate_left,
        );
    }

    #[test]
    fn test_LSR() {
        tests::accumulator::test_0x4A_LSR(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x4A_LSR_logical_shift_right,
        );
    }

    #[test]
    fn test_ROR_negative() {
        tests::accumulator::test_0x6A_ROR_negative(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x6A_ROR_rotate_right,
        );
    }

    #[test]
    fn test_ROR_carry_zero() {
        tests::accumulator::test_0x6A_ROR_carry_zero(
            MOS6502::<DECIMAL_MODE_ENABLED, TestDataBus>::op_0x6A_ROR_rotate_right,
        );
    }
}
