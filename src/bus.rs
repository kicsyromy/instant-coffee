pub trait Device: Default {
    fn read(&self, addr: u16) -> u8;
    fn write(&mut self, addr: u16, data: u8);
}

pub trait DataBus {
    fn read(&self, addr: u16) -> u8;
    fn write(&mut self, addr: u16, data: u8);
}

#[macro_export]
macro_rules! data_bus {
    ($ty_name: tt, $($name:ident: $device:ty => $($start_addr: literal -> $end_addr: literal);*),*) => {
        pub struct $ty_name {
            $($name: $device),*
        }

        impl $ty_name {
            pub fn new() -> Self {
                Self {
                    $($name: <$device>::default()),*
                }
            }
        }

        impl DataBus for $ty_name {
            #[allow(unreachable_patterns)]
            fn read(&self, addr: u16) -> u8 {
                match addr {
                    $($($start_addr..=$end_addr => self.$name.read(addr)),*),*,
                    _ => panic!("Invalid address: {:#04X}", addr)
                }
            }

            #[allow(unreachable_patterns)]
            fn write(&mut self, addr: u16, data: u8) {
                match addr {
                    $($($start_addr..=$end_addr => self.$name.write(addr, data)),*),*,
                    _ => panic!("Invalid address: {:#04X}", addr)
                }
            }
        }
    };
}

#[cfg(test)]
pub mod tests {
    use super::*;

    struct TestRam {
        data: [u8; 0xFFFF + 1],
    }

    impl Default for TestRam {
        fn default() -> Self {
            Self {
                data: [0; 0xFFFF + 1],
            }
        }
    }

    impl Device for TestRam {
        fn read(&self, addr: u16) -> u8 {
            if addr as usize >= self.data.len() {
                panic!("Invalid address: {:#04X}", addr);
            }

            self.data[addr as usize]
        }

        fn write(&mut self, addr: u16, data: u8) {
            if addr as usize >= self.data.len() {
                panic!("Invalid address: {:#04X}", addr);
            }

            self.data[addr as usize] = data;
        }
    }

    data_bus!(
        TestDataBus,
        ram: TestRam => 0x0000 -> 0xFFFF
    );

    impl TestDataBus {
        pub fn write_bytes(&mut self, addr: u16, data: &[u8]) {
            self.ram.data[addr as usize..addr as usize + data.len()].copy_from_slice(data);
        }
    }
}
