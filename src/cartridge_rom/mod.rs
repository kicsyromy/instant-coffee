mod ines;

#[allow(non_camel_case_types)]
#[allow(dead_code)]
#[derive(Debug, Eq, PartialEq)]
pub enum ROMFileFormat {
    iNESArchaic,
    iNES,
    NES20,
}

pub fn rom_file_format(data: &[u8]) -> Option<ROMFileFormat> {
    if data.len() < 16 {
        return None;
    }

    unsafe {
        // SAFETY: Bounds check happens before any element access
        if *data.get_unchecked(0) != b'N'
            || *data.get_unchecked(1) != b'E'
            || *data.get_unchecked(2) != b'S'
            || *data.get_unchecked(3) != 0x1A
        {
            return None;
        }
    }

    let byte7 = unsafe {
        // SAFETY: Bounds check happens before any element access
        *data.get_unchecked(7)
    };

    if byte7 & 0x0c == 0x08 {
        return Some(ROMFileFormat::NES20);
    }

    if byte7 & 0x0c == 0x04 {
        return Some(ROMFileFormat::iNESArchaic);
    }

    if byte7 & 0x0c == 0x00 {
        for _ in 12..=15 {
            // Should check if all zeroes but some rippers put their names across these bytes
            // REF: https://www.nesdev.org/wiki/INES#iNES_file_format
        }

        return Some(ROMFileFormat::iNES);
    }

    None
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn rom_file_format() {
        const NES_TEST: &[u8] = include_bytes!("../../test/ROMs/nestest.nes");

        let fmt = super::rom_file_format(NES_TEST);
        assert!(fmt.is_some());
        assert_eq!(fmt.unwrap(), ROMFileFormat::iNES);
    }
}
