use crate::bus::Device;

pub trait VectorsAddressRange {
    const START_ADDR: u16 = 0xFFFA;
    const END_ADDR: u16 = 0xFFFF;
}

pub struct Vectors {
    data: [u8; (<Vectors as VectorsAddressRange>::END_ADDR
        - <Vectors as VectorsAddressRange>::START_ADDR) as usize
        + 1],
}

impl Vectors {
    pub fn new() -> Self {
        Self {
            data: [0; (Self::END_ADDR - Self::START_ADDR) as usize + 1],
        }
    }
}

impl Default for Vectors {
    fn default() -> Self {
        Self::new()
    }
}

impl Device for Vectors {
    fn read(&self, addr: u16) -> u8 {
        if addr < Self::START_ADDR || addr > Self::END_ADDR {
            panic!("Invalid address: {:#04X}", addr);
        }

        self.data[(addr - Self::START_ADDR) as usize]
    }

    fn write(&mut self, addr: u16, data: u8) {
        if addr < Self::START_ADDR || addr > Self::END_ADDR {
            panic!("Invalid address: {:#04X}", addr);
        }

        self.data[(addr - Self::START_ADDR) as usize] = data;
    }
}

impl VectorsAddressRange for Vectors {}
