use crate::bus::Device;

struct Rom {
    data: Vec<u8>,
    start_addr: u16,
    end_addr: u16,
}

impl Rom {
    pub fn new(data: Vec<u8>, start_addr: u16) -> Self {
        let data_len = data.len() as u16;
        Self {
            data,
            start_addr,
            end_addr: start_addr + data_len - 1,
        }
    }
}

impl Default for Rom {
    fn default() -> Self {
        Self::new(vec![0; 0xFFFF + 1], 0x0000)
    }
}

impl Device for Rom {
    fn read(&self, addr: u16) -> u8 {
        if addr < self.start_addr || addr > self.end_addr {
            panic!("Invalid address: {:#04X}", addr);
        }

        self.data[(addr - self.start_addr) as usize]
    }

    fn write(&mut self, addr: u16, data: u8) {
        if addr < self.start_addr || addr > self.end_addr {
            panic!("Invalid address: {:#04X}", addr);
        }

        self.data[(addr - self.start_addr) as usize] = data;
    }
}
