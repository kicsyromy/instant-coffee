use caffeine::{Application, WindowBuilder};

mod bus;
mod cartridge_rom;
mod mos_6502;
mod ram;
mod rom;
mod vectors;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::init();

    let app = Application::new()?;

    let _main_window = WindowBuilder::new()
        .title("Instant Coffee".into())
        .on_render(move |window, mut painter, _render_event| {
            painter.set_color(1.0, 0.1, 0.1, 1.0);
            painter.clear();

            painter.set_color(0.0, 0.0, 0.0, 0.5);
            painter.draw_line(0.0, 0.0, window.width() as f32, window.height() as f32);
        })
        .vsync(true)
        .build()?;

    Ok(Application::exec(app)?)
}
