pub use application::*;
use errors::*;
pub use renderer::*;
pub use window::*;

#[macro_use]
pub mod macros;

pub mod errors;
pub mod events;

mod application;
mod renderer;
mod window;

pub type CaffeineResult<T> = Result<T, CaffeineError>;

type DisableSync = std::marker::PhantomData<std::cell::Cell<()>>;
type DisableSend = std::marker::PhantomData<std::sync::MutexGuard<'static, ()>>;
