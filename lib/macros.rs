#[allow(unused_macros)]

macro_rules! c_str_literal {
    ($string: literal) => {{
        let cstring = concat!($string, "\0");
        cstring.as_ptr() as *const i8
    }};
}

macro_rules! sdl_error {
    () => {{
        unsafe {
            std::ffi::CStr::from_ptr(sdl2_sys::SDL_GetError())
                .to_str()
                .unwrap()
        }
    }};
}

#[macro_export]
macro_rules! enclose {
    ( ($( $x:ident ),*) $y:expr ) => {
        {
            $(let $x = $x.clone();)*
            $y
        }
    };
}
