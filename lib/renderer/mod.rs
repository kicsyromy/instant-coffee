pub use errors::*;
pub(crate) use graphics_device_context::GraphicsDeviceContext;
pub(crate) use rendering_context::{Painter, RenderingContext};

mod ffi {
    use crate::renderer::graphics_device_context::*;

    extern "C" {
        pub(crate) fn rendering_init(error: &mut u32) -> *mut ffi::GraphicsDeviceContext;
        pub(crate) fn rendering_end(graphics_device_context: *mut *mut ffi::GraphicsDeviceContext);
    }
}

pub(crate) fn rendering_init() -> GraphicsDeviceContext {
    let mut error = 0;

    let ffi_gdc = unsafe { ffi::rendering_init(&mut error) };
    assert_eq!(error, 0);

    GraphicsDeviceContext { handle: ffi_gdc }
}

pub(crate) fn rendering_end(mut graphics_device_context: GraphicsDeviceContext) {
    unsafe {
        ffi::rendering_end(&mut graphics_device_context.handle);
    }
}

mod errors;
mod graphics_device_context;
mod rendering_context;

pub trait Renderable {
    fn render(&self, painter: &mut Painter);
}
