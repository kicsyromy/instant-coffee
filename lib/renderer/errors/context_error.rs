use std::fmt::{Debug, Display, Formatter};

/// Describes an graphics and rendering related errors
#[derive(Debug)]
pub enum ContextError {
    NativeContextError(String),
    VSyncUpdateError(String),
    ActivationError(String),
}

impl PartialEq for ContextError {
    fn eq(&self, other: &Self) -> bool {
        use ContextError::*;

        match &self {
            NativeContextError(_) => {
                matches!(other, NativeContextError(_))
            }
            VSyncUpdateError(_) => {
                matches!(other, VSyncUpdateError(_))
            }
            ActivationError(_) => {
                matches!(other, ActivationError(_))
            }
        }
    }
}

impl Display for ContextError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        use ContextError::*;

        match &self {
            NativeContextError(message) => f.write_fmt(format_args!(
                "Failed to set up underlying native rendering context: {}",
                message
            )),
            VSyncUpdateError(message) => f.write_fmt(format_args!(
                "Failed to change vertical refresh sync mode: {}",
                message
            )),
            ActivationError(message) => {
                f.write_fmt(format_args!("Failed to activate context: {}", message))
            }
        }
    }
}
