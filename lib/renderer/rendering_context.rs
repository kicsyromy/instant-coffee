use crate::*;

#[allow(dead_code)]
mod ffi {
    #[repr(C)]
    pub(crate) struct RenderingContext {
        _unused: [u8; 0],
    }

    #[repr(C)]
    pub(crate) struct Handle {
        _unused: [u8; 0],
    }

    #[repr(C)]
    #[allow(non_camel_case_types)]
    pub(crate) enum SgAction {
        _SG_ACTION_DEFAULT,
        SG_ACTION_CLEAR,
        SG_ACTION_LOAD,
        SG_ACTION_DONTCARE,
    }

    #[repr(C)]
    pub(crate) struct SgColor {
        r: f32,
        g: f32,
        b: f32,
        a: f32,
    }

    #[repr(C)]
    pub(crate) struct SgColorAttachmentAction {
        action: SgAction,
        value: SgColor,
    }

    #[repr(C)]
    pub(crate) struct SgDepthAttachmentAction {
        action: SgAction,
        value: f32,
    }

    #[repr(C)]
    pub(crate) struct SgStencilAttachmentAction {
        action: SgAction,
        value: u8,
    }

    #[repr(C)]
    pub(crate) struct SgPassAction {
        _start_canary: u32,
        colors: [SgColorAttachmentAction; 4],
        depth: SgDepthAttachmentAction,
        stencil: SgStencilAttachmentAction,
        _end_canary: u32,
    }

    #[repr(C)]
    pub(crate) struct SgpVec2 {
        x: f32,
        y: f32,
    }

    pub(crate) type SgpPoint = SgpVec2;

    #[repr(C)]
    pub(crate) struct SgpLine {
        a: SgpPoint,
        b: SgpPoint,
    }

    #[repr(C)]
    pub(crate) struct SgpTriangle {
        a: SgpPoint,
        b: SgpPoint,
        c: SgpPoint,
    }

    #[repr(C)]
    pub(crate) struct SgpRect {
        x: f32,
        y: f32,
        w: f32,
        h: f32,
    }

    pub(crate) type GraphicsDeviceContext =
        crate::renderer::graphics_device_context::ffi::GraphicsDeviceContext;

    extern "C" {
        pub(crate) fn rendering_context_new(
            gdc: *mut GraphicsDeviceContext,
            window_handle: *mut Handle,
            error: &mut u32,
        ) -> *mut RenderingContext;
        pub(crate) fn rendering_context_free(this: *mut RenderingContext);
        pub(crate) fn rendering_context_activate(this: *mut RenderingContext);
        pub(crate) fn rendering_context_deactivate(this: *mut RenderingContext);
        pub(crate) fn rendering_context_set_vsync(this: *mut RenderingContext, enabled: u8);
        pub(crate) fn rendering_context_present(this: *mut RenderingContext) -> u32;
        pub(crate) fn rendering_context_resize(
            this: *mut RenderingContext,
            width: i32,
            height: i32,
        ) -> u32;

        pub(crate) fn sg_begin_default_pass(
            pass_action: *const SgPassAction,
            width: i32,
            height: i32,
        );
        pub(crate) fn sg_end_pass();
        pub(crate) fn sg_commit();

        pub(crate) fn sgp_begin(width: i32, height: i32);
        pub(crate) fn sgp_flush();
        pub(crate) fn sgp_end();
        pub(crate) fn sgp_project(left: f32, right: f32, top: f32, bottom: f32);

        pub(crate) fn sgp_set_color(r: f32, g: f32, b: f32, a: f32);
        pub(crate) fn sgp_viewport(x: i32, y: i32, w: i32, h: i32);

        pub(crate) fn sgp_clear();
        pub(crate) fn sgp_draw_points(points: *const SgpPoint, count: u32);
        pub(crate) fn sgp_draw_point(x: f32, y: f32);
        pub(crate) fn sgp_draw_lines(lines: *const SgpLine, count: u32);
        pub(crate) fn sgp_draw_line(ax: f32, ay: f32, bx: f32, by: f32);
        pub(crate) fn sgp_draw_lines_strip(points: *const SgpPoint, count: u32);
        pub(crate) fn sgp_draw_filled_triangles(triangles: *const SgpTriangle, count: u32);
        pub(crate) fn sgp_draw_filled_triangle(
            ax: f32,
            ay: f32,
            bx: f32,
            by: f32,
            cx: f32,
            cy: f32,
        );
        pub(crate) fn sgp_draw_filled_triangles_strip(points: *const SgpPoint, count: u32);
        pub(crate) fn sgp_draw_filled_rects(rects: *const SgpRect, count: u32);
        pub(crate) fn sgp_draw_filled_rect(x: f32, y: f32, w: f32, h: f32);
    }
}

pub struct Painter<'a> {
    rc: &'a mut RenderingContext,
    width: i32,
    height: i32,
}

pub struct RenderingContext {
    handle: *mut ffi::RenderingContext,
}

impl RenderingContext {
    pub fn new(gdc: &mut GraphicsDeviceContext, window: &mut Window) -> Self {
        let mut error = 0;

        #[cfg(target_os = "windows")]
        let window_handle = {
            use raw_window_handle::{HasRawWindowHandle, RawWindowHandle};

            match window.raw_window_handle() {
                RawWindowHandle::Win32(handle) => handle.hwnd as *mut _,
                _ => unimplemented!(),
            }
        };

        #[cfg(target_os = "linux")]
        let window_handle = { window.native_handle() as _ };

        let ffi_rc = unsafe { ffi::rendering_context_new(gdc.handle, window_handle, &mut error) };
        assert_eq!(error, 0);

        Self { handle: ffi_rc }
    }

    pub fn activate(&mut self, x: i32, y: i32, width: i32, height: i32) -> Painter {
        unsafe {
            ffi::rendering_context_activate(self.handle);
        }

        Painter::new(self, x, y, width, height)
    }

    fn deactivate(&mut self) {
        unsafe {
            ffi::rendering_context_deactivate(self.handle);
        }
    }

    pub fn enable_vsync(&mut self) {
        unsafe {
            ffi::rendering_context_set_vsync(self.handle, 1);
        }
    }

    pub fn disable_vsync(&mut self) {
        unsafe {
            ffi::rendering_context_set_vsync(self.handle, 0);
        }
    }

    pub fn present(&mut self) {
        let result = unsafe { ffi::rendering_context_present(self.handle) };
        assert_eq!(result, 0);
    }

    pub fn resize(&mut self, width: i32, height: i32) {
        let result = unsafe { ffi::rendering_context_resize(self.handle, width, height) };
        assert_eq!(result, 0);
    }
}

impl<'a> Painter<'a> {
    pub fn new(rc: &'a mut RenderingContext, x: i32, y: i32, width: i32, height: i32) -> Self {
        unsafe {
            // Begin recording draw commands for a frame buffer of size (width, height).
            ffi::sgp_begin(width, height);
            // Set frame buffer drawing region to (0, 0, width, height).
            ffi::sgp_viewport(x, y, width, height);
            // Set drawing coordinate space to (left=-ratio, right=ratio, top=1, bottom=-1).
            ffi::sgp_project(0.0, width as f32, 0.0, height as f32);
        }

        Self { rc, width, height }
    }

    pub fn set_color(&mut self, r: f32, g: f32, b: f32, a: f32) {
        unsafe { ffi::sgp_set_color(r, g, b, a) }
    }

    pub fn draw_point(&mut self, x: f32, y: f32) {
        unsafe { ffi::sgp_draw_point(x, y) }
    }

    pub fn draw_line(&mut self, ax: f32, ay: f32, bx: f32, by: f32) {
        unsafe { ffi::sgp_draw_line(ax, ay, bx, by) }
    }

    pub fn draw_filled_rect(&mut self, x: f32, y: f32, w: f32, h: f32) {
        unsafe { ffi::sgp_draw_filled_rect(x, y, w, h) }
    }

    pub fn clear(&mut self) {
        unsafe {
            ffi::sgp_clear();
        }
    }
}

impl Drop for RenderingContext {
    fn drop(&mut self) {
        unsafe { ffi::rendering_context_free(self.handle) }
    }
}

impl Drop for Painter<'_> {
    fn drop(&mut self) {
        unsafe {
            let pass_action = core::mem::zeroed();
            ffi::sg_begin_default_pass(&pass_action, self.width, self.height);
            // Dispatch all draw commands to Sokol GFX.
            ffi::sgp_flush();
            // Finish a draw command queue, clearing it.
            ffi::sgp_end();
            // End render pass.
            ffi::sg_end_pass();
            // Commit Sokol render.
            ffi::sg_commit();
        }

        self.rc.present();
        self.rc.deactivate();
    }
}
