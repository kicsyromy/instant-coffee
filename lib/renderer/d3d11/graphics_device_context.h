#pragma once

#include <d3d11.h>
#include <dxgi.h>

#include <stdint.h>
#include <stdio.h>

// ## Forward declarations
struct RenderingContext;
static ID3D11RenderTargetView *rendering_context_render_target_view(struct RenderingContext *self);
static ID3D11DepthStencilView *rendering_context_depth_stencil_view(struct RenderingContext *self);

// ## Struct definition
struct GraphicsDeviceContext
{
    ID3D11Device        *d3d_device;
    ID3D11DeviceContext *d3d_device_context;
    IDXGIFactory        *dxgi_factory;

    struct RenderingContext *active_rendering_context;
};

// ## Internal API
struct GraphicsDeviceContext *graphics_device_context_new(struct sg_desc *sg_desc, uint32_t *error);
static IDXGISwapChain *graphics_device_context_create_swap_chain(struct GraphicsDeviceContext *self,
                                                                 void     *window_handle,
                                                                 uint32_t *error);
static ID3D11RenderTargetView *graphics_device_context_create_render_target_view(
    struct GraphicsDeviceContext *self,
    IDXGISwapChain               *swap_chain,
    uint32_t                     *error);
static ID3D11Texture2D *graphics_device_context_create_depth_stencil(
    struct GraphicsDeviceContext *self,
    IDXGISwapChain               *swap_chain,
    uint32_t                     *error);
static ID3D11DepthStencilView *graphics_device_context_create_depth_stencil_view(
    struct GraphicsDeviceContext *self,
    ID3D11Texture2D              *depth_stencil,
    uint32_t                     *error);

static const void *graphics_device_context_get_render_target_view(void *gdc);
static const void *graphics_device_context_get_depth_stencil_view(void *gdc);

// ## Implementation
struct GraphicsDeviceContext *graphics_device_context_new(struct sg_desc *sg_desc, uint32_t *error)
{
    static const D3D_FEATURE_LEVEL D3D_FEATURE_LEVELS[] = {
        D3D_FEATURE_LEVEL_12_1, D3D_FEATURE_LEVEL_12_0, D3D_FEATURE_LEVEL_11_1,
        D3D_FEATURE_LEVEL_11_0, D3D_FEATURE_LEVEL_10_1, D3D_FEATURE_LEVEL_10_0,
        D3D_FEATURE_LEVEL_9_3,  D3D_FEATURE_LEVEL_9_2,  D3D_FEATURE_LEVEL_9_1
    };
    static const UINT D3D_CREATION_FLAGS = D3D11_CREATE_DEVICE_BGRA_SUPPORT;

    struct GraphicsDeviceContext *self =
        (struct GraphicsDeviceContext *)malloc(sizeof(struct GraphicsDeviceContext));
    memset(self, 0, sizeof(struct GraphicsDeviceContext));

    uint32_t result = D3D11CreateDevice(NULL,
                                        D3D_DRIVER_TYPE_HARDWARE,
                                        NULL,
                                        D3D_CREATION_FLAGS,
                                        D3D_FEATURE_LEVELS,
                                        ARRAYSIZE(D3D_FEATURE_LEVELS),
                                        D3D11_SDK_VERSION,
                                        &self->d3d_device,
                                        NULL,
                                        &self->d3d_device_context);
    if (result != S_OK)
    {
        fprintf(stderr, "Failed to create HW D3D11Device: 0x%X\nTrying WARP instead...", result);

        // If the initialization fails, fall back to the WARP device.
        // For more information on WARP, see:
        // https://go.microsoft.com/fwlink/?LinkId=286690
        result = D3D11CreateDevice(NULL,
                                   D3D_DRIVER_TYPE_WARP,
                                   NULL,
                                   D3D_CREATION_FLAGS,
                                   D3D_FEATURE_LEVELS,
                                   ARRAYSIZE(D3D_FEATURE_LEVELS),
                                   D3D11_SDK_VERSION,
                                   &self->d3d_device,
                                   NULL,
                                   &self->d3d_device_context);
        if (result != S_OK)
        {
            fprintf(stderr, "\nFailed to create D3D11Device: 0x%X\n", result);
            goto fail;
        }

        fprintf(stderr, "Success!\n");
    }

    if (!self->d3d_device)
    {
        fprintf(stderr,
                "Failed to create D3D11Device: D3D11CreateDevice returned "
                "NULL ID3DDevice");
        result = S_FALSE;
        goto fail;
    }

    IDXGIDevice *dxgi_device = NULL;
    result                   = self->d3d_device->lpVtbl->QueryInterface(self->d3d_device,
                                                      &IID_IDXGIDevice,
                                                      (void **)&dxgi_device);
    if (result != S_OK)
    {
        fprintf(stderr, "Failed to get DXGIDevice associated with D3D11Device: 0x%X\n", result);
        goto fail;
    }

    IDXGIAdapter *dxgi_adapter = NULL;
    result                     = dxgi_device->lpVtbl->GetAdapter(dxgi_device, &dxgi_adapter);
    if (result != S_OK)
    {
        fprintf(stderr, "Failed to get DXGIAdapter associated with D3D11Device: 0x%X\n", result);
        goto fail;
    }

    result = dxgi_adapter->lpVtbl->GetParent(dxgi_adapter,
                                             &IID_IDXGIFactory,
                                             (void **)&self->dxgi_factory);
    if (result != S_OK)
    {
        fprintf(stderr, "Failed to get DXGIFactory associated with D3D11Device: 0x%X\n", result);
        goto fail;
    }
    dxgi_adapter->lpVtbl->Release(dxgi_adapter);

    sg_desc->context.d3d11.device         = self->d3d_device;
    sg_desc->context.d3d11.device_context = self->d3d_device_context;
    sg_desc->context.d3d11.render_target_view_userdata_cb =
        &graphics_device_context_get_render_target_view;
    sg_desc->context.d3d11.depth_stencil_view_userdata_cb =
        &graphics_device_context_get_depth_stencil_view;
    sg_desc->context.d3d11.user_data = self;

    return self;

fail:
    graphics_device_context_free(self);

    *error = result;
    return NULL;
}

void graphics_device_context_free(struct GraphicsDeviceContext *self)
{
    if (self->d3d_device)
    {
        self->d3d_device->lpVtbl->Release(self->d3d_device);
    }

    if (self->d3d_device_context)
    {
        self->d3d_device_context->lpVtbl->Release(self->d3d_device_context);
    }

    if (self->dxgi_factory)
    {
        self->dxgi_factory->lpVtbl->Release(self->dxgi_factory);
    }

    free(self);
}

static IDXGISwapChain *graphics_device_context_create_swap_chain(struct GraphicsDeviceContext *self,
                                                                 void     *window_handle,
                                                                 uint32_t *error)
{
    DXGI_SWAP_CHAIN_DESC swap_chain_description;
    memset(&swap_chain_description, 0, sizeof(DXGI_SWAP_CHAIN_DESC));
    swap_chain_description.BufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
    swap_chain_description.SampleDesc.Count  = 1;
    swap_chain_description.BufferUsage       = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    swap_chain_description.BufferCount       = 2;
    swap_chain_description.SwapEffect        = DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL;
    swap_chain_description.OutputWindow      = window_handle;
    swap_chain_description.Windowed          = TRUE;

    struct IDXGISwapChain *swap_chain = NULL;
    *error = self->dxgi_factory->lpVtbl->CreateSwapChain(self->dxgi_factory,
                                                         (IUnknown *)self->d3d_device,
                                                         &swap_chain_description,
                                                         &swap_chain);
    if (*error != S_OK)
    {
        fprintf(stderr, "Failed to create SwapChain: 0x%X\n", *error);
        return NULL;
    }

    return swap_chain;
}

static ID3D11RenderTargetView *graphics_device_context_create_render_target_view(
    struct GraphicsDeviceContext *self,
    IDXGISwapChain               *swap_chain,
    uint32_t                     *error)
{
    ID3D11Resource *back_buffer = NULL;
    *error =
        swap_chain->lpVtbl->GetBuffer(swap_chain, 0, &IID_ID3D11Resource, (void **)&back_buffer);
    if (*error != S_OK)
    {
        fprintf(stderr, "Failed to retrieve back buffer from swap_chain: 0x%X\n", *error);
        return NULL;
    }
    ID3D11RenderTargetView *render_target_view = NULL;
    *error = self->d3d_device->lpVtbl->CreateRenderTargetView(self->d3d_device,
                                                              back_buffer,
                                                              NULL,
                                                              &render_target_view);
    if (*error != S_OK)
    {
        fprintf(stderr, "Failed to create render target view: 0x%X\n", *error);
        return NULL;
    }
    back_buffer->lpVtbl->Release(back_buffer);

    return render_target_view;
}

static ID3D11Texture2D *graphics_device_context_create_depth_stencil(
    struct GraphicsDeviceContext *self,
    IDXGISwapChain               *swap_chain,
    uint32_t                     *error)
{
    ID3D11Texture2D *back_buffer = NULL;
    *error =
        swap_chain->lpVtbl->GetBuffer(swap_chain, 0, &IID_ID3D11Resource, (void **)&back_buffer);
    if (*error != S_OK)
    {
        fprintf(stderr, "Failed to retrieve back buffer from swap_chain: 0x%X\n", *error);
        return NULL;
    }
    D3D11_TEXTURE2D_DESC back_buffer_desc;
    back_buffer->lpVtbl->GetDesc(back_buffer, &back_buffer_desc);
    back_buffer->lpVtbl->Release(back_buffer);

    /* common desc for MSAA and depth-stencil texture */
    D3D11_TEXTURE2D_DESC tex_desc;
    memset(&tex_desc, 0, sizeof(D3D11_TEXTURE2D_DESC));
    tex_desc.Width              = back_buffer_desc.Width;
    tex_desc.Height             = back_buffer_desc.Height;
    tex_desc.MipLevels          = 1;
    tex_desc.ArraySize          = 1;
    tex_desc.Usage              = D3D11_USAGE_DEFAULT;
    tex_desc.Format             = DXGI_FORMAT_D24_UNORM_S8_UINT;
    tex_desc.BindFlags          = D3D11_BIND_DEPTH_STENCIL;
    tex_desc.SampleDesc.Count   = 1;
    tex_desc.SampleDesc.Quality = 0;

    ID3D11Texture2D *depth_stencil = NULL;
    *error                         = self->d3d_device->lpVtbl->CreateTexture2D(self->d3d_device,
                                                       &tex_desc,
                                                       NULL,
                                                       &depth_stencil);
    if (*error != S_OK)
    {
        fprintf(stderr, "Failed to create depth stencil texture: 0x%X\n", *error);
        return NULL;
    }

    return depth_stencil;
}

static ID3D11DepthStencilView *graphics_device_context_create_depth_stencil_view(
    struct GraphicsDeviceContext *self,
    ID3D11Texture2D              *depth_stencil,
    uint32_t                     *error)
{
    ID3D11DepthStencilView *depth_stencil_view = NULL;
    *error = self->d3d_device->lpVtbl->CreateDepthStencilView(self->d3d_device,
                                                              (ID3D11Resource *)depth_stencil,
                                                              NULL,
                                                              &depth_stencil_view);
    if (*error != S_OK)
    {
        fprintf(stderr, "Failed to create depth stencil view: 0x%X\n", *error);
        return NULL;
    }

    return depth_stencil_view;
}

static const void *graphics_device_context_get_render_target_view(void *gdc)
{
    struct GraphicsDeviceContext *self = (struct GraphicsDeviceContext *)gdc;
    return rendering_context_render_target_view(self->active_rendering_context);
}

static const void *graphics_device_context_get_depth_stencil_view(void *gdc)
{
    struct GraphicsDeviceContext *self = (struct GraphicsDeviceContext *)gdc;
    return rendering_context_depth_stencil_view(self->active_rendering_context);
}
