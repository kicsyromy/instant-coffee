#pragma once

#include "graphics_device_context.h"

#include <stdint.h>
#include <stdio.h>

// ## Struct definition
struct RenderingContext
{
    struct GraphicsDeviceContext *graphics_device_context;
    IDXGISwapChain               *swap_chain;
    ID3D11RenderTargetView       *render_target_view;
    ID3D11Texture2D              *depth_stencil;
    ID3D11DepthStencilView       *depth_stencil_view;
    int8_t                        vsync_enabled;
};

// ## Internal API
static ID3D11RenderTargetView *rendering_context_render_target_view(struct RenderingContext *self);
static ID3D11DepthStencilView *rendering_context_depth_stencil_view(struct RenderingContext *self);

struct RenderingContext *rendering_context_new(struct GraphicsDeviceContext *gdc,
                                               HWND                          window_handle,
                                               uint32_t                     *error)
{
    struct RenderingContext *self =
        (struct RenderingContext *)malloc(sizeof(struct RenderingContext));
    memset(self, 0, sizeof(struct RenderingContext));

    self->graphics_device_context = gdc;

    self->swap_chain = graphics_device_context_create_swap_chain(self->graphics_device_context,
                                                                 window_handle,
                                                                 error);
    if (*error != S_OK)
    {
        fprintf(stderr,
                "Failed to create RenderingContext: Failed to create SwapChain: 0x%X\n",
                *error);
        goto fail;
    }

    self->render_target_view =
        graphics_device_context_create_render_target_view(self->graphics_device_context,
                                                          self->swap_chain,
                                                          error);
    if (*error != S_OK)
    {
        fprintf(stderr,
                "Failed to resize SwapChain: Failed to create new RenderTargetView: 0x%X\n",
                *error);
        goto fail;
    }
    self->depth_stencil =
        graphics_device_context_create_depth_stencil(self->graphics_device_context,
                                                     self->swap_chain,
                                                     error);
    if (*error != S_OK)
    {
        fprintf(stderr,
                "Failed to resize SwapChain: Failed to create new DepthStencil: 0x%X\n",
                *error);
        goto fail;
    }
    self->depth_stencil_view =
        graphics_device_context_create_depth_stencil_view(self->graphics_device_context,
                                                          self->depth_stencil,
                                                          &*error);
    if (*error != S_OK)
    {
        fprintf(stderr,
                "Failed to resize SwapChain: Failed to create new DepthStencilView: 0x%X\n",
                *error);
        goto fail;
    }

    return self;

fail:
    rendering_context_free(self);
    return NULL;
}

void rendering_context_free(struct RenderingContext *self)
{
    if (self->depth_stencil_view)
    {
        self->depth_stencil_view->lpVtbl->Release(self->depth_stencil_view);
    }

    if (self->depth_stencil)
    {
        self->depth_stencil->lpVtbl->Release(self->depth_stencil);
    }

    if (self->render_target_view)
    {
        self->render_target_view->lpVtbl->Release(self->render_target_view);
    }

    if (self->swap_chain)
    {
        self->swap_chain->lpVtbl->Release(self->swap_chain);
    }

    free(self);
}

void rendering_context_activate(struct RenderingContext *self)
{
    self->graphics_device_context->active_rendering_context = self;
}

void rendering_context_deactivate(struct RenderingContext *self)
{
    self->graphics_device_context->active_rendering_context = NULL;
}

void rendering_context_set_vsync(struct RenderingContext *self, uint8_t value)
{
    self->vsync_enabled = value & 0x01;
}

uint32_t rendering_context_present(struct RenderingContext *self)
{
    return self->swap_chain->lpVtbl->Present(self->swap_chain, self->vsync_enabled, 0);
}

uint32_t rendering_context_resize(struct RenderingContext *self, int width, int height)
{
    if (self->depth_stencil_view)
    {
        self->depth_stencil_view->lpVtbl->Release(self->depth_stencil_view);
        self->depth_stencil_view = NULL;
    }

    if (self->depth_stencil)
    {
        self->depth_stencil->lpVtbl->Release(self->depth_stencil);
        self->depth_stencil = NULL;
    }

    if (self->render_target_view)
    {
        self->render_target_view->lpVtbl->Release(self->render_target_view);
        self->render_target_view = NULL;
    }

    // Use the client area of the window
    if (width < 0 || height < 0)
    {
        width  = 0;
        height = 0;
    }
    uint32_t error = self->swap_chain->lpVtbl->ResizeBuffers(self->swap_chain,
                                                             0,
                                                             width,
                                                             height,
                                                             DXGI_FORMAT_UNKNOWN,
                                                             0);
    if (error != S_OK)
    {
        fprintf(stderr, "Failed to resize SwapChain: ResizeBuffers failed: 0x%X\n", error);
        return error;
    }

    self->render_target_view =
        graphics_device_context_create_render_target_view(self->graphics_device_context,
                                                          self->swap_chain,
                                                          &error);
    if (error != S_OK)
    {
        fprintf(stderr,
                "Failed to resize SwapChain: Failed to create new RenderTargetView: 0x%X\n",
                error);
        return error;
    }
    self->depth_stencil =
        graphics_device_context_create_depth_stencil(self->graphics_device_context,
                                                     self->swap_chain,
                                                     &error);
    if (error != S_OK)
    {
        fprintf(stderr,
                "Failed to resize SwapChain: Failed to create new DepthStencil: 0x%X\n",
                error);
        return error;
    }
    self->depth_stencil_view =
        graphics_device_context_create_depth_stencil_view(self->graphics_device_context,
                                                          self->depth_stencil,
                                                          &error);
    if (error != S_OK)
    {
        fprintf(stderr,
                "Failed to resize SwapChain: Failed to create new DepthStencilView: 0x%X\n",
                error);
        return error;
    }

    return S_OK;
}

static ID3D11RenderTargetView *rendering_context_render_target_view(struct RenderingContext *self)
{
    return self->render_target_view;
}

static ID3D11DepthStencilView *rendering_context_depth_stencil_view(struct RenderingContext *self)
{
    return self->depth_stencil_view;
}
