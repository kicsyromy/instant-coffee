#pragma once

struct GraphicsDeviceContext;

// ## Internal API
struct GraphicsDeviceContext *graphics_device_context_new(struct sg_desc *sg_desc, uint32_t *error);
void                          graphics_device_context_free(struct GraphicsDeviceContext *self);

// ## Rendering backend specific Implementation
#if SOKOL_D3D11
#include "d3d11/graphics_device_context.h"
#elif _SOKOL_ANY_GL
#include "opengl/graphics_device_context.h"
#else
#error "Unsupported rendering backend"
#endif
