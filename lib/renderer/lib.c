#define SOKOL_IMPL

#include <sokol_gfx.h>

#define SOKOL_MALLOC malloc
#define SOKOL_FREE   free

#include <sokol_gp.h>

#include "graphics_device_context.h"
#include "rendering_context.h"

#include <stdint.h>

struct GraphicsDeviceContext *rendering_init(uint32_t *error)
{
    struct sg_desc sg_desc;
    memset(&sg_desc, 0, sizeof(struct sg_desc));

    struct GraphicsDeviceContext *gc = graphics_device_context_new(&sg_desc, error);
    if (*error != 0)
    {
        return NULL;
    }

    sg_setup(&sg_desc);
    if (!sg_isvalid())
    {
        *error = SGP_ERROR_ALLOC_FAILED;
        fprintf(stderr, "Failed to create Sokol GFX context!\n");
        return NULL;
    }

    sgp_desc sgp_desc;
    memset(&sgp_desc, 0, sizeof(struct sgp_desc));
    sgp_setup(&sgp_desc);
    if (!sgp_is_valid())
    {
        *error = sgp_get_last_error();
        fprintf(stderr, "Failed to create Sokol GP context: %s\n", sgp_get_error_message(*error));
        return NULL;
    }

    return gc;
}

void rendering_end(struct GraphicsDeviceContext **gc)
{
    sgp_shutdown();
    sg_shutdown();

    graphics_device_context_free(*gc);
    *gc = NULL;
}
