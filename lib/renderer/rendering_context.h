#pragma once

#include <stdint.h>

struct GraphicsDeviceContext;
struct RenderingContext;

// ## Public API
struct RenderingContext *rendering_context_new(struct GraphicsDeviceContext *gdc,
                                               void                         *window_handle,
                                               uint32_t                     *error);
void                     rendering_context_free(struct RenderingContext *self);
void                     rendering_context_activate(struct RenderingContext *self);
void                     rendering_context_deactivate(struct RenderingContext *self);
void                     rendering_context_set_vsync(struct RenderingContext *self, uint8_t value);
uint32_t                 rendering_context_present(struct RenderingContext *self);
uint32_t rendering_context_resize(struct RenderingContext *self, int width, int height);

// ## Rendering backend specific Implementation
#if defined(SOKOL_D3D11)
#include "d3d11/rendering_context.h"
#elif _SOKOL_ANY_GL
#include "opengl/rendering_context.h"
#else
#error "Unsupported rendering backend"
#endif
