pub(crate) mod ffi {
    #[repr(C)]
    pub(crate) struct GraphicsDeviceContext {
        _unused: [u8; 0],
    }
}

pub struct GraphicsDeviceContext {
    pub(crate) handle: *mut ffi::GraphicsDeviceContext,
}

unsafe impl Send for GraphicsDeviceContext {}

unsafe impl Sync for GraphicsDeviceContext {}
