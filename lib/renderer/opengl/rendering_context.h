#pragma once

#include <SDL2/SDL.h>

#include "graphics_device_context.h"

struct RenderingContext
{
    sg_context sgcontext;

    SDL_Window   *window;
    SDL_GLContext gl_context;
};

// ## Public API
struct RenderingContext *rendering_context_new(struct GraphicsDeviceContext *gdc,
                                               void                         *window_handle,
                                               uint32_t                     *error)
{
    struct RenderingContext *self =
        (struct RenderingContext *)malloc(sizeof(struct RenderingContext));
    memset(self, 0, sizeof(struct RenderingContext));

    *error = 0;

    self->window     = window_handle;
    self->gl_context = gdc->c;

    return self;
}

void rendering_context_free(struct RenderingContext *self)
{
    self->window = NULL;

    free(self);
}

void rendering_context_activate(struct RenderingContext *self)
{
    assert(SDL_GL_MakeCurrent(self->window, self->gl_context) == 0);
}

void rendering_context_deactivate(struct RenderingContext *self)
{
    (void)self;
}

void rendering_context_set_vsync(struct RenderingContext *self, uint8_t value)
{
    SDL_GLContext previous_ctx    = SDL_GL_GetCurrentContext();
    SDL_Window   *previous_window = SDL_GL_GetCurrentWindow();

    rendering_context_activate(self);
    SDL_GL_SetSwapInterval(value & 0x01);

    if (previous_window != NULL && previous_ctx != NULL)
    {
        SDL_GL_MakeCurrent(previous_window, previous_ctx);
    }
}

uint32_t rendering_context_present(struct RenderingContext *self)
{
    SDL_GL_SwapWindow(self->window);

    return 0;
}

uint32_t rendering_context_resize(struct RenderingContext *self, int width, int height)
{
    (void)self;
    (void)width;
    (void)height;

    return 0;
}
