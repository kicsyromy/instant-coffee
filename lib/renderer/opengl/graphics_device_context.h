#pragma once

#include <SDL2/SDL.h>

#include <stdint.h>
#include <stdio.h>

struct GraphicsDeviceContext
{
    SDL_Window *w;
    // This shouldn't be needed but sokol_gp/gfx do not want to play ball with multiple GL contexts
    SDL_GLContext c;
};

// ## Internal API

// This function assumes SDL_Init has been called
struct GraphicsDeviceContext *graphics_device_context_new(struct sg_desc *sg_desc, uint32_t *error)
{
    (void)sg_desc;

    struct GraphicsDeviceContext *self =
        (struct GraphicsDeviceContext *)malloc(sizeof(struct GraphicsDeviceContext));

    *error = 0;

    int result = SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
    if (result < 0)
    {
        fprintf(stderr, "Failed to initialize OpenGL: %s\n", SDL_GetError());
        *error = (uint32_t)-result;
        goto fail;
    }

    result = SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    if (result < 0)
    {
        fprintf(stderr, "Failed to initialize OpenGL: %s\n", SDL_GetError());
        *error = (uint32_t)-result;
        goto fail;
    }

    result = SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
    if (result < 0)
    {
        fprintf(stderr, "Failed to initialize OpenGL: %s\n", SDL_GetError());
        *error = (uint32_t)-result;
        goto fail;
    }

    // Create a dummy window to have a default OpenGL context available (sokol_gp requires one)
    self->w = SDL_CreateWindow("", 0, 0, 0, 0, SDL_WINDOW_OPENGL | SDL_WINDOW_HIDDEN);
    if (self->w == NULL)
    {
        fprintf(stderr,
                "Failed to initialize OpenGL: Failed to create initial GL context (window): %s\n",
                SDL_GetError());
        *error = 1;
        goto fail;
    }
    self->c = SDL_GL_CreateContext(self->w);
    if (self->c == NULL)
    {
        fprintf(stderr,
                "Failed to initialize OpenGL: Failed to create initial GL context: %s\n",
                SDL_GetError());
        *error = 1;
        goto fail;
    }

    return self;

fail:
    graphics_device_context_free(self);

    return NULL;
}

void graphics_device_context_free(struct GraphicsDeviceContext *self)
{
    if (self->w)
    {
        SDL_DestroyWindow(self->w);
    }

    free(self);
}
