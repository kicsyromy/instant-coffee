use std::sync::RwLock;
/// Errors that can occur during application execution
use std::{
    fmt::{Debug, Display, Formatter},
    sync::{MutexGuard, PoisonError, RwLockReadGuard, RwLockWriteGuard, Weak},
};

use crate::Application;

/// The errors that can occur during application execution
#[derive(Debug)]
pub enum ApplicationError {
    /// An instance of an application already exists and a new one cannot be created.
    AlreadyExists,
    /// Failed to initialize SDL.
    SDLInitializationFailed(String),
    /// GFX initialization failed
    GFXInitializationFailed(String),
    /// Cannot register for an event type
    EventRegistrationFailed(String),
    /// Failed to post an event to the queue
    PostEventFailed(String),
    /// A path could not be found
    PathNotFound(String),
    /// A path that was expected to be a directory was not
    NotADirectory(String),
    /// Not enough permissions to access a path
    PathPermissionDenied(String),
    /// An unknown system error
    SystemError(Box<dyn std::error::Error>),
}

impl From<PoisonError<RwLockWriteGuard<'_, Application<'_>>>> for ApplicationError {
    fn from(_: PoisonError<RwLockWriteGuard<'_, Application<'_>>>) -> Self {
        ApplicationError::SystemError("Application RwLock was poisoned, buggy app?".into())
    }
}

impl From<PoisonError<RwLockReadGuard<'_, Application<'_>>>> for ApplicationError {
    fn from(_: PoisonError<RwLockReadGuard<'_, Application<'_>>>) -> Self {
        ApplicationError::SystemError("Application RwLock was poisoned, buggy app?".into())
    }
}

impl From<PoisonError<MutexGuard<'_, Weak<RwLock<Application<'_>>>>>> for ApplicationError {
    fn from(_: PoisonError<MutexGuard<'_, Weak<RwLock<Application<'_>>>>>) -> Self {
        ApplicationError::SystemError("Application global lock was poisoned, buggy app?".into())
    }
}

impl PartialEq for ApplicationError {
    fn eq(&self, other: &Self) -> bool {
        match &self {
            ApplicationError::AlreadyExists => {
                matches!(other, ApplicationError::AlreadyExists)
            }
            ApplicationError::SDLInitializationFailed(_) => {
                matches!(other, ApplicationError::SDLInitializationFailed(_))
            }
            ApplicationError::GFXInitializationFailed(_) => {
                matches!(other, ApplicationError::GFXInitializationFailed(_))
            }
            ApplicationError::EventRegistrationFailed(_) => {
                matches!(other, ApplicationError::EventRegistrationFailed(_))
            }
            ApplicationError::PostEventFailed(_) => {
                matches!(other, ApplicationError::PostEventFailed(_))
            }
            ApplicationError::PathNotFound(_) => {
                matches!(other, ApplicationError::PathNotFound(_))
            }
            ApplicationError::NotADirectory(_) => {
                matches!(other, ApplicationError::NotADirectory(_))
            }
            ApplicationError::PathPermissionDenied(_) => {
                matches!(other, ApplicationError::PathPermissionDenied(_))
            }
            ApplicationError::SystemError(_) => {
                matches!(other, ApplicationError::SystemError(_))
            }
        }
    }
}

impl Display for ApplicationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            ApplicationError::AlreadyExists => f.write_str(
                "ApplicationError: Cannot create more then once instance of Application",
            ),
            ApplicationError::SDLInitializationFailed(message) => {
                f.write_fmt(format_args!("ApplicationError: {}", message))
            }
            ApplicationError::GFXInitializationFailed(message) => {
                f.write_fmt(format_args!("ApplicationError: {}", message))
            }
            ApplicationError::EventRegistrationFailed(message) => {
                f.write_fmt(format_args!("ApplicationError: {}", message))
            }
            ApplicationError::PostEventFailed(message) => {
                f.write_fmt(format_args!("ApplicationError: {}", message))
            }
            ApplicationError::PathNotFound(message) => {
                f.write_fmt(format_args!("ApplicationError: {}", message))
            }
            ApplicationError::NotADirectory(message) => {
                f.write_fmt(format_args!("ApplicationError: {}", message))
            }
            ApplicationError::PathPermissionDenied(message) => {
                f.write_fmt(format_args!("ApplicationError: {}", message))
            }
            ApplicationError::SystemError(error) => Display::fmt(&error, f),
        }
    }
}
