use std::{
    error::Error,
    fmt::{Display, Formatter},
    sync::{MutexGuard, PoisonError, RwLock, RwLockReadGuard, RwLockWriteGuard, Weak},
};

pub use application_error::*;
pub use window_error::*;

use crate::Application;

mod application_error;
mod window_error;

#[derive(Debug, PartialEq)]
pub enum CaffeineError {
    Application(ApplicationError),
    Window(WindowError),
}

impl From<ApplicationError> for CaffeineError {
    fn from(e: ApplicationError) -> Self {
        Self::Application(e)
    }
}

impl From<WindowError> for CaffeineError {
    fn from(e: WindowError) -> Self {
        Self::Window(e)
    }
}

impl From<PoisonError<RwLockWriteGuard<'_, Application<'_>>>> for CaffeineError {
    fn from(e: PoisonError<RwLockWriteGuard<'_, Application<'_>>>) -> Self {
        e.into()
    }
}

impl From<PoisonError<RwLockReadGuard<'_, Application<'_>>>> for CaffeineError {
    fn from(e: PoisonError<RwLockReadGuard<'_, Application<'_>>>) -> Self {
        e.into()
    }
}

impl From<PoisonError<MutexGuard<'_, Weak<RwLock<Application<'_>>>>>> for CaffeineError {
    fn from(e: PoisonError<MutexGuard<'_, Weak<RwLock<Application<'_>>>>>) -> Self {
        e.into()
    }
}

impl Display for CaffeineError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Application(e) => e.fmt(f),
            Self::Window(e) => e.fmt(f),
        }
    }
}

impl Error for CaffeineError {}
