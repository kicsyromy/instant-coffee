use std::{cell::RefCell, ops::Deref, rc::Rc};

use log::*;
use raw_window_handle::*;
use sdl2_sys::{
    SDL_CreateWindow, SDL_DestroyWindow, SDL_GetVersion, SDL_GetWindowGrab, SDL_GetWindowID,
    SDL_GetWindowSize, SDL_GetWindowWMInfo, SDL_SetRelativeMouseMode, SDL_SetWindowGrab,
    SDL_SysWMinfo, SDL_Window, SDL_WindowFlags, SDL_bool, SDL_SYSWM_TYPE,
    SDL_WINDOWPOS_UNDEFINED_MASK,
};

use crate::events::*;
use crate::{
    Application, CaffeineResult, CallbackResult, DisableSend, DisableSync, Painter,
    RenderingContext, WindowError,
};

#[allow(unused_macros)]
macro_rules! sdl_wm_get_ptr {
    ($wmi: expr, $i: literal) => {{
        let index = $i as usize;
        let mut result: *mut core::ffi::c_void = core::ptr::null_mut();
        unsafe {
            core::ptr::copy(
                &mut { $wmi }.info.dummy[index * core::mem::size_of::<usize>()] as *mut _
                    as *mut core::ffi::c_void,
                core::mem::transmute(&mut result),
                core::mem::size_of::<usize>(),
            )
        }

        result
    }};
}

pub type KeyPressCallback = dyn FnMut(&mut Window, &KeyPressEvent);
pub type KeyReleaseCallback = dyn FnMut(&mut Window, &KeyReleaseEvent);
pub type MouseMoveCallback = dyn FnMut(&mut Window, &MouseMoveEvent);
pub type MouseButtonPressCallback = dyn FnMut(&mut Window, &MouseButtonPressEvent);
pub type MouseButtonReleaseCallback = dyn FnMut(&mut Window, &MouseButtonReleaseEvent);
pub type RenderCallback = dyn FnMut(&mut Window, Painter, &RenderEvent);

pub struct Window {
    data: Rc<RefCell<WindowData>>,
    _disable_send: Option<DisableSend>,
    _disable_sync: Option<DisableSync>,
}

impl Window {
    #[allow(clippy::too_many_arguments)]
    fn new(
        mut title: String,
        x: i32,
        y: i32,
        width: i32,
        height: i32,
        vsync: bool,
        key_press_callback: Box<KeyPressCallback>,
        key_release_callback: Box<KeyReleaseCallback>,
        mouse_move_callback: Box<MouseMoveCallback>,
        mouse_button_press_callback: Box<MouseButtonPressCallback>,
        mouse_button_release_callback: Box<MouseButtonReleaseCallback>,
        render_callback: Box<RenderCallback>,
    ) -> CaffeineResult<Self> {
        title.push('\0');

        #[allow(unused_mut)]
        let mut window_flags = SDL_WindowFlags::SDL_WINDOW_RESIZABLE as u32;

        #[cfg(target_os = "linux")]
        {
            if cfg!(feature = "dxvk") {
                window_flags |= SDL_WindowFlags::SDL_WINDOW_VULKAN as u32;
            } else {
                window_flags |= SDL_WindowFlags::SDL_WINDOW_OPENGL as u32;
            }
        }

        let mut this = Self {
            data: Rc::new(RefCell::new(WindowData {
                context: None,
                frame_counter: 0,
                time: 0_f32,
                key_press_callback,
                key_release_callback,
                mouse_move_callback,
                mouse_button_press_callback,
                mouse_button_release_callback,
                render_callback,
                handle: WindowHandle {
                    ptr: unsafe {
                        SDL_CreateWindow(
                            title.as_ptr() as *const i8,
                            x,
                            y,
                            width,
                            height,
                            window_flags,
                        )
                    },
                },
            })),
            _disable_send: None,
            _disable_sync: None,
        };

        if let Some(app) = Application::instance() {
            let mut app = app.write()?;

            {
                let mut new_context =
                    RenderingContext::new(app.graphics_device_context(), &mut this);
                if vsync {
                    new_context.enable_vsync();
                }
                let this_context = &mut this.data.borrow_mut().context;
                *this_context = Some(new_context);
            }

            let this_data = Rc::clone(&this.data);
            app.register_event_handler(
                move |_window_id, event: &KeyPressEvent| {
                    let mut cb = {
                        let mut this = RefCell::borrow_mut(this_data.deref());
                        core::mem::replace(&mut this.key_press_callback, Box::new(|_, _| {}))
                    };

                    (cb)(
                        &mut Window {
                            data: Rc::clone(&this_data),
                            _disable_send: None,
                            _disable_sync: None,
                        },
                        event,
                    );

                    {
                        let mut this = RefCell::borrow_mut(this_data.deref());
                        core::mem::swap(&mut this.key_press_callback, &mut cb);
                    }

                    CallbackResult::Continue
                },
                Some(this.id()),
                None,
            )?;

            let this_data = Rc::clone(&this.data);
            app.register_event_handler(
                move |_window_id, event: &KeyReleaseEvent| {
                    let mut cb = {
                        let mut this = RefCell::borrow_mut(this_data.deref());
                        core::mem::replace(&mut this.key_release_callback, Box::new(|_, _| {}))
                    };

                    (cb)(
                        &mut Window {
                            data: Rc::clone(&this_data),
                            _disable_send: None,
                            _disable_sync: None,
                        },
                        event,
                    );

                    {
                        let mut this = RefCell::borrow_mut(this_data.deref());
                        core::mem::swap(&mut this.key_release_callback, &mut cb);
                    }

                    CallbackResult::Continue
                },
                Some(this.id()),
                None,
            )?;

            let this_data = Rc::clone(&this.data);
            app.register_event_handler(
                move |_window_id, event: &MouseMoveEvent| {
                    let mut cb = {
                        let mut this = RefCell::borrow_mut(this_data.deref());
                        core::mem::replace(&mut this.mouse_move_callback, Box::new(|_, _| {}))
                    };

                    (cb)(
                        &mut Window {
                            data: Rc::clone(&this_data),
                            _disable_send: None,
                            _disable_sync: None,
                        },
                        event,
                    );

                    {
                        let mut this = RefCell::borrow_mut(this_data.deref());
                        core::mem::swap(&mut this.mouse_move_callback, &mut cb);
                    }

                    CallbackResult::Continue
                },
                Some(this.id()),
                None,
            )?;

            let this_data = Rc::clone(&this.data);
            app.register_event_handler(
                move |_window_id, event: &MouseButtonPressEvent| {
                    let mut cb = {
                        let mut this = RefCell::borrow_mut(this_data.deref());
                        core::mem::replace(
                            &mut this.mouse_button_press_callback,
                            Box::new(|_, _| {}),
                        )
                    };

                    (cb)(
                        &mut Window {
                            data: Rc::clone(&this_data),
                            _disable_send: None,
                            _disable_sync: None,
                        },
                        event,
                    );

                    {
                        let mut this = RefCell::borrow_mut(this_data.deref());
                        core::mem::swap(&mut this.mouse_button_press_callback, &mut cb);
                    }

                    CallbackResult::Continue
                },
                Some(this.id()),
                None,
            )?;

            let this_data = Rc::clone(&this.data);
            app.register_event_handler(
                move |_window_id, event: &MouseButtonReleaseEvent| {
                    let mut cb = {
                        let mut this = RefCell::borrow_mut(this_data.deref());
                        core::mem::replace(
                            &mut this.mouse_button_release_callback,
                            Box::new(|_, _| {}),
                        )
                    };

                    (cb)(
                        &mut Window {
                            data: Rc::clone(&this_data),
                            _disable_send: None,
                            _disable_sync: None,
                        },
                        event,
                    );

                    {
                        let mut this = RefCell::borrow_mut(this_data.deref());
                        core::mem::swap(&mut this.mouse_button_release_callback, &mut cb);
                    }

                    CallbackResult::Continue
                },
                Some(this.id()),
                None,
            )?;

            let this_data = Rc::clone(&this.data);
            app.register_event_handler(
                move |window_id, event: &MouseWheelEvent| {
                    let mut this = RefCell::borrow_mut(this_data.deref());
                    this.on_mouse_wheel(window_id, event);

                    CallbackResult::Continue
                },
                Some(this.id()),
                None,
            )?;

            let this_data = Rc::clone(&this.data);
            app.register_event_handler(
                move |window_id, event: &WindowMouseEnterEvent| {
                    let mut this = RefCell::borrow_mut(this_data.deref());
                    this.on_mouse_entered(window_id, event);

                    CallbackResult::Continue
                },
                Some(this.id()),
                None,
            )?;

            let this_data = Rc::clone(&this.data);
            app.register_event_handler(
                move |window_id, event: &WindowMouseLeaveEvent| {
                    let mut this = RefCell::borrow_mut(this_data.deref());
                    this.on_mouse_left(window_id, event);

                    CallbackResult::Continue
                },
                Some(this.id()),
                None,
            )?;

            let this_data = Rc::clone(&this.data);
            app.register_event_handler(
                move |window_id, event: &WindowResizeEvent| {
                    let mut this = RefCell::borrow_mut(this_data.deref());
                    this.context
                        .as_mut()
                        .unwrap()
                        .resize(event.width, event.height);
                    this.on_window_resized(window_id, event);

                    CallbackResult::Continue
                },
                Some(this.id()),
                None,
            )?;

            let this_data = Rc::clone(&this.data);
            app.register_event_handler(
                move |window_id, event: &WindowCloseEvent| {
                    let mut this = RefCell::borrow_mut(this_data.deref());
                    this.on_window_closed(window_id, event);

                    CallbackResult::Continue
                },
                Some(this.id()),
                None,
            )?;

            let this_data = Rc::clone(&this.data);
            app.register_event_handler(
                move |_window_id, event: &RenderEvent| {
                    let (mut context, mut cb) = {
                        let mut this = RefCell::borrow_mut(this_data.deref());

                        this.time += event.time_elapsed();
                        if this.time >= 1.0 {
                            info!("FPS: {}", this.frame_counter + event.frames_rendered());
                            this.time -= 1.0;
                            this.frame_counter = -event.frames_rendered();
                        }

                        (
                            core::mem::replace(&mut this.context, None),
                            core::mem::replace(&mut this.render_callback, Box::new(|_, _, _| {})),
                        )
                    };

                    let mut window = Window {
                        data: Rc::clone(&this_data),
                        _disable_send: None,
                        _disable_sync: None,
                    };

                    {
                        let context = context.as_mut().unwrap();
                        let painter = context.activate(0, 0, window.width(), window.height());
                        (cb)(&mut window, painter, event);
                    }

                    {
                        let mut this = RefCell::borrow_mut(this_data.deref());
                        core::mem::swap(&mut this.context, &mut context);
                        core::mem::swap(&mut this.render_callback, &mut cb);
                    }

                    CallbackResult::Continue
                },
                Some(this.id()),
                None,
            )?;
        } else {
            return Err(WindowError::NoApplicationInstance.into());
        }

        Ok(this)
    }

    pub fn id(&self) -> i32 {
        RefCell::borrow(self.data.deref()).id()
    }

    pub fn size(&self) -> (i32, i32) {
        RefCell::borrow(self.data.deref()).size()
    }

    pub fn width(&self) -> i32 {
        RefCell::borrow(self.data.deref()).width()
    }

    pub fn height(&self) -> i32 {
        RefCell::borrow(self.data.deref()).height()
    }

    pub fn capture_mouse(&mut self) {
        unsafe {
            SDL_SetRelativeMouseMode(SDL_bool::SDL_TRUE);
            SDL_SetWindowGrab(self.native_handle() as _, SDL_bool::SDL_TRUE);
        }
    }

    pub fn release_mouse(&mut self) {
        unsafe {
            SDL_SetRelativeMouseMode(SDL_bool::SDL_FALSE);
            SDL_SetWindowGrab(self.native_handle() as _, SDL_bool::SDL_FALSE);
        }
    }

    pub fn is_mouse_captured(&self) -> bool {
        unsafe { SDL_GetWindowGrab(self.native_handle_const() as _) == SDL_bool::SDL_TRUE }
    }

    pub fn native_handle(&mut self) -> *mut core::ffi::c_void {
        RefCell::borrow_mut(self.data.deref()).handle.ptr as _
    }

    fn native_handle_const(&self) -> *mut core::ffi::c_void {
        RefCell::borrow(self.data.deref()).handle.ptr as _
    }
}

pub(crate) struct WindowHandle {
    pub(crate) ptr: *mut SDL_Window,
}

impl Drop for WindowHandle {
    fn drop(&mut self) {
        unsafe { SDL_DestroyWindow(self.ptr) }
    }
}

struct WindowData {
    context: Option<RenderingContext>,
    frame_counter: i64,
    time: f32,
    key_press_callback: Box<KeyPressCallback>,
    key_release_callback: Box<KeyReleaseCallback>,
    mouse_move_callback: Box<MouseMoveCallback>,
    mouse_button_press_callback: Box<MouseButtonPressCallback>,
    mouse_button_release_callback: Box<MouseButtonReleaseCallback>,
    render_callback: Box<RenderCallback>,
    handle: WindowHandle,
}

impl WindowData {
    pub fn id(&self) -> i32 {
        (unsafe { SDL_GetWindowID(self.handle.ptr) }) as i32
    }

    pub fn size(&self) -> (i32, i32) {
        let mut w = 0_i32;
        let mut h = 0_i32;
        unsafe {
            SDL_GetWindowSize(self.handle.ptr, &mut w, &mut h);
        }
        (w, h)
    }

    pub fn width(&self) -> i32 {
        let mut w = 0_i32;

        unsafe {
            SDL_GetWindowSize(self.handle.ptr, &mut w, std::ptr::null_mut());
        }
        w
    }

    pub fn height(&self) -> i32 {
        let mut h = 0_i32;
        unsafe {
            SDL_GetWindowSize(self.handle.ptr, std::ptr::null_mut(), &mut h);
        }
        h
    }

    fn on_mouse_wheel(&mut self, window_id: i32, event: &MouseWheelEvent) {
        trace!("Mouse wheel {:#?} on window {}", event, window_id);
    }

    fn on_mouse_entered(&mut self, window_id: i32, event: &WindowMouseEnterEvent) {
        trace!("Mouse entered {:#?} on window {}", event, window_id);
    }

    fn on_mouse_left(&mut self, window_id: i32, event: &WindowMouseLeaveEvent) {
        trace!("Mouse left {:#?} on window {}", event, window_id);
    }

    fn on_window_resized(&mut self, window_id: i32, event: &WindowResizeEvent) {
        trace!("Window resized {:#?} on window {}", event, window_id);
    }

    fn on_window_closed(&mut self, window_id: i32, event: &WindowCloseEvent) {
        trace!("Window closed {:#?} pressed on window {}", event, window_id);
        unsafe { SDL_DestroyWindow(self.handle.ptr) };
    }
}

pub struct WindowBuilder {
    x: i32,
    y: i32,
    width: i32,
    height: i32,
    title: String,
    vsync: bool,
    key_press_callback: Box<KeyPressCallback>,
    key_release_callback: Box<KeyReleaseCallback>,
    mouse_move_callback: Box<MouseMoveCallback>,
    mouse_button_press_callback: Box<MouseButtonPressCallback>,
    mouse_button_release_callback: Box<MouseButtonReleaseCallback>,
    render_callback: Box<RenderCallback>,
}

impl WindowBuilder {
    pub fn new() -> Self {
        Self {
            x: SDL_WINDOWPOS_UNDEFINED_MASK as i32,
            y: SDL_WINDOWPOS_UNDEFINED_MASK as i32,
            width: 1280,
            height: 800,
            title: "".into(),
            vsync: true,
            key_press_callback: Box::new(|_, _| {}),
            key_release_callback: Box::new(|_, _| {}),
            mouse_move_callback: Box::new(|_, _| {}),
            mouse_button_press_callback: Box::new(|_, _| {}),
            mouse_button_release_callback: Box::new(|_, _| {}),
            render_callback: Box::new(|_, _, _| {}),
        }
    }

    pub fn title(mut self, v: String) -> Self {
        self.title = v;

        self
    }

    pub fn x(mut self, v: i32) -> Self {
        self.x = v;

        self
    }

    pub fn y(mut self, v: i32) -> Self {
        self.y = v;

        self
    }

    pub fn width(mut self, v: i32) -> Self {
        self.width = v;

        self
    }

    pub fn height(mut self, v: i32) -> Self {
        self.height = v;

        self
    }

    pub fn vsync(mut self, v: bool) -> Self {
        self.vsync = v;

        self
    }

    pub fn on_key_pressed<F>(mut self, cb: F) -> Self
    where
        F: FnMut(&mut Window, &KeyPressEvent) + 'static,
    {
        self.key_press_callback = Box::new(cb);

        self
    }

    pub fn on_key_released<F>(mut self, cb: F) -> Self
    where
        F: FnMut(&mut Window, &KeyReleaseEvent) + 'static,
    {
        self.key_release_callback = Box::new(cb);

        self
    }

    pub fn on_mouse_moved<F>(mut self, cb: F) -> Self
    where
        F: FnMut(&mut Window, &MouseMoveEvent) + 'static,
    {
        self.mouse_move_callback = Box::new(cb);

        self
    }

    pub fn on_mouse_button_pressed<F>(mut self, cb: F) -> Self
    where
        F: FnMut(&mut Window, &MouseButtonPressEvent) + 'static,
    {
        self.mouse_button_press_callback = Box::new(cb);

        self
    }

    pub fn on_mouse_button_released<F>(mut self, cb: F) -> Self
    where
        F: FnMut(&mut Window, &MouseButtonReleaseEvent) + 'static,
    {
        self.mouse_button_release_callback = Box::new(cb);

        self
    }

    pub fn on_render<F>(mut self, cb: F) -> Self
    where
        F: FnMut(&mut Window, Painter, &RenderEvent) + 'static,
    {
        self.render_callback = Box::new(cb);

        self
    }

    pub fn build(self) -> CaffeineResult<Window> {
        Window::new(
            self.title,
            self.x,
            self.y,
            self.width,
            self.height,
            self.vsync,
            self.key_press_callback,
            self.key_release_callback,
            self.mouse_move_callback,
            self.mouse_button_press_callback,
            self.mouse_button_release_callback,
            self.render_callback,
        )
    }
}

impl Default for WindowBuilder {
    fn default() -> Self {
        Self::new()
    }
}

unsafe impl HasRawWindowHandle for Window {
    fn raw_window_handle(&self) -> RawWindowHandle {
        let mut wmi: SDL_SysWMinfo = unsafe { core::mem::zeroed() };

        unsafe { SDL_GetVersion(&mut wmi.version) };

        if unsafe { SDL_GetWindowWMInfo(self.native_handle_const() as *mut _, &mut wmi) }
            == SDL_bool::SDL_FALSE
        {
            panic!("Failed to get platform windowing information");
        }

        match wmi.subsystem {
            #[cfg(target_os = "linux")]
            SDL_SYSWM_TYPE::SDL_SYSWM_X11 => {
                let mut handle = XlibWindowHandle::empty();
                handle.window = unsafe { wmi.info.x11.window };

                RawWindowHandle::Xlib(handle)
            }
            #[cfg(target_os = "linux")]
            SDL_SYSWM_TYPE::SDL_SYSWM_WAYLAND => {
                let mut handle = WaylandWindowHandle::empty();
                handle.surface = unsafe { wmi.info.wl.surface } as *mut _;

                RawWindowHandle::Wayland(handle)
            }
            #[cfg(target_os = "windows")]
            SDL_SYSWM_TYPE::SDL_SYSWM_WINDOWS => {
                let mut handle = Win32WindowHandle::empty();

                handle.hwnd = sdl_wm_get_ptr!(wmi, 0);
                handle.hinstance = sdl_wm_get_ptr!(wmi, 2);

                RawWindowHandle::Win32(handle)
            }
            #[cfg(target_os = "android")]
            SDL_SYSWM_TYPE::SDL_SYSWM_ANDROID => {
                let mut handle = AndroidNdkWindowHandle::empty();
                handle.a_native_window = unsafe { wmi.info.android.window };

                RawWindowHandle::AndroidNdk(handle)
            }
            #[cfg(target_os = "macos")]
            SDL_SYSWM_TYPE::SDL_SYSWM_COCOA => {
                let mut handle = AppKitWindowHandle::empty();
                handle.ns_window = unsafe { wmi.info.cocoa.window };

                RawWindowHandle::AppKit(handle)
            }
            #[cfg(target_os = "ios")]
            SDL_SYSWM_TYPE::SDL_SYSWM_UIKIT => {
                let mut handle = UiKitWindowHandle::empty();
                handle.ui_window = unsafe { wmi.info.uikit.window };

                RawWindowHandle::UiKit(handle)
            }
            _ => panic!("Unsupported video subsystem"),
        }
    }
}
